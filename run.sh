#!/bin/sh

SCRIPT_PATH=`dirname $0 | xargs readlink -f`
BUILD_PATH=${SCRIPT_PATH}/build
BINARY_PATH=${BUILD_PATH}/bin
EXECUTABLE=${BINARY_PATH}/myran

${EXECUTABLE}
