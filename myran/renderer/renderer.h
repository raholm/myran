#pragma once

#include <memory>

#include "../window.h"
#include "../game/game.h"

#include "vulkan/vulkan.h"

#include "cube_mesh.h"

namespace myran {

  struct color
  {
    u8 Red { 0 };
    u8 Green { 0 };
    u8 Blue { 0 };
    u8 Alpha { 255 };
  };

  constexpr VkDeviceSize kDeviceMemorySize = 2 * 128 * 1024 * 1024;
  constexpr VkDeviceSize kHostMemorySize = 128 * 1024 * 1024;
  constexpr VkDeviceSize kInstanceBufferSize = 4 * 128 * 1024;
  constexpr VkDeviceSize kUniformBufferSize = 1024;
  constexpr VkDeviceSize kCubeMeshBufferSize = kCubeMesh.size() * 6 * 4;
  constexpr u8 kMinimapScaleFactor = 4;
  constexpr color kPlayerColor = { 0, 255, 0, 255 };
  constexpr f32 kPlayerSize = { 0.25 };
  constexpr color kItemColor = { 255, 0, 255, 255 };
  constexpr f32 kItemSize = { 0.25 };
  constexpr f32 kItemY = 0.5f;

  class renderer final
  {
  public:
    static std::unique_ptr<renderer> Create(const window& Window);

  public:
    void StartFrame();
    void RenderFrame();
    void WaitToFinish();

    void RenderGlobalObserver(const first_person_camera& Camera,
                              const glm::vec3 PlayerPosition,
                              const maze_geometry& MazeGeometry);

    void RenderGameplay(const first_person_camera& PlayerCamera,
                        const minimap_camera& MinimapCamera,
                        const maze_geometry& MazeGeometry);

  private:
    renderer() = default;

  private:
    struct global_observer_renderer
    {
      vk_render_pass RenderPass;
      vk_descriptor_set_layout DescriptorSetLayout;
      vk_pipeline_layout PipelineLayout;
      vk_graphics_pipeline Pipeline;

      std::vector<vk_descriptor_set> DescriptorSets;
    };

    struct gameplay_renderer
    {
      vk_render_pass RenderPass;
      vk_descriptor_set_layout DescriptorSetLayout;
      vk_pipeline_layout PipelineLayout;

      std::vector<vk_descriptor_set> DescriptorSets;

      vk_graphics_pipeline Offscreen_Pipeline;
      vk_graphics_pipeline Onscreen_Pipeline;
      vk_graphics_pipeline Minimap_Pipeline;

      // @todo: We do not need it since we use input attachments instead
      vk_sampler Sampler;

      std::vector<vk_image> ColorImages;
      std::vector<vk_image_view> ColorImageViews;

      std::vector<vk_image> WorldPositionImages;
      std::vector<vk_image_view> WorldPositionImageViews;

      std::vector<vk_image> NormalImages;
      std::vector<vk_image_view> NormalImageViews;
    };

  private:
    static global_observer_renderer CreateGlobalObserverRenderer_(const vk_device& Device,
                                                                  const vk_descriptor_pool& DescriptorPool,
                                                                  const vk_swap_chain& SwapChain,
                                                                  const std::vector<vk_buffer>& VertexUniformBuffers,
                                                                  const std::vector<vk_buffer>& FragmentUniformBuffers,
                                                                  const std::vector<vk_image>& DepthBuffers);

    static gameplay_renderer CreateGameplayRenderer_(const vk_device& Device,
                                                     const vk_descriptor_pool& DescriptorPool,
                                                     const vk_swap_chain& SwapChain,
                                                     const std::vector<vk_buffer>& VertexUniformBuffers,
                                                     const std::vector<vk_buffer>& FragmentUniformBuffers,
                                                     const std::vector<vk_image>& DepthBuffers,
                                                     std::vector<vk_image>&& ColorImages,
                                                     std::vector<vk_image_view>&& ColorImageViews,
                                                     std::vector<vk_image>&& WorldPositionImages,
                                                     std::vector<vk_image_view>&& WorldPositionImageViews,
                                                     std::vector<vk_image>&& NormalImages,
                                                     std::vector<vk_image_view>&& NormalImageViews);

  private:
    vk_instance Instance_;
    vk_surface Surface_;
    vk_physical_device PhysicalDevice_;
    vk_device Device_;
    vk_swap_chain SwapChain_;
    vk_command_pool GraphicsCommandPool_;

    vk_descriptor_pool DescriptorPool_;

    global_observer_renderer GlobalObserverRenderer_;
    gameplay_renderer GameplayRenderer_;

    vk_memory DeviceMemory_;
    vk_memory HostMemory_;

    vk_buffer CubeMeshVertexBuffer_;

    std::vector<vk_image> DepthBuffers_;
    std::vector<vk_image_view> DepthBufferViews_;
    std::vector<vk_framebuffer> Framebuffers_;

    std::vector<vk_buffer> MazeDeviceInstanceBuffers_;
    std::vector<vk_buffer> MazeHostInstanceBuffers_;

    std::vector<vk_buffer> DeviceVertexUniformBuffers_;
    std::vector<vk_buffer> HostVertexUniformBuffers_;

    std::vector<vk_buffer> DeviceFragmentUniformBuffers_;
    std::vector<vk_buffer> HostFragmentUniformBuffers_;

    std::vector<vk_command_buffer> GraphicsCommandBuffers_;
    std::vector<vk_semaphore> ImageAvailableSemaphores_;
    std::vector<vk_semaphore> RenderFinishedSemaphores_;
    std::vector<vk_fence> FrameInFlightFences_;

    u32 CurrentFrameNumber_;
    u32 CurrentFrameIndex_;

  };

}  // myran
