#include <myran/debug.h>

#include "renderer.h"

namespace myran {

  std::unique_ptr<renderer> renderer::Create(const window& Window)
  {
    LogInfo("Creating renderer...");

    auto Renderer = std::unique_ptr<renderer>(new renderer());
    if (!Renderer) return nullptr;

    std::vector<std::string> ValidationLayerNames = {
      "VK_LAYER_KHRONOS_validation",
    };

    std::vector<std::string> ExtensionNames = {

    };

    std::vector<std::string> DeviceExtensionNames = {
      VK_KHR_SWAPCHAIN_EXTENSION_NAME,
      VK_KHR_MAINTENANCE1_EXTENSION_NAME,
    };

    vk_application_info ApplicationInfo;
    ApplicationInfo.Name = "myran";
    ApplicationInfo.Version = {1, 0, 0};
    ApplicationInfo.EngineName = "myran";
    ApplicationInfo.EngineVersion = {1, 0, 0};
    ApplicationInfo.ApiVersion = {1, 2, 0};

    const std::vector<std::string>& SurfaceExtensions = Window.GetSurfaceExtensions();

    for (auto Extension : SurfaceExtensions)
      ExtensionNames.push_back(Extension);

    vk_instance Instance(ApplicationInfo, ExtensionNames, ValidationLayerNames);
    vk_surface Surface(Instance, Window);

    const std::vector<vk_physical_device>& PhysicalDevices = Instance.GetPhysicalDevices();
    Assert(PhysicalDevices.size() > 0);

    u32 PhysicalDeviceIndex = 0;
    for (; PhysicalDeviceIndex < PhysicalDevices.size(); ++PhysicalDeviceIndex)
    {
      const vk_physical_device& PhysicalDevice = PhysicalDevices[PhysicalDeviceIndex];

      if (PhysicalDevice.HasComputeQueue() &&
          PhysicalDevice.HasGraphicsQueue() &&
          PhysicalDevice.HasTransferQueue() &&
          PhysicalDevice.SupportsSurface(Surface))
        break;
    }

    if (PhysicalDeviceIndex >= PhysicalDevices.size())
    {
      LogError("This is really bad!");
      return nullptr;
    }

    vk_physical_device PhysicalDevice = PhysicalDevices[PhysicalDeviceIndex];
    PhysicalDevice.UpdatePresentQueue(Surface);

    vk_device Device(PhysicalDevice,
                     ValidationLayerNames,
                     DeviceExtensionNames);

    vk_swap_chain SwapChain(Device,
                            Surface,
                            PhysicalDevice,
                            {Window.GetScreenWidth(), Window.GetScreenHeight()},
                            2);

    vk_descriptor_pool DescriptorPool = vk_descriptor_pool::Create(Device);

    vk_command_pool GraphicsCommandPool(Device.GetGraphicsQueue().GetFamilyIndex(),
                                        Device);

    vk_memory DeviceMemory = vk_memory::CreateDeviceMemory(Device, PhysicalDevice, kDeviceMemorySize);
    vk_memory HostMemory = vk_memory::CreateHostMemory(Device, PhysicalDevice, kHostMemorySize);

    std::vector<vk_command_buffer> GraphicsCommandBuffers = GraphicsCommandPool.AllocateCommandBuffers(SwapChain.GetImageCount());

    std::vector<vk_semaphore> ImageAvailableSemaphores;
    std::vector<vk_semaphore> RenderFinishedSemaphores;
    std::vector<vk_fence> FrameInFlightFences;
    std::vector<vk_buffer> MazeDeviceInstanceBuffers;
    std::vector<vk_buffer> MazeHostInstanceBuffers;

    std::vector<vk_buffer> DeviceVertexUniformBuffers;
    std::vector<vk_buffer> HostVertexUniformBuffers;
    std::vector<vk_buffer> DeviceFragmentUniformBuffers;
    std::vector<vk_buffer> HostFragmentUniformBuffers;

    std::vector<vk_image> DepthBuffers;
    std::vector<vk_image> ColorImages;
    std::vector<vk_image> WorldPositionImages;
    std::vector<vk_image> NormalImages;
    std::vector<vk_framebuffer> Framebuffers;

    ImageAvailableSemaphores.reserve(SwapChain.GetImageCount());
    RenderFinishedSemaphores.reserve(SwapChain.GetImageCount());
    FrameInFlightFences.reserve(SwapChain.GetImageCount());
    MazeDeviceInstanceBuffers.reserve(SwapChain.GetImageCount());
    MazeHostInstanceBuffers.reserve(SwapChain.GetImageCount());

    DeviceVertexUniformBuffers.reserve(SwapChain.GetImageCount());
    HostVertexUniformBuffers.reserve(SwapChain.GetImageCount());

    DeviceFragmentUniformBuffers.reserve(SwapChain.GetImageCount());
    HostFragmentUniformBuffers.reserve(SwapChain.GetImageCount());

    DepthBuffers.reserve(SwapChain.GetImageCount());
    ColorImages.reserve(SwapChain.GetImageCount());
    WorldPositionImages.reserve(SwapChain.GetImageCount());
    NormalImages.reserve(SwapChain.GetImageCount());
    Framebuffers.reserve(SwapChain.GetImageCount());

    for (u32 Index = 0; Index < SwapChain.GetImageCount(); ++Index)
    {
      ImageAvailableSemaphores.emplace_back(vk_semaphore(Device));
      RenderFinishedSemaphores.emplace_back(vk_semaphore(Device));
      FrameInFlightFences.emplace_back(vk_fence(Device));

      MazeDeviceInstanceBuffers.emplace_back(vk_buffer::CreateVertexBuffer(Device, kInstanceBufferSize));
      MazeHostInstanceBuffers.emplace_back(vk_buffer::CreateStageBuffer(Device, kInstanceBufferSize));

      DeviceVertexUniformBuffers.emplace_back(vk_buffer::CreateUniformBuffer(Device, kUniformBufferSize));
      HostVertexUniformBuffers.emplace_back(vk_buffer::CreateStageBuffer(Device, kUniformBufferSize));

      DeviceFragmentUniformBuffers.emplace_back(vk_buffer::CreateUniformBuffer(Device, kUniformBufferSize));
      HostFragmentUniformBuffers.emplace_back(vk_buffer::CreateStageBuffer(Device, kUniformBufferSize));

      DepthBuffers.emplace_back(vk_image::CreateDepthBuffer(Device, SwapChain.GetImageExtent()));
      ColorImages.emplace_back(vk_image::CreateColor(Device, SwapChain.GetImageExtent()));
      WorldPositionImages.emplace_back(vk_image::CreateWorldPosition(Device, SwapChain.GetImageExtent()));
      NormalImages.emplace_back(vk_image::CreateNormal(Device, SwapChain.GetImageExtent()));
      Framebuffers.emplace_back(vk_framebuffer());
    }

    vk_buffer CubeMeshVertexBuffer = vk_buffer::CreateVertexBuffer(Device, kCubeMeshBufferSize);

    {
      VkDeviceSize DeviceMemoryOffset = 0;

      {
        DeviceMemoryOffset = CubeMeshVertexBuffer.GetNextAlignedOffset(DeviceMemoryOffset);
        CubeMeshVertexBuffer.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += CubeMeshVertexBuffer.GetRequiredSize();
      }

      for (auto& Buffer : MazeDeviceInstanceBuffers)
      {
        DeviceMemoryOffset = Buffer.GetNextAlignedOffset(DeviceMemoryOffset);
        Buffer.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Buffer : DeviceVertexUniformBuffers)
      {
        DeviceMemoryOffset = Buffer.GetNextAlignedOffset(DeviceMemoryOffset);
        Buffer.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Buffer : DeviceFragmentUniformBuffers)
      {
        DeviceMemoryOffset = Buffer.GetNextAlignedOffset(DeviceMemoryOffset);
        Buffer.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Buffer : DepthBuffers)
      {
        DeviceMemoryOffset = Buffer.GetNextAlignedOffset(DeviceMemoryOffset);
        Buffer.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Image : ColorImages)
      {
        DeviceMemoryOffset = Image.GetNextAlignedOffset(DeviceMemoryOffset);
        Image.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Image.GetRequiredSize();
      }

      for (auto& Image : WorldPositionImages)
      {
        DeviceMemoryOffset = Image.GetNextAlignedOffset(DeviceMemoryOffset);
        Image.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Image.GetRequiredSize();
      }

      for (auto& Image : NormalImages)
      {
        DeviceMemoryOffset = Image.GetNextAlignedOffset(DeviceMemoryOffset);
        Image.Bind(DeviceMemory, DeviceMemoryOffset);
        DeviceMemoryOffset += Image.GetRequiredSize();
      }

      VkDeviceSize HostMemoryOffset = 0;

      for (auto& Buffer : MazeHostInstanceBuffers)
      {
        HostMemoryOffset = Buffer.GetNextAlignedOffset(HostMemoryOffset);
        Buffer.Bind(HostMemory, HostMemoryOffset);
        HostMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Buffer : HostVertexUniformBuffers)
      {
        HostMemoryOffset = Buffer.GetNextAlignedOffset(HostMemoryOffset);
        Buffer.Bind(HostMemory, HostMemoryOffset);
        HostMemoryOffset += Buffer.GetRequiredSize();
      }

      for (auto& Buffer : HostFragmentUniformBuffers)
      {
        HostMemoryOffset = Buffer.GetNextAlignedOffset(HostMemoryOffset);
        Buffer.Bind(HostMemory, HostMemoryOffset);
        HostMemoryOffset += Buffer.GetRequiredSize();
      }
    }

    std::vector<vk_image_view> DepthBufferViews;
    std::vector<vk_image_view> ColorImageViews;
    std::vector<vk_image_view> WorldPositionImageViews;
    std::vector<vk_image_view> NormalImageViews;

    DepthBufferViews.reserve(DepthBuffers.size());
    ColorImageViews.reserve(DepthBuffers.size());
    WorldPositionImageViews.reserve(DepthBuffers.size());
    NormalImageViews.reserve(DepthBuffers.size());

    for (const auto& Buffer : DepthBuffers)
    {
      DepthBufferViews.emplace_back(vk_image_view::CreateDepthBuffer(Device, Buffer));
    }

    for (const auto& Image : ColorImages)
    {
      ColorImageViews.emplace_back(vk_image_view::CreateColor(Device, Image));
    }

    for (const auto& Image : WorldPositionImages)
    {
      WorldPositionImageViews.emplace_back(vk_image_view::CreateWorldPosition(Device, Image));
    }

    for (const auto& Image : NormalImages)
    {
      NormalImageViews.emplace_back(vk_image_view::CreateNormal(Device, Image));
    }

    // Transfer Quad Mesh
    {
      auto& CommandBuffer = GraphicsCommandBuffers[0];
      const auto& HostBuffer = MazeHostInstanceBuffers[0];
      const auto& DeviceBuffer = CubeMeshVertexBuffer;

      auto HostStack = HostMemory.GetStackView(HostBuffer.GetMemoryOffset());

      for (const auto Vertex : kCubeMesh)
        HostStack.Push(Vertex);

      const auto PushedSize = HostStack.GetPushedSize();

      CommandBuffer.Begin();
      const VkBufferCopy region = {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = PushedSize,
      };

      vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                      HostBuffer.GetHandle(),
                      DeviceBuffer.GetHandle(),
                      1,
                      &region);

      CommandBuffer.End();

      auto& Queue = Device.GetGraphicsQueue();
      Queue.Submit(CommandBuffer);
      Queue.Wait();
    }

    Renderer->Instance_ = std::move(Instance);
    Renderer->Surface_ = std::move(Surface);
    Renderer->PhysicalDevice_ = std::move(PhysicalDevice);
    Renderer->Device_ = std::move(Device);
    Renderer->SwapChain_ = std::move(SwapChain);
    Renderer->GraphicsCommandPool_ = std::move(GraphicsCommandPool);
    Renderer->GraphicsCommandBuffers_ = std::move(GraphicsCommandBuffers);
    Renderer->ImageAvailableSemaphores_ = std::move(ImageAvailableSemaphores);
    Renderer->RenderFinishedSemaphores_ = std::move(RenderFinishedSemaphores);
    Renderer->FrameInFlightFences_ = std::move(FrameInFlightFences);
    Renderer->MazeDeviceInstanceBuffers_ = std::move(MazeDeviceInstanceBuffers);
    Renderer->MazeHostInstanceBuffers_ = std::move(MazeHostInstanceBuffers);
    Renderer->DeviceVertexUniformBuffers_ = std::move(DeviceVertexUniformBuffers);
    Renderer->HostVertexUniformBuffers_ = std::move(HostVertexUniformBuffers);
    Renderer->DeviceFragmentUniformBuffers_ = std::move(DeviceFragmentUniformBuffers);
    Renderer->HostFragmentUniformBuffers_ = std::move(HostFragmentUniformBuffers);
    Renderer->CubeMeshVertexBuffer_ = std::move(CubeMeshVertexBuffer);
    Renderer->DepthBuffers_ = std::move(DepthBuffers);
    Renderer->DepthBufferViews_ = std::move(DepthBufferViews);
    Renderer->Framebuffers_ = std::move(Framebuffers);
    Renderer->DeviceMemory_ = std::move(DeviceMemory);
    Renderer->HostMemory_ = std::move(HostMemory);
    Renderer->DescriptorPool_ = std::move(DescriptorPool);
    Renderer->GlobalObserverRenderer_ = std::move(CreateGlobalObserverRenderer_(Renderer->Device_,
                                                                                Renderer->DescriptorPool_,
                                                                                Renderer->SwapChain_,
                                                                                Renderer->DeviceVertexUniformBuffers_,
                                                                                Renderer->DeviceFragmentUniformBuffers_,
                                                                                Renderer->DepthBuffers_));
    Renderer->GameplayRenderer_ = std::move(CreateGameplayRenderer_(Renderer->Device_,
                                                                    Renderer->DescriptorPool_,
                                                                    Renderer->SwapChain_,
                                                                    Renderer->DeviceVertexUniformBuffers_,
                                                                    Renderer->DeviceFragmentUniformBuffers_,
                                                                    Renderer->DepthBuffers_,
                                                                    std::move(ColorImages),
                                                                    std::move(ColorImageViews),
                                                                    std::move(WorldPositionImages),
                                                                    std::move(WorldPositionImageViews),
                                                                    std::move(NormalImages),
                                                                    std::move(NormalImageViews)
                                              ));
    Renderer->CurrentFrameNumber_ = 0;
    Renderer->CurrentFrameIndex_ = 0;
    return Renderer;
  }

  renderer::global_observer_renderer renderer::CreateGlobalObserverRenderer_(const vk_device& Device,
                                                                             const vk_descriptor_pool& DescriptorPool,
                                                                             const vk_swap_chain& SwapChain,
                                                                             const std::vector<vk_buffer>& VertexUniformBuffers,
                                                                             const std::vector<vk_buffer>& FragmentUniformBuffers,
                                                                             const std::vector<vk_image>& DepthBuffers)
  {
    global_observer_renderer Result;
    Result.RenderPass = vk_render_pass::CreateGlobalObserver(Device, SwapChain, DepthBuffers[0]);
    Result.DescriptorSetLayout = vk_descriptor_set_layout::CreateGlobalObserver(Device);
    Result.PipelineLayout = vk_pipeline_layout::CreateGlobalObserver(Device, Result.DescriptorSetLayout);
    Result.Pipeline = vk_graphics_pipeline::CreateGlobalObserver(Device, Result.PipelineLayout, Result.RenderPass, SwapChain.GetImageExtent());

    Result.DescriptorSets.reserve(SwapChain.GetImageCount());

    for (u32 Index = 0; Index < SwapChain.GetImageCount(); ++Index)
    {
      Result.DescriptorSets.emplace_back(vk_descriptor_set::Create(Device, DescriptorPool, Result.DescriptorSetLayout));
    }

    for (u32 Index = 0; Index < SwapChain.GetImageCount(); ++Index)
    {
      const auto& DescriptorSet = Result.DescriptorSets[Index];
      const auto& VertexUniformBuffer = VertexUniformBuffers[Index];
      const auto& FragmentUniformBuffer = FragmentUniformBuffers[Index];

      const VkDescriptorBufferInfo VertexUniformBufferDescription = {
        .buffer = VertexUniformBuffer.GetHandle(),
        .offset = 0,
        .range = VK_WHOLE_SIZE,
      };

      const VkDescriptorBufferInfo FragmentUniformBufferDescription = {
        .buffer = FragmentUniformBuffer.GetHandle(),
        .offset = 0,
        .range = VK_WHOLE_SIZE,
      };

      std::vector<VkWriteDescriptorSet> Writes;

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 0;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        Write.pBufferInfo = &VertexUniformBufferDescription;

        Writes.push_back(Write);
      }

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 1;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        Write.pBufferInfo = &FragmentUniformBufferDescription;

        Writes.push_back(Write);
      }

      vkUpdateDescriptorSets(Device.GetHandle(),
                             Writes.size(),
                             Writes.data(),
                             0,
                             nullptr);
    }

    return Result;
  }

  renderer::gameplay_renderer renderer::CreateGameplayRenderer_(const vk_device& Device,
                                                                const vk_descriptor_pool& DescriptorPool,
                                                                const vk_swap_chain& SwapChain,
                                                                const std::vector<vk_buffer>& VertexUniformBuffers,
                                                                const std::vector<vk_buffer>& FragmentUniformBuffers,
                                                                const std::vector<vk_image>& DepthBuffers,
                                                                std::vector<vk_image>&& ColorImages,
                                                                std::vector<vk_image_view>&& ColorImageViews,
                                                                std::vector<vk_image>&& WorldPositionImages,
                                                                std::vector<vk_image_view>&& WorldPositionImageViews,
                                                                std::vector<vk_image>&& NormalImages,
                                                                std::vector<vk_image_view>&& NormalImageViews)
  {
    const auto ImageSize = SwapChain.GetImageExtent();
    auto MinimapSize = ImageSize;
    MinimapSize.width /= kMinimapScaleFactor;
    MinimapSize.height /= kMinimapScaleFactor;

    gameplay_renderer Result;
    Result.RenderPass = vk_render_pass::CreateGameplay(Device, SwapChain, DepthBuffers[0], ColorImages[0], WorldPositionImages[0], NormalImages[0]);
    Result.DescriptorSetLayout = vk_descriptor_set_layout::CreateGameplay(Device);
    Result.PipelineLayout = vk_pipeline_layout::CreateGameplay(Device, Result.DescriptorSetLayout);
    Result.Offscreen_Pipeline = vk_graphics_pipeline::CreateGameplay_Offscreen(Device,
                                                                               Result.PipelineLayout,
                                                                               Result.RenderPass,
                                                                               ImageSize);
    Result.Onscreen_Pipeline = vk_graphics_pipeline::CreateGameplay_Onscreen(Device,
                                                                             Result.PipelineLayout,
                                                                             Result.RenderPass,
                                                                             ImageSize);
    Result.Minimap_Pipeline = vk_graphics_pipeline::CreateGameplay_Minimap(Device,
                                                                           Result.PipelineLayout,
                                                                           Result.RenderPass,
                                                                           ImageSize,
                                                                           MinimapSize);
    Result.Sampler = vk_sampler::CreateGameplay(Device);
    Result.ColorImages = std::move(ColorImages);
    Result.ColorImageViews = std::move(ColorImageViews);
    Result.WorldPositionImages = std::move(WorldPositionImages);
    Result.WorldPositionImageViews = std::move(WorldPositionImageViews);
    Result.NormalImages = std::move(NormalImages);
    Result.NormalImageViews = std::move(NormalImageViews);

    Result.DescriptorSets.reserve(SwapChain.GetImageCount());

    for (u32 Index = 0; Index < SwapChain.GetImageCount(); ++Index)
    {
      Result.DescriptorSets.emplace_back(vk_descriptor_set::Create(Device, DescriptorPool, Result.DescriptorSetLayout));
    }

    for (u32 Index = 0; Index < SwapChain.GetImageCount(); ++Index)
    {
      const auto& DescriptorSet = Result.DescriptorSets[Index];
      const auto& VertexUniformBuffer = VertexUniformBuffers[Index];
      const auto& FragmentUniformBuffer = FragmentUniformBuffers[Index];
      const auto& ColorImageView = Result.ColorImageViews[Index];
      const auto& WorldPositionImageView = Result.WorldPositionImageViews[Index];
      const auto& NormalImageView = Result.NormalImageViews[Index];

      const VkDescriptorBufferInfo VertexUniformBufferDescription = {
        .buffer = VertexUniformBuffer.GetHandle(),
        .offset = 0,
        .range = VK_WHOLE_SIZE,
      };

      const VkDescriptorBufferInfo FragmentUniformBufferDescription = {
        .buffer = FragmentUniformBuffer.GetHandle(),
        .offset = 0,
        .range = VK_WHOLE_SIZE,
      };

      const VkDescriptorImageInfo ColorImageDescription = {
        .sampler = VK_NULL_HANDLE,
        .imageView = ColorImageView.GetHandle(),
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      };

      const VkDescriptorImageInfo WorldPositionImageDescription = {
        .sampler = VK_NULL_HANDLE,
        .imageView = WorldPositionImageView.GetHandle(),
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      };

      const VkDescriptorImageInfo NormalImageDescription = {
        .sampler = VK_NULL_HANDLE,
        .imageView = NormalImageView.GetHandle(),
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      };

      std::vector<VkWriteDescriptorSet> Writes;

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 0;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        Write.pBufferInfo = &VertexUniformBufferDescription;

        Writes.push_back(Write);
      }

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 1;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        Write.pBufferInfo = &FragmentUniformBufferDescription;

        Writes.push_back(Write);
      }

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 2;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        Write.pImageInfo = &ColorImageDescription;

        Writes.push_back(Write);
      }

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 3;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        Write.pImageInfo = &WorldPositionImageDescription;

        Writes.push_back(Write);
      }

      {
        VkWriteDescriptorSet Write = GetWriteDescriptorSet();
        Write.dstSet = DescriptorSet.GetHandle();
        Write.dstBinding = 4;
        Write.dstArrayElement = 0;
        Write.descriptorCount = 1;
        Write.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
        Write.pImageInfo = &NormalImageDescription;

        Writes.push_back(Write);
      }

      vkUpdateDescriptorSets(Device.GetHandle(),
                             Writes.size(),
                             Writes.data(),
                             0,
                             nullptr);
    }

    return Result;
  }

  void renderer::StartFrame()
  {
    vk_swap_chain::image CurrentImage = SwapChain_.AcquireNextImage(ImageAvailableSemaphores_[CurrentFrameIndex_]);
    if (CurrentImage.Status.IsError()) return;

    FrameInFlightFences_[CurrentFrameIndex_].WaitAndReset();

    GraphicsCommandBuffers_[CurrentFrameIndex_].Begin();
  }

  void renderer::RenderFrame()
  {
    auto& CommandBuffer = GraphicsCommandBuffers_[CurrentFrameIndex_];

    CommandBuffer.End();

    {
      const vk_status Status =
        Device_.GetGraphicsQueue().Submit(CommandBuffer,
                                          VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                          ImageAvailableSemaphores_[CurrentFrameIndex_],
                                          RenderFinishedSemaphores_[CurrentFrameIndex_],
                                          FrameInFlightFences_[CurrentFrameIndex_]);
      if (Status.IsError()) return;
    }

    {
      const vk_status Status =
        Device_.GetPresentQueue().Present(SwapChain_, RenderFinishedSemaphores_[CurrentFrameIndex_]);
      if (Status.IsError()) return;
    }


    CurrentFrameNumber_++;
    CurrentFrameIndex_ = (CurrentFrameIndex_ + 1) % SwapChain_.GetImageCount();
  }

  void renderer::WaitToFinish()
  {
    Device_.Wait();
  }

  void renderer::RenderGlobalObserver(const first_person_camera& Camera,
                                      const glm::vec3 PlayerPosition,
                                      const maze_geometry& MazeGeometry)

  {
    const auto& CommandBuffer = GraphicsCommandBuffers_[CurrentFrameIndex_];

    const auto& InstanceHostBuffer = MazeHostInstanceBuffers_[CurrentFrameIndex_];
    const auto& VertexUniformHostBuffer = HostVertexUniformBuffers_[CurrentFrameIndex_];
    const auto& FragmentUniformHostBuffer = HostFragmentUniformBuffers_[CurrentFrameIndex_];

    const auto& InstanceDeviceBuffer = MazeDeviceInstanceBuffers_[CurrentFrameIndex_];
    const auto& VertexUniformDeviceBuffer = DeviceVertexUniformBuffers_[CurrentFrameIndex_];
    const auto& FragmentUniformDeviceBuffer = DeviceFragmentUniformBuffers_[CurrentFrameIndex_];

    const color FloorColor = { 255, 0, 0, 255 };
    const color WallColor = { 0, 0, 255, 255 };

    auto InstanceHostStack = HostMemory_.GetStackView(InstanceHostBuffer.GetMemoryOffset());
    auto InstanceCount = 0;

    for (const auto& FloorBlock : MazeGeometry.FloorBlocks)
    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, FloorBlock.Center);
      Transform = glm::scale(Transform, FloorBlock.Size);

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(FloorColor);

      InstanceCount++;

      if (FloorBlock.HasItem)
      {
        auto Position = FloorBlock.Center;
        Position.y = kItemY;

        glm::mat4 Transform = glm::mat4(1.0f);
        Transform = glm::translate(Transform, Position);
        Transform = glm::scale(Transform, { kItemSize, kItemSize, kItemSize});

        InstanceHostStack.Push(Transform);
        InstanceHostStack.Push(kItemColor);

        InstanceCount++;
      }
    }

    for (const auto& WallBlock : MazeGeometry.WallBlocks)
    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, WallBlock.Center);
      Transform = glm::scale(Transform, WallBlock.Size);

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(WallColor);

      InstanceCount++;
    }


    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, PlayerPosition);
      Transform = glm::scale(Transform, { kPlayerSize, kPlayerSize, kPlayerSize });

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(kPlayerColor);

      InstanceCount++;
    }

    const auto InstancePushedSize = InstanceHostStack.GetPushedSize();

    auto VertexUniformHostStack = HostMemory_.GetStackView(VertexUniformHostBuffer.GetMemoryOffset());

    VertexUniformHostStack.Push(Camera.GetView());
    VertexUniformHostStack.Push(Camera.GetProjection());
    VertexUniformHostStack.Push(Camera.GetProjectionView());

    const auto VertexUniformPushedSize = VertexUniformHostStack.GetPushedSize();

    auto FragmentUniformHostStack = HostMemory_.GetStackView(FragmentUniformHostBuffer.GetMemoryOffset());

    FragmentUniformHostStack.Push(Camera.Position);

    const auto FragmentUniformPushedSize = FragmentUniformHostStack.GetPushedSize();

    // Copy To Device
    {
      std::vector<VkBufferMemoryBarrier> PreCopyBarriers;
      std::vector<VkBufferMemoryBarrier> PostCopyBarriers;

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = InstanceHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = VertexUniformHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = FragmentUniformHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = InstanceDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
        Barrier.buffer = InstanceDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = VertexUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
        Barrier.buffer = VertexUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = FragmentUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
        Barrier.buffer = FragmentUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      vkCmdPipelineBarrier(CommandBuffer.GetHandle(),
                           VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           0,
                           0,
                           nullptr,
                           PreCopyBarriers.size(),
                           PreCopyBarriers.data(),
                           0,
                           nullptr);

      if (InstancePushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = InstancePushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        InstanceHostBuffer.GetHandle(),
                        InstanceDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      if (VertexUniformPushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = VertexUniformPushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        VertexUniformHostBuffer.GetHandle(),
                        VertexUniformDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      if (FragmentUniformPushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = FragmentUniformPushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        FragmentUniformHostBuffer.GetHandle(),
                        FragmentUniformDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      vkCmdPipelineBarrier(CommandBuffer.GetHandle(),
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           VK_PIPELINE_STAGE_VERTEX_INPUT_BIT |
                           VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
                           0,
                           0,
                           nullptr,
                           PostCopyBarriers.size(),
                           PostCopyBarriers.data(),
                           0,
                           nullptr);
    }

    // Bind And Draw
    {
      const auto DescriptorSetHandle = GlobalObserverRenderer_.DescriptorSets[CurrentFrameIndex_].GetHandle();

      const auto& RenderPass = GlobalObserverRenderer_.RenderPass;
      const auto& DepthBufferView = DepthBufferViews_[CurrentFrameIndex_];
      const auto& ColorImageView = SwapChain_.GetCurrentImageView();
      const auto ImageExtent = SwapChain_.GetImageExtent();

      auto& Framebuffer = Framebuffers_[CurrentFrameIndex_];
      Framebuffer = vk_framebuffer::CreateGlobalObserver(Device_,
                                                         RenderPass,
                                                         ColorImageView,
                                                         DepthBufferView,
                                                         ImageExtent);

      const std::array<VkClearValue, 2> ClearValues = {
        VkClearValue
        {
          .color = {
            0.2f, 0.25f, 0.25f, 1.0f,
          }
        },
        {
          .depthStencil = {
            .depth = 1.0f, // @note: Maximum depth
            .stencil = 0,
          }
        }
      };

      VkRenderPassBeginInfo BeginInfo = GetRenderPassBeginInfo();
      BeginInfo.renderPass = RenderPass.GetHandle();
      BeginInfo.framebuffer = Framebuffer.GetHandle();
      BeginInfo.renderArea = {
        {
          0,
          0,
        },
        ImageExtent,
      };
      BeginInfo.clearValueCount = ClearValues.size();
      BeginInfo.pClearValues = ClearValues.data();

      vkCmdBeginRenderPass(CommandBuffer.GetHandle(),
                           &BeginInfo,
                           VK_SUBPASS_CONTENTS_INLINE);

      vkCmdBindDescriptorSets(CommandBuffer.GetHandle(),
                              VK_PIPELINE_BIND_POINT_GRAPHICS,
                              GlobalObserverRenderer_.PipelineLayout.GetHandle(),
                              0,
                              1,
                              &DescriptorSetHandle,
                              0,
                              nullptr);
      vkCmdBindPipeline(CommandBuffer.GetHandle(),
                        VK_PIPELINE_BIND_POINT_GRAPHICS,
                        GlobalObserverRenderer_.Pipeline.GetHandle());

      const std::array<VkBuffer, 2> VertexBufferHandles = {
        CubeMeshVertexBuffer_.GetHandle(),
        InstanceDeviceBuffer.GetHandle(),
      };

      const std::array<VkDeviceSize, 2> VertexBufferOffsets = {
        0,
        0
      };

      vkCmdBindVertexBuffers(CommandBuffer.GetHandle(),
                             0,
                             VertexBufferHandles.size(),
                             VertexBufferHandles.data(),
                             VertexBufferOffsets.data());
      vkCmdDraw(CommandBuffer.GetHandle(),
                kCubeMesh.size(),
                InstanceCount,
                0,
                0);
      vkCmdEndRenderPass(CommandBuffer.GetHandle());
    }
  }

  void renderer::RenderGameplay(const first_person_camera& PlayerCamera,
                                const minimap_camera& MinimapCamera,
                                const maze_geometry& MazeGeometry)

  {
    const auto& CommandBuffer = GraphicsCommandBuffers_[CurrentFrameIndex_];

    const auto& InstanceHostBuffer = MazeHostInstanceBuffers_[CurrentFrameIndex_];
    const auto& VertexUniformHostBuffer = HostVertexUniformBuffers_[CurrentFrameIndex_];
    const auto& FragmentUniformHostBuffer = HostFragmentUniformBuffers_[CurrentFrameIndex_];

    const auto& InstanceDeviceBuffer = MazeDeviceInstanceBuffers_[CurrentFrameIndex_];
    const auto& VertexUniformDeviceBuffer = DeviceVertexUniformBuffers_[CurrentFrameIndex_];
    const auto& FragmentUniformDeviceBuffer = DeviceFragmentUniformBuffers_[CurrentFrameIndex_];

    const color FloorColor = { 255, 0, 0, 255 };
    const color WallColor = { 0, 0, 255, 255 };

    auto InstanceHostStack = HostMemory_.GetStackView(InstanceHostBuffer.GetMemoryOffset());
    auto InstanceCount = 0;

    for (const auto& FloorBlock : MazeGeometry.FloorBlocks)
    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, FloorBlock.Center);
      Transform = glm::scale(Transform, FloorBlock.Size);

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(FloorColor);
      InstanceHostStack.Push(static_cast<s8>(FloorBlock.IsVisited));
      // @note: Alignment
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);

      InstanceCount++;

      if (FloorBlock.HasItem)
      {
        auto Position = FloorBlock.Center;
        Position.y = kItemY;

        glm::mat4 Transform = glm::mat4(1.0f);
        Transform = glm::translate(Transform, Position);
        Transform = glm::scale(Transform, { kItemSize, kItemSize, kItemSize});

        InstanceHostStack.Push(Transform);
        InstanceHostStack.Push(kItemColor);
        InstanceHostStack.Push((s8) 0);
        InstanceHostStack.Push((s8) 0);
        InstanceHostStack.Push((s8) 0);
        InstanceHostStack.Push((s8) 0);

        InstanceCount++;
      }
    }

    for (const auto& WallBlock : MazeGeometry.WallBlocks)
    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, WallBlock.Center);
      Transform = glm::scale(Transform, WallBlock.Size);

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(WallColor);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);

      InstanceCount++;
    }

    {
      glm::mat4 Transform = glm::mat4(1.0f);
      Transform = glm::translate(Transform, PlayerCamera.Position);
      Transform = glm::scale(Transform, { kPlayerSize, kPlayerSize, kPlayerSize });

      InstanceHostStack.Push(Transform);
      InstanceHostStack.Push(kPlayerColor);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);
      InstanceHostStack.Push((s8) 0);

      InstanceCount++;
    }

    const auto InstancePushedSize = InstanceHostStack.GetPushedSize();

    auto VertexUniformHostStack = HostMemory_.GetStackView(VertexUniformHostBuffer.GetMemoryOffset());

    VertexUniformHostStack.Push(PlayerCamera.GetView());
    VertexUniformHostStack.Push(PlayerCamera.GetProjection());
    VertexUniformHostStack.Push(PlayerCamera.GetProjectionView());

    VertexUniformHostStack.Push(MinimapCamera.GetView());
    VertexUniformHostStack.Push(MinimapCamera.GetProjection());
    VertexUniformHostStack.Push(MinimapCamera.GetProjectionView());

    const auto VertexUniformPushedSize = VertexUniformHostStack.GetPushedSize();

    auto FragmentUniformHostStack = HostMemory_.GetStackView(FragmentUniformHostBuffer.GetMemoryOffset());

    FragmentUniformHostStack.Push(PlayerCamera.Position);
    FragmentUniformHostStack.Push(0.0f); // @note: Alignment
    FragmentUniformHostStack.Push(PlayerCamera.Forward);
    FragmentUniformHostStack.Push(0.0f);
    FragmentUniformHostStack.Push(SwapChain_.GetImageExtent().width);
    FragmentUniformHostStack.Push(SwapChain_.GetImageExtent().height);
    FragmentUniformHostStack.Push(0.0f);
    FragmentUniformHostStack.Push(0.0f);
    FragmentUniformHostStack.Push(MinimapCamera.Position);
    FragmentUniformHostStack.Push(0.0f);

    const auto FragmentUniformPushedSize = FragmentUniformHostStack.GetPushedSize();

    // Copy To Device
    {
      std::vector<VkBufferMemoryBarrier> PreCopyBarriers;
      std::vector<VkBufferMemoryBarrier> PostCopyBarriers;

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = InstanceHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = VertexUniformHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        Barrier.buffer = FragmentUniformHostBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = InstanceDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;
        Barrier.buffer = InstanceDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = VertexUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
        Barrier.buffer = VertexUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = 0;
        Barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.buffer = FragmentUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PreCopyBarriers.push_back(Barrier);
      }

      {
        VkBufferMemoryBarrier Barrier = GetBufferMemoryBarrier();
        Barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        Barrier.dstAccessMask = VK_ACCESS_UNIFORM_READ_BIT;
        Barrier.buffer = FragmentUniformDeviceBuffer.GetHandle();
        Barrier.offset = 0;
        Barrier.size = VK_WHOLE_SIZE;

        PostCopyBarriers.push_back(Barrier);
      }

      vkCmdPipelineBarrier(CommandBuffer.GetHandle(),
                           VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           0,
                           0,
                           nullptr,
                           PreCopyBarriers.size(),
                           PreCopyBarriers.data(),
                           0,
                           nullptr);

      if (InstancePushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = InstancePushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        InstanceHostBuffer.GetHandle(),
                        InstanceDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      if (VertexUniformPushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = VertexUniformPushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        VertexUniformHostBuffer.GetHandle(),
                        VertexUniformDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      if (FragmentUniformPushedSize > 0)
      {
        const VkBufferCopy region = {
          .srcOffset = 0,
          .dstOffset = 0,
          .size = FragmentUniformPushedSize,
        };

        vkCmdCopyBuffer(CommandBuffer.GetHandle(),
                        FragmentUniformHostBuffer.GetHandle(),
                        FragmentUniformDeviceBuffer.GetHandle(),
                        1,
                        &region);
      }

      vkCmdPipelineBarrier(CommandBuffer.GetHandle(),
                           VK_PIPELINE_STAGE_TRANSFER_BIT,
                           VK_PIPELINE_STAGE_VERTEX_INPUT_BIT |
                           VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
                           0,
                           0,
                           nullptr,
                           PostCopyBarriers.size(),
                           PostCopyBarriers.data(),
                           0,
                           nullptr);
    }

    // Bind And Draw
    {
      const auto DescriptorSetHandle = GameplayRenderer_.DescriptorSets[CurrentFrameIndex_].GetHandle();

      const auto& RenderPass = GameplayRenderer_.RenderPass;
      const auto& DepthBufferView = DepthBufferViews_[CurrentFrameIndex_];
      const auto& OutputColorImageView = SwapChain_.GetCurrentImageView();
      const auto& ColorImageView = GameplayRenderer_.ColorImageViews[CurrentFrameIndex_];
      const auto& WorldPositionImageView = GameplayRenderer_.WorldPositionImageViews[CurrentFrameIndex_];
      const auto& NormalImageView = GameplayRenderer_.NormalImageViews[CurrentFrameIndex_];
      const auto ImageExtent = SwapChain_.GetImageExtent();

      auto& Framebuffer = Framebuffers_[CurrentFrameIndex_];
      Framebuffer = vk_framebuffer::CreateGameplay(Device_,
                                                   RenderPass,
                                                   OutputColorImageView,
                                                   DepthBufferView,
                                                   ColorImageView,
                                                   WorldPositionImageView,
                                                   NormalImageView,
                                                   ImageExtent);

      const std::array<VkClearValue, 5> ClearValues = {
        VkClearValue
        {
          .color = {
            0.2f, 0.25f, 0.25f, 1.0f,
          }
        },
        {
          .depthStencil = {
            .depth = 1.0f, // @note: Maximum depth
            .stencil = 0,
          }
        },
        {
          .color = {
            0.0f, 0.0f, 0.0f, 0.0f,
          }
        },
        {
          .color = {
            0.0f, 0.0f, 0.0f, 0.0f,
          }
        },
        {
          .color = {
            0.0f, 0.0f, 0.0f, 0.0f,
          }
        },
      };

      VkRenderPassBeginInfo BeginInfo = GetRenderPassBeginInfo();
      BeginInfo.renderPass = RenderPass.GetHandle();
      BeginInfo.framebuffer = Framebuffer.GetHandle();
      BeginInfo.renderArea = {
        {
          0,
          0,
        },
        ImageExtent,
      };
      BeginInfo.clearValueCount = ClearValues.size();
      BeginInfo.pClearValues = ClearValues.data();

      const std::array<VkBuffer, 2> VertexBufferHandles = {
        CubeMeshVertexBuffer_.GetHandle(),
        InstanceDeviceBuffer.GetHandle(),
      };

      const std::array<VkDeviceSize, 2> VertexBufferOffsets = {
        0,
        0
      };

      vkCmdBindVertexBuffers(CommandBuffer.GetHandle(),
                             0,
                             VertexBufferHandles.size(),
                             VertexBufferHandles.data(),
                             VertexBufferOffsets.data());

      vkCmdBindDescriptorSets(CommandBuffer.GetHandle(),
                              VK_PIPELINE_BIND_POINT_GRAPHICS,
                              GameplayRenderer_.PipelineLayout.GetHandle(),
                              0,
                              1,
                              &DescriptorSetHandle,
                              0,
                              nullptr);

      vkCmdBeginRenderPass(CommandBuffer.GetHandle(),
                           &BeginInfo,
                           VK_SUBPASS_CONTENTS_INLINE);

      // Offscreen
      {
        vkCmdBindPipeline(CommandBuffer.GetHandle(),
                          VK_PIPELINE_BIND_POINT_GRAPHICS,
                          GameplayRenderer_.Offscreen_Pipeline.GetHandle());

        vkCmdDraw(CommandBuffer.GetHandle(),
                  kCubeMesh.size(),
                  InstanceCount - 1, // @note: Avoid rendering player
                  0,
                  0);
      }

      vkCmdNextSubpass(CommandBuffer.GetHandle(),
                       VK_SUBPASS_CONTENTS_INLINE);

      // Onscreen
      {
        vkCmdBindPipeline(CommandBuffer.GetHandle(),
                          VK_PIPELINE_BIND_POINT_GRAPHICS,
                          GameplayRenderer_.Onscreen_Pipeline.GetHandle());

        vkCmdDraw(CommandBuffer.GetHandle(), 3, 1, 0, 0);
      }

      vkCmdNextSubpass(CommandBuffer.GetHandle(),
                       VK_SUBPASS_CONTENTS_INLINE);

      // Minimap
      {
        vkCmdBindPipeline(CommandBuffer.GetHandle(),
                          VK_PIPELINE_BIND_POINT_GRAPHICS,
                          GameplayRenderer_.Minimap_Pipeline.GetHandle());

        const VkClearAttachment ClearAttachment {
          .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
          .colorAttachment = 1,
          .clearValue {
            .depthStencil = {
              .depth = 1.0f, // @note: Maximum depth
              .stencil = 0,
            }
          }
        };

        const VkClearRect ClearArea = {
          .rect = {
            .offset = {
              .x = 0,
              .y = 0,
            },
            .extent = ImageExtent,
          },
          .baseArrayLayer = 0,
          .layerCount = 1,
        };

        vkCmdClearAttachments(CommandBuffer.GetHandle(),
                              1,
                              &ClearAttachment,
                              1,
                              &ClearArea);

        vkCmdDraw(CommandBuffer.GetHandle(),
                  kCubeMesh.size(),
                  InstanceCount,
                  0,
                  0);
      }

      vkCmdEndRenderPass(CommandBuffer.GetHandle());
    }
  }

}  // myran
