#include "device.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_device::vk_device(vk_physical_device& PhysicalDevice,
                       const std::vector<std::string>& ValidationLayerNames,
                       const std::vector<std::string>& ExtensionNames)
    : ValidationLayerNames_(ValidationLayerNames),
      ExtensionNames_(ExtensionNames)
  {
    std::vector<VkDeviceQueueCreateInfo> QueueCreateInfoList;
    QueueCreateInfoList.reserve(PhysicalDevice.GetQueueFamilyIndices().size());

    f32 QueuePriority = 1.0f;

    for (s32 QueueFamilyIndex : PhysicalDevice.GetQueueFamilyIndices())
    {
      VkDeviceQueueCreateInfo QueueCreateInfo = GetDeviceQueueCreateInfo();
      QueueCreateInfo.queueFamilyIndex = QueueFamilyIndex;
      QueueCreateInfo.queueCount = 1;
      QueueCreateInfo.pQueuePriorities = &QueuePriority;

      QueueCreateInfoList.push_back(QueueCreateInfo);
    }

    std::vector<const char*> ExtensionStringNames;
    ExtensionStringNames.reserve(ExtensionNames_.size());

    for (const auto& Name : ExtensionNames_)
      ExtensionStringNames.push_back(Name.c_str());

    std::vector<const char*> ValidationLayerStringNames;
    ValidationLayerStringNames.reserve(ValidationLayerNames_.size());

    for (const auto& Name : ValidationLayerNames_)
      ValidationLayerStringNames.push_back(Name.c_str());

    // @note We enable all the features
    VkPhysicalDeviceFeatures DeviceFeatures = PhysicalDevice.GetFeatures();

    VkDeviceCreateInfo CreateInfo = GetDeviceCreateInfo();
    CreateInfo.queueCreateInfoCount = QueueCreateInfoList.size();
    CreateInfo.pQueueCreateInfos = QueueCreateInfoList.data();
    CreateInfo.pEnabledFeatures = &DeviceFeatures;
    CreateInfo.enabledExtensionCount = ExtensionStringNames.size();
    CreateInfo.ppEnabledExtensionNames = ExtensionStringNames.data();
    CreateInfo.enabledLayerCount = ValidationLayerStringNames.size();
    CreateInfo.ppEnabledLayerNames = ValidationLayerStringNames.data();

    vk_status Status;
    Status = vkCreateDevice(PhysicalDevice.GetHandle(),
                            &CreateInfo,
                            VK_NULL_HANDLE,
                            &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_logical_device: " << Status);
      return;
    }

    AddQueues(PhysicalDevice);
  }

  vk_device::vk_device(vk_device&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(ValidationLayerNames_, Other.ValidationLayerNames_);
    std::swap(ExtensionNames_, Other.ExtensionNames_);
    std::swap(Queues_, Other.Queues_);
  }

  vk_device& vk_device::operator=(vk_device&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(ValidationLayerNames_, Rhs.ValidationLayerNames_);
    std::swap(ExtensionNames_, Rhs.ExtensionNames_);
    std::swap(Queues_, Rhs.Queues_);
    return *this;
  }

  vk_device::~vk_device()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_device...");
      vkDestroyDevice(Handle_, VK_NULL_HANDLE);
    }
  }

  vk_status vk_device::Wait() const
  {
    vk_status Status;
    Status = vkDeviceWaitIdle(Handle_);

    if (Status.IsError())
      LogError("Failed to wait for device to become idle: " << Status);

    return Status;
  }

  VkDevice vk_device::GetHandle() const
  {
    return Handle_;
  }

  const std::vector<std::string>& vk_device::GetValidationLayerNames() const
  {
    return ValidationLayerNames_;
  }

  const std::vector<std::string>& vk_device::GetExtensionNames() const
  {
    return ExtensionNames_;
  }

  const vk_queue& vk_device::GetComputeQueue() const
  {
    return Queues_.at(queue_family::Compute);
  }

  const vk_queue& vk_device::GetGraphicsQueue() const
  {
    return Queues_.at(queue_family::Graphics);
  }

  const vk_queue& vk_device::GetPresentQueue() const
  {
    return Queues_.at(queue_family::Present);
  }

  const vk_queue& vk_device::GetTransferQueue() const
  {
    return Queues_.at(queue_family::Transfer);
  }

  vk_queue& vk_device::GetComputeQueue()
  {
    return Queues_.at(queue_family::Compute);
  }

  vk_queue& vk_device::GetGraphicsQueue()
  {
    return Queues_.at(queue_family::Graphics);
  }

  vk_queue& vk_device::GetPresentQueue()
  {
    return Queues_.at(queue_family::Present);
  }

  vk_queue& vk_device::GetTransferQueue()
  {
    return Queues_.at(queue_family::Transfer);
  }

  void vk_device::AddQueues(const vk_physical_device& PhysicalDevice)
  {
    AddQueue(queue_family::Compute, PhysicalDevice.GetComputeQueueFamilyIndex());
    AddQueue(queue_family::Graphics, PhysicalDevice.GetGraphicsQueueFamilyIndex());
    AddQueue(queue_family::Present, PhysicalDevice.GetPresentQueueFamilyIndex());
    AddQueue(queue_family::Transfer, PhysicalDevice.GetTransferQueueFamilyIndex());
  }

  void vk_device::AddQueue(const queue_family Family, const s32 Index)
  {
    VkQueue Handle;
    vkGetDeviceQueue(Handle_,
                     Index,
                     0,
                     &Handle);

    Queues_.emplace(std::make_pair(Family, vk_queue(Handle, Index)));
  }

}  // myran
