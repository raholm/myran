#pragma once

#include <string>
#include <vector>
#include <unordered_map>

#include "physical_device.h"
#include "queue.h"
#include "status.h"

namespace myran {

  class vk_device final
  {
  public:
    vk_device() = default;
    vk_device(vk_physical_device& PhysicalDevice,
              const std::vector<std::string>& ValidationLayerNames,
              const std::vector<std::string>& ExtensionNames);

    vk_device(const vk_device& Other) = delete;
    vk_device& operator=(const vk_device& Rhs)= delete;

    vk_device(vk_device&& Other);
    vk_device& operator=(vk_device&& Rhs);

    ~vk_device();

    vk_status Wait() const;

    VkDevice GetHandle() const;
    const std::vector<std::string>& GetValidationLayerNames() const;
    const std::vector<std::string>& GetExtensionNames() const;

    const vk_queue& GetComputeQueue() const;
    const vk_queue& GetGraphicsQueue() const;
    const vk_queue& GetPresentQueue() const;
    const vk_queue& GetTransferQueue() const;

    vk_queue& GetComputeQueue();
    vk_queue& GetGraphicsQueue();
    vk_queue& GetPresentQueue();
    vk_queue& GetTransferQueue();

  private:
    enum class queue_family
    {
      Compute,
      Graphics,
      Present,
      Transfer,
    };

    VkDevice Handle_ { VK_NULL_HANDLE };

    std::vector<std::string> ValidationLayerNames_;
    std::vector<std::string> ExtensionNames_;

    std::unordered_map<queue_family, vk_queue> Queues_;

    void AddQueues(const vk_physical_device& PhysicalDevice);
    void AddQueue(const queue_family Family, const s32 Index);

  };


}  // myran
