#pragma once

#include "vulkan_headers.h"
#include "status.h"
#include "image.h"

namespace myran {

  class vk_swap_chain;
  class vk_device;

  class vk_render_pass
  {
  public:
    static vk_render_pass CreateGlobalObserver(const vk_device& Device,
                                               const vk_swap_chain& SwapChain,
                                               const vk_image& DepthBuffer);
    static vk_render_pass CreateGameplay(const vk_device& Device,
                                         const vk_swap_chain& SwapChain,
                                         const vk_image& DepthBuffer,
                                         const vk_image& ColorImage,
                                         const vk_image& WorldPositionImage,
                                         const vk_image& NormalImage);

  public:
    vk_render_pass() = default;

    vk_render_pass(vk_render_pass&& Other);
    vk_render_pass& operator=(vk_render_pass&& Rhs);

    ~vk_render_pass();

    VkRenderPass GetHandle() const;

  private:
    VkRenderPass Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
