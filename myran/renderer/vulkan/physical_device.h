#pragma once

#include <string>
#include <vector>
#include <unordered_set>

#include "vulkan_headers.h"
#include "surface.h"

#include "../../types.h"

namespace myran {

  class vk_physical_device
  {
  public:
    vk_physical_device() = default;
    vk_physical_device(const VkPhysicalDevice& Handle);

    ~vk_physical_device() = default;

    VkPhysicalDevice GetHandle() const;
    const std::string& GetName() const;
    const VkPhysicalDeviceFeatures& GetFeatures() const;
    const VkPhysicalDeviceProperties& GetProperties() const;
    const VkPhysicalDeviceMemoryProperties& GetMemoryProperties() const;
    const std::vector<VkExtensionProperties>& GetExtensionProperties() const;

    bool HasComputeQueue() const;
    bool HasGraphicsQueue() const;
    bool HasPresentQueue() const;
    bool HasTransferQueue() const;

    s32 GetComputeQueueFamilyIndex() const;
    s32 GetGraphicsQueueFamilyIndex() const;
    s32 GetPresentQueueFamilyIndex() const;
    s32 GetTransferQueueFamilyIndex() const;
    const std::unordered_set<s32>& GetQueueFamilyIndices() const;

    u32 GetMemoryTypeIndex(const VkMemoryPropertyFlags properties) const;

    bool SupportsSurface(vk_surface& Surface) const;
    void UpdatePresentQueue(vk_surface& Surface);

  private:
    VkPhysicalDevice Handle_ { VK_NULL_HANDLE };
    std::string Name_;
    VkPhysicalDeviceFeatures Features_;
    VkPhysicalDeviceProperties Properties_;
    VkPhysicalDeviceMemoryProperties MemoryProperties_;
    std::vector<VkExtensionProperties> ExtensionProperties_;

    s32 ComputeQueueFamilyIndex_{ -1 };
    s32 GraphicsQueueFamilyIndex_{ -1 };
    s32 PresentQueueFamilyIndex_{ -1 };
    s32 TransferQueueFamilyIndex_{ -1 };

    std::vector<VkQueueFamilyProperties> QueueFamilyProperties_;
    std::unordered_set<s32> QueueFamilyIndices_;

    void FindAndAssignQueueFamilyIndex();
    void UpdateQueueFamilyIndices();

  };


}  // myran
