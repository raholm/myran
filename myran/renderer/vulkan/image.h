#pragma once

#include "device.h"
#include "memory.h"

namespace myran {

  class vk_image final
  {
  public:
    static vk_image CreateDepthBuffer(const vk_device& Device, const VkExtent2D Size);
    static vk_image CreateColor(const vk_device& Device, const VkExtent2D Size);
    static vk_image CreateWorldPosition(const vk_device& Device, const VkExtent2D Size);
    static vk_image CreateNormal(const vk_device& Device, const VkExtent2D Size);
    static vk_image Create(const vk_device& Device,
                           const VkExtent2D Size,
                           const VkFormat Format,
                           const VkImageUsageFlags Usage);

  public:
    vk_image() = default;
    vk_image(const VkImage& Image);

    vk_image(vk_image&& Other);
    vk_image& operator=(vk_image&& Rhs);

    ~vk_image();

    VkImage GetHandle() const;
    VkFormat GetFormat() const;

    VkDeviceSize GetRequiredSize() const;
    VkDeviceSize GetNextAlignedOffset(const VkDeviceSize Offset) const;

    void Bind(const vk_memory& Memory, const VkDeviceSize Offset);

  private:
    VkImage Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };
    VkFormat Format_;
    VkMemoryRequirements MemoryRequirements_;

  };

}  // myran
