#pragma once

#include "device.h"
#include "status.h"

namespace myran {

  class vk_sampler final
  {
  public:
    static vk_sampler CreateGameplay(const vk_device& Device);

  public:
    vk_sampler() = default;

    vk_sampler(vk_sampler&& Other);
    vk_sampler& operator=(vk_sampler&& Rhs);

    ~vk_sampler();

    VkSampler GetHandle() const;

  private:
    VkSampler Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
