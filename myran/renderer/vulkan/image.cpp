#include <myran/debug.h>

#include "image.h"
#include "getters.h"

namespace myran {

  vk_image vk_image::CreateDepthBuffer(const vk_device& Device, const VkExtent2D Size)
  {
    return Create(Device,
                  Size,
                  VK_FORMAT_D32_SFLOAT,
                  VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
  }

  vk_image vk_image::CreateColor(const vk_device& Device, const VkExtent2D Size)
  {
    return Create(Device,
                  Size,
                  VK_FORMAT_R8G8B8A8_UNORM,
                  VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);
  }

  vk_image vk_image::CreateWorldPosition(const vk_device& Device, const VkExtent2D Size)
  {
    return Create(Device,
                  Size,
                  VK_FORMAT_R32G32B32A32_SFLOAT,
                  VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);
  }

  vk_image vk_image::CreateNormal(const vk_device& Device, const VkExtent2D Size)
  {
    return Create(Device,
                  Size,
                  VK_FORMAT_R32G32B32A32_SFLOAT,
                  VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT);
  }

  vk_image vk_image::Create(const vk_device& Device,
                            const VkExtent2D Size,
                            const VkFormat Format,
                            const VkImageUsageFlags Usage)
  {
    VkImageCreateInfo CreateInfo = GetImageCreateInfo();
    CreateInfo.imageType = VK_IMAGE_TYPE_2D;
    CreateInfo.format = Format;
    CreateInfo.extent = { Size.width, Size.height, 1 };
    CreateInfo.mipLevels = 1;
    CreateInfo.arrayLayers = 1;
    CreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    CreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    CreateInfo.usage = Usage;
    CreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    vk_image Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateImage(Result.Device_,
                                           &CreateInfo,
                                           nullptr,
                                           &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to create vk image: " << Status);
      return {};
    }

    Result.Format_ = CreateInfo.format;
    vkGetImageMemoryRequirements(Result.Device_, Result.Handle_, &Result.MemoryRequirements_);
    return Result;
  }

  vk_image::vk_image(const VkImage& Handle)
    : Handle_(Handle)
  {}

  vk_image::vk_image(vk_image&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
    std::swap(Format_, Other.Format_);
    std::swap(MemoryRequirements_, Other.MemoryRequirements_);
  }

  vk_image& vk_image::operator=(vk_image&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    std::swap(Format_, Rhs.Format_);
    std::swap(MemoryRequirements_, Rhs.MemoryRequirements_);
    return *this;
  }

  vk_image::~vk_image()
  {
    if (Handle_ != VK_NULL_HANDLE && Device_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_image...");
      vkDestroyImage(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkImage vk_image::GetHandle() const
  {
    return Handle_;
  }

  VkFormat vk_image::GetFormat() const
  {
    return Format_;
  }

  VkDeviceSize vk_image::GetRequiredSize() const
  {
    return MemoryRequirements_.size;
  }

  VkDeviceSize vk_image::GetNextAlignedOffset(const VkDeviceSize Offset) const
  {
    const auto Alignment = MemoryRequirements_.alignment;
    const auto Missing = Offset % Alignment;

    if (Missing == 0)
      return Offset;

    return Offset + (Alignment - Missing);
  }

  void vk_image::Bind(const vk_memory& Memory, const VkDeviceSize Offset)
  {
    vkBindImageMemory(Device_, Handle_, Memory.GetHandle(), Offset);
  }

}  // myran
