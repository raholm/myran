#include "pipeline_layout.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_pipeline_layout vk_pipeline_layout::CreateGlobalObserver(const vk_device& Device,
                                                              const vk_descriptor_set_layout& DescriptorSetLayout)
  {
    const auto Handle = DescriptorSetLayout.GetHandle();

    VkPipelineLayoutCreateInfo CreateInfo = GetPipelineLayoutCreateInfo();
    CreateInfo.setLayoutCount = 1;
    CreateInfo.pSetLayouts = &Handle;

    vk_pipeline_layout Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreatePipelineLayout(Result.Device_,
                                    &CreateInfo,
                                    VK_NULL_HANDLE,
                                    &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_pipeline_layout: " << Status);
      return {};
    }

    return Result;
  }

  vk_pipeline_layout vk_pipeline_layout::CreateGameplay(const vk_device& Device,
                                                        const vk_descriptor_set_layout& DescriptorSetLayout)
  {
    const auto Handle = DescriptorSetLayout.GetHandle();

    VkPipelineLayoutCreateInfo CreateInfo = GetPipelineLayoutCreateInfo();
    CreateInfo.setLayoutCount = 1;
    CreateInfo.pSetLayouts = &Handle;

    vk_pipeline_layout Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreatePipelineLayout(Result.Device_,
                                    &CreateInfo,
                                    VK_NULL_HANDLE,
                                    &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_pipeline_layout: " << Status);
      return {};
    }

    return Result;
  }

  vk_pipeline_layout::vk_pipeline_layout(vk_pipeline_layout&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_pipeline_layout& vk_pipeline_layout::operator=(vk_pipeline_layout&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_pipeline_layout::~vk_pipeline_layout()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_pipeline_layout...");
      vkDestroyPipelineLayout(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkPipelineLayout vk_pipeline_layout::GetHandle() const
  {
    return Handle_;
  }

}  // myran
