#include "surface.h"
#include "status.h"
#include "instance.h"
#include "physical_device.h"

#include "../../debug.h"

namespace myran {

  vk_surface::vk_surface(vk_instance& Instance,
                         const window& Window)
    : Instance_(Instance.GetHandle())
  {
    vk_status Status;
    Status = glfwCreateWindowSurface(Instance_,
                                     Window.GetHandle(),
                                     VK_NULL_HANDLE,
                                     &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to create vk_surface: " << Status);
      return;
    }
  }

  vk_surface::vk_surface(vk_surface&& Other)
  {
    std::swap(Instance_, Other.Instance_);
    std::swap(Handle_, Other.Handle_);
  }

  vk_surface& vk_surface::operator=(vk_surface&& Rhs)
  {
    std::swap(Instance_, Rhs.Instance_);
    std::swap(Handle_, Rhs.Handle_);
    return *this;
  }

  vk_surface::~vk_surface()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_surface...");
      vkDestroySurfaceKHR(Instance_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkSurfaceKHR vk_surface::GetHandle() const
  {
    return Handle_;
  }


  vk_surface_capabilities::vk_surface_capabilities(const vk_surface& Surface,
                                                   const vk_physical_device& PhysicalDevice)

    : Surface_(Surface.GetHandle()),
      PhysicalDevice_(PhysicalDevice.GetHandle()),
      Capabilities_(Capabilities()),
      Formats_(Formats()),
      PresentModes_(PresentModes())
  {}

  bool vk_surface_capabilities::SupportsImageCount(const u32 Count) const
  {
    bool Status = false;

    // There is no limit to the number of images except for memory
    if (Capabilities_.maxImageCount == 0)
      Status = Count >= Capabilities_.minImageCount;
    else
      Status = Count >= Capabilities_.minImageCount && Count <= Capabilities_.maxImageCount;

    if (!Status)
      LogWarning("Surface supports "
                 << Capabilities_.minImageCount
                 << " to "
                 << Capabilities_.maxImageCount
                 << " images");

    return Status;
  }

  bool vk_surface_capabilities::SupportsImageExtent(const VkExtent2D Extent) const
  {
    bool IsSupported = Extent.width >= Capabilities_.minImageExtent.width &&
      Extent.width <= Capabilities_.maxImageExtent.width &&
      Extent.height >= Capabilities_.minImageExtent.height &&
      Extent.height <= Capabilities_.maxImageExtent.height;

    if (!IsSupported)
      LogWarning("Surface supports extent from ("
                 << Capabilities_.minImageExtent.width
                 << ", "
                 << Capabilities_.minImageExtent.height
                 << ") to "
                 << Capabilities_.maxImageExtent.width
                 << ", "
                 << Capabilities_.maxImageExtent.height
                 << ")");

    return IsSupported;
  }

  bool vk_surface_capabilities::SupportsImageUsage(const VkImageUsageFlags Flags) const
  {
    bool IsSupported = (Capabilities_.supportedUsageFlags & Flags) == Flags;

    if (!IsSupported)
      LogWarning("Surface supports the folloing image usages:\n"
                 << ((Capabilities_.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_SRC_BIT) ? "TransferSource, " : "")
                 << ((Capabilities_.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT) ? "TransferDestination, " : "")
                 << ((Capabilities_.supportedUsageFlags & VK_IMAGE_USAGE_SAMPLED_BIT) ? "Sampled, " : "")
                 << ((Capabilities_.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) ? "ColorAttachment, " : "")
                 << ((Capabilities_.supportedUsageFlags & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) ? "DepthStencilAttachment" : ""));

    return IsSupported;
  }

  bool vk_surface_capabilities::SupportsImageArrayLayers(const u32 ArrayLayers) const
  {
    bool IsSupported = ArrayLayers <= Capabilities_.maxImageArrayLayers;

    if (!IsSupported)
      LogWarning("Surface supports up to "
                 << Capabilities_.maxImageArrayLayers
                 << " image array layers");

    return IsSupported;
  }

  bool vk_surface_capabilities::SupportsTransform(const VkSurfaceTransformFlagsKHR Transform) const
  {
    bool IsSupported = Capabilities_.supportedTransforms & Transform;

    if (!IsSupported)
      LogWarning("Surface supports the following transforms:\n"
                 << ((Capabilities_.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) ? "Identity" : ""));

    return IsSupported;
  }

  bool vk_surface_capabilities::SupportsCompositeAlpha(const VkCompositeAlphaFlagsKHR CompositeAlpha) const
  {
    bool IsSupported = Capabilities_.supportedCompositeAlpha & CompositeAlpha;

    if (!IsSupported)
      LogWarning("Surface supports the following composite alpha:\n"
                 << ((Capabilities_.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR) ? "Opaque" : ""));

    return IsSupported;
  }

  bool vk_surface_capabilities::SupportsPresentMode(const VkPresentModeKHR PresentMode) const
  {
    bool IsSupported = false;

    for (VkPresentModeKHR PM : PresentModes_)
      if (PM & PresentMode)
        IsSupported = true;

    if (!IsSupported)
    {
      std::ostringstream Oss;
      Oss << "Surface supports the following present modes:\n";

      for (VkPresentModeKHR PM : PresentModes_)
      {
        if (PM & VK_PRESENT_MODE_IMMEDIATE_KHR)
          Oss << "Immediate, ";
        else if (PM & VK_PRESENT_MODE_FIFO_KHR)
          Oss << "Fifo, ";
        else if (PM & VK_PRESENT_MODE_FIFO_RELAXED_KHR)
          Oss << "FifoRelaxed, ";
        else if (PM & VK_PRESENT_MODE_MAILBOX_KHR)
          Oss << "MailBox, ";
      }

      LogWarning(Oss.str());
    }

    return IsSupported;
  }

  VkSurfaceFormatKHR vk_surface_capabilities::GetPreferredSurfaceFormat() const
  {
    VkFormat PreferredFormat = VK_FORMAT_B8G8R8A8_UNORM;
    VkColorSpaceKHR PreferredColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
    VkSurfaceFormatKHR ResultingSurfaceFormat = Formats_[0];

    if (Formats_.size() == 1) {
      if (Formats_[0].format == VK_FORMAT_UNDEFINED)
      {
        // This is a special case for Vulkan meaning there is no preferred surface format
        // so whichever works
        ResultingSurfaceFormat.format = PreferredFormat;
        ResultingSurfaceFormat.colorSpace = PreferredColorSpace;
      } else
      {
        ResultingSurfaceFormat.format = Formats_[0].format;
        ResultingSurfaceFormat.colorSpace = Formats_[0].colorSpace;
      }
    } else
    {
      if (std::any_of(Formats_.cbegin(),
                      Formats_.cend(),
                      [&PreferredFormat, &PreferredColorSpace](const VkSurfaceFormatKHR& Format)
                        {
                          return Format.format == PreferredFormat &&
                            Format.colorSpace == PreferredColorSpace;
                        }))
      {
        ResultingSurfaceFormat.format = PreferredFormat;
        ResultingSurfaceFormat.colorSpace = PreferredColorSpace;
      }
    }

    return ResultingSurfaceFormat;
  }

  VkFormat vk_surface_capabilities::GetPreferredImageFormat() const
  {
    VkSurfaceFormatKHR SurfaceFormat = GetPreferredSurfaceFormat();
    return SurfaceFormat.format;
  }

  VkImageUsageFlags vk_surface_capabilities::GetPreferredImageUsage() const
  {
    VkImageUsageFlags Preferred =
      VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
      VK_IMAGE_USAGE_TRANSFER_DST_BIT;

    if (SupportsImageUsage(Preferred))
      return Preferred;

    return VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  }

  VkSurfaceTransformFlagBitsKHR vk_surface_capabilities::GetPreferredPreTransform() const
  {
    return Capabilities_.currentTransform;
  }

  VkColorSpaceKHR vk_surface_capabilities::GetPreferredImageColorSpace() const
  {
    VkSurfaceFormatKHR SurfaceFormat = GetPreferredSurfaceFormat();
    return SurfaceFormat.colorSpace;
  }

  VkPresentModeKHR vk_surface_capabilities::GetPreferredPresentMode() const
  {
    VkPresentModeKHR PreferredMode = VK_PRESENT_MODE_MAILBOX_KHR;
    VkPresentModeKHR SecondaryPreferredMode = VK_PRESENT_MODE_FIFO_KHR;
    VkPresentModeKHR ResultingPresentMode = VK_PRESENT_MODE_FIFO_KHR;

    for (VkPresentModeKHR PresentMode : PresentModes_)
    {
      if (PresentMode == PreferredMode)
      {
        ResultingPresentMode = PreferredMode;
        break;
      } else if (PresentMode == SecondaryPreferredMode)
      {
        ResultingPresentMode = SecondaryPreferredMode;
      }
    }

    return ResultingPresentMode;
  }

  VkExtent2D vk_surface_capabilities::GetPreferredImageExtent(const u32 PreferredWidth,
                                                              const u32 PreferredHeight) const
  {
    // NOTE(ra-hol) : std::numeric_limits<u32>::max() is a special value for width/height
    // for certain window managers such that the resolution best matches the window within its bounds
    VkExtent2D ResultingImageExtent;

    if (Capabilities_.currentExtent.width != std::numeric_limits<u32>::max())
    {
      ResultingImageExtent = Capabilities_.currentExtent;
    } else
    {
      ResultingImageExtent.width =
        std::max(Capabilities_.minImageExtent.width,
                 std::min(Capabilities_.maxImageExtent.width,
                          PreferredWidth));
      ResultingImageExtent.height =
        std::max(Capabilities_.minImageExtent.height,
                 std::min(Capabilities_.maxImageExtent.height,
                          PreferredHeight));
    }

    return ResultingImageExtent;
  }

  const VkSurfaceCapabilitiesKHR& vk_surface_capabilities::GetCapabilities() const
  {
    return Capabilities_;
  }

  const std::vector<VkSurfaceFormatKHR>& vk_surface_capabilities::GetFormats() const
  {
    return Formats_;
  }

  const std::vector<VkPresentModeKHR>& vk_surface_capabilities::GetPresentModes() const
  {
    return PresentModes_;
  }

  VkSurfaceCapabilitiesKHR vk_surface_capabilities::Capabilities() const
  {
    LogInternal("Getting surface capabilities...");

    VkSurfaceCapabilitiesKHR Capabilities;
    vk_status Status;
    Status = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(PhysicalDevice_,
                                                       Surface_,
                                                       &Capabilities);

    if (Status.IsError())
      LogError("Failed to get physical device surface capabilities: " << Status);

    return Capabilities;
  }

  std::vector<VkSurfaceFormatKHR> vk_surface_capabilities::Formats() const
  {
    LogInternal("Getting surface format(s)...");

    std::vector<VkSurfaceFormatKHR> Formats;

    vk_status Status;
    u32 FormatCount = 0;
    Status = vkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDevice_,
                                                  Surface_,
                                                  &FormatCount,
                                                  nullptr);

    if (Status.IsError())
      LogError("Failed to get physical device surface formats: " << Status);

    if (Status.IsOk() && FormatCount > 0) {
      Formats.resize(FormatCount);
      Status = vkGetPhysicalDeviceSurfaceFormatsKHR(PhysicalDevice_,
                                                    Surface_,
                                                    &FormatCount,
                                                    Formats.data());

      if (Status.IsError())
        LogError("Failed to get physical device surface formats: " << Status);
    }

    return Formats;
  }

  std::vector<VkPresentModeKHR> vk_surface_capabilities::PresentModes() const
  {
    LogInternal("Getting present mode(s)...");

    std::vector<VkPresentModeKHR> PresentModes;

    vk_status Status;
    u32 PresentModeCount = 0;
    Status = vkGetPhysicalDeviceSurfacePresentModesKHR(PhysicalDevice_,
                                                       Surface_,
                                                       &PresentModeCount,
                                                       nullptr);
    if (Status.IsError())
      LogError("Failed to get physical device surface present modes: " << Status);

    if (Status.IsOk() && PresentModeCount > 0) {
      PresentModes.resize(PresentModeCount);
      Status = vkGetPhysicalDeviceSurfacePresentModesKHR(PhysicalDevice_,
                                                         Surface_,
                                                         &PresentModeCount,
                                                         PresentModes.data());

      if (Status.IsError())
        LogError("Failed to get physical device surface present modes: " << Status);

    }

    return PresentModes;
  }

}  // myran
