#pragma once

#include <limits>

#include "device.h"
#include "status.h"

namespace myran {

  class vk_fence
  {
  public:
    vk_fence(vk_device& Device);

    vk_fence(vk_fence&& Other);
    vk_fence& operator=(vk_fence&& Rhs);

    ~vk_fence();

    vk_status Wait(u64 Timeout = std::numeric_limits<u64>::max());
    vk_status Reset();
    vk_status WaitAndReset(u64 Timeout = std::numeric_limits<u64>::max());

    bool IsSignlad() const;

    VkFence GetHandle() const;

  private:
    VkFence Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
