#include "shader_module.h"
#include "getters.h"

#include <fstream>

#include "../../debug.h"

namespace myran {

  vk_shader_module vk_shader_module::CreateFromName(const vk_device& Device,
                                                    const stage Stage,
                                                    const std::string& Name)
  {
    LogInfo("Creating shader module '" << Name << "'...");

    const char* ProjectPath = getenv("MYRAN_HOME");
    Assert(ProjectPath);

    std::filesystem::path FilePath;
    FilePath /= ProjectPath;
    FilePath /= "build";
    FilePath /= "bin";
    FilePath /= "shaders";

    if (Stage == stage::Vertex)
    {
      FilePath /= "vertex";
    }
    else if (Stage == stage::Fragment)
    {
      FilePath /= "fragment";
    }

    FilePath /= Name + ".spv";

    vk_shader_module Result;
    Result.Device_ = Device.GetHandle();
    Result.Stage_ = Stage;
    Result.FilePath_ = FilePath;
    Result.Spirv_ = GetSpirv(FilePath);

    VkShaderModuleCreateInfo CreateInfo = GetShaderModuleCreateInfo();
    CreateInfo.codeSize = Result.Spirv_.size();
    CreateInfo.pCode = reinterpret_cast<const u32*>(Result.Spirv_.data());

    const vk_status Status = vkCreateShaderModule(Result.Device_,
                                                  &CreateInfo,
                                                  VK_NULL_HANDLE,
                                                  &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_shader_module: " << Status);
      return {};
    }

    return Result;
  }

  vk_shader_module::vk_shader_module(vk_shader_module&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
    std::swap(Stage_, Other.Stage_);
    std::swap(FilePath_, Other.FilePath_);
    std::swap(Spirv_, Other.Spirv_);
  }

  vk_shader_module& vk_shader_module::operator=(vk_shader_module&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    std::swap(Stage_, Rhs.Stage_);
    std::swap(FilePath_, Rhs.FilePath_);
    std::swap(Spirv_, Rhs.Spirv_);
    return *this;
  }

  vk_shader_module::~vk_shader_module()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_shader_module...");
      vkDestroyShaderModule(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkShaderModule vk_shader_module::GetHandle() const
  {
    return Handle_;
  }

  std::vector<byte> vk_shader_module::GetSpirv(const std::filesystem::path& FilePath)
  {
    std::ifstream File(FilePath, std::ios::in | std::ios::ate | std::ios::binary);

    if (!File)
    {
      LogError("Failed to read shader file: " << FilePath);
      return {};
    }

    const auto Size = File.tellg();
    std::vector<byte> Spirv(Size);

    File.seekg(0);
    File.read(Spirv.data(), Size);
    File.close();
    return Spirv;
  }

}  // myran
