#include "command_pool.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_command_pool::vk_command_pool(const u32 QueueFamilyIndex,
                                   const vk_device& Device)
    : Device_(Device.GetHandle())
  {
    VkCommandPoolCreateInfo CreateInfo = GetCommandPoolCreateInfo();
    CreateInfo.queueFamilyIndex = QueueFamilyIndex;
    CreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    vk_status Status;
    Status = vkCreateCommandPool(Device_,
                                 &CreateInfo,
                                 VK_NULL_HANDLE,
                                 &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_command_pool: " << Status);
      return;
    }
  }

  vk_command_pool::vk_command_pool(vk_command_pool&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_command_pool& vk_command_pool::operator=(vk_command_pool&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_command_pool::~vk_command_pool()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_command_pool...");
      vkDestroyCommandPool(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  std::vector<vk_command_buffer> vk_command_pool::AllocateCommandBuffers(const u32 Count)
  {
    Assert(Count > 1);

    VkCommandBufferAllocateInfo AllocateInfo = GetCommandBufferAllocateInfo();
    AllocateInfo.commandPool = Handle_;
    AllocateInfo.commandBufferCount = Count;
    AllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    std::vector<VkCommandBuffer> Handles(Count);
    vk_status Status;
    Status = vkAllocateCommandBuffers(Device_, &AllocateInfo, Handles.data());

    if (Status.IsError())
    {
      LogError("Failed to allocate vk_command_buffer: " << Status);
      Handles.clear();
    }

    std::vector<vk_command_buffer> Result;
    Result.reserve(Handles.size());

    for (VkCommandBuffer Handle : Handles)
      Result.emplace_back(vk_command_buffer(Handle));

    return Result;
  }

  vk_command_buffer vk_command_pool::AllocateCommandBuffer()
  {
    VkCommandBufferAllocateInfo AllocateInfo = GetCommandBufferAllocateInfo();
    AllocateInfo.commandPool = Handle_;
    AllocateInfo.commandBufferCount = 1;
    AllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

    VkCommandBuffer Handle;
    vk_status Status;
    Status = vkAllocateCommandBuffers(Device_, &AllocateInfo, &Handle);

    if (Status.IsError())
    {
      LogError("Failed to allocate vk_command_buffer: " << Status);
      Handle = VK_NULL_HANDLE;
    }

    return vk_command_buffer(Handle);
  }

  VkCommandPool vk_command_pool::GetHandle() const
  {
    return Handle_;
  }

}  // myran
