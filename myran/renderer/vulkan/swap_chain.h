#pragma once

#include <vector>

#include "vulkan_headers.h"
#include "status.h"

namespace myran {

  class vk_device;
  class vk_surface;
  class vk_physical_device;
  class vk_fence;
  class vk_semaphore;
  class vk_image_view;
  class vk_image;

  class vk_swap_chain
  {
  public:
    struct image
    {
      u32 Index;
      vk_status Status;
    };

  public:
    vk_swap_chain() = default;
    vk_swap_chain(const vk_device& Device,
                  const vk_surface& Surface,
                  const vk_physical_device& PhysicalDevice,
                  const VkExtent2D PreferredWindowExtent,
                  const u32 PreferredImageCount);

    vk_swap_chain(vk_swap_chain&& Other);
    vk_swap_chain& operator=(vk_swap_chain&& Rhs);

    ~vk_swap_chain();

    image AcquireNextImage(const vk_semaphore& Semaphore);
    image AcquireNextImage(const vk_fence& Fence);
    image AcquireNextImage(const vk_semaphore& Semaphore,
                           const vk_fence& Fence);

    VkSwapchainKHR GetHandle() const;
    u32 GetImageCount() const;
    u32 GetCurrentImageIndex() const;
    VkFormat GetImageFormat() const;
    VkExtent2D GetImageExtent() const;
    const vk_image& GetImage(const u32 Index) const;
    const vk_image_view& GetImageView(const u32 Index) const;
    const vk_image& GetCurrentImage() const;
    const vk_image_view& GetCurrentImageView() const;

  private:
    VkDevice Device_ { VK_NULL_HANDLE };
    VkSwapchainKHR Handle_ { VK_NULL_HANDLE };

    VkSurfaceFormatKHR SurfaceFormat_;
    VkExtent2D ImageExtent_;

    u32 CurrentImageIndex_;
    std::vector<VkImage> ImageHandles_;
    std::vector<vk_image> Images_;
    std::vector<vk_image_view> ImageViews_;

    void CreateImages();
  };

}  // myran
