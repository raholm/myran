#include "swap_chain.h"
#include "device.h"
#include "surface.h"
#include "physical_device.h"
#include "fence.h"
#include "semaphore.h"
#include "image.h"
#include "image_view.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_swap_chain::vk_swap_chain(const vk_device& Device,
                               const vk_surface& Surface,
                               const vk_physical_device& PhysicalDevice,
                               const VkExtent2D PreferredWindowExtent,
                               const u32 PreferredImageCount)
    : Device_(Device.GetHandle())
  {
    vk_surface_capabilities SurfaceCapabilities(Surface, PhysicalDevice);

    u32 ImageCount = PreferredImageCount;

    if (!SurfaceCapabilities.SupportsImageCount(ImageCount))
    {
      ImageCount = SurfaceCapabilities.GetCapabilities().maxImageCount;

      if (ImageCount == 0)
        ImageCount = SurfaceCapabilities.GetCapabilities().minImageCount;
    }

    SurfaceFormat_ = SurfaceCapabilities.GetPreferredSurfaceFormat();
    ImageExtent_ = SurfaceCapabilities.GetPreferredImageExtent(PreferredWindowExtent.width,
                                                               PreferredWindowExtent.height);

    VkSwapchainCreateInfoKHR CreateInfo = GetSwapchainCreateInfoKHR();
    CreateInfo.surface = Surface.GetHandle();
    CreateInfo.minImageCount = ImageCount;
    CreateInfo.imageFormat = SurfaceFormat_.format;
    CreateInfo.imageColorSpace = SurfaceFormat_.colorSpace;
    CreateInfo.imageExtent = ImageExtent_;
    CreateInfo.preTransform = SurfaceCapabilities.GetPreferredPreTransform();
    CreateInfo.presentMode = SurfaceCapabilities.GetPreferredPresentMode();
    CreateInfo.imageUsage = SurfaceCapabilities.GetPreferredImageUsage();

    CreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    CreateInfo.imageArrayLayers = 1;
    CreateInfo.clipped = VK_TRUE;

    if (Device.GetGraphicsQueue() == Device.GetPresentQueue())
    {
      CreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
      CreateInfo.queueFamilyIndexCount = 0;
      CreateInfo.pQueueFamilyIndices = nullptr;
    }
    else
    {
      std::array<u32, 2> QueueFamilyIndices = {
        static_cast<u32>(Device.GetGraphicsQueue().GetFamilyIndex()),
        static_cast<u32>(Device.GetPresentQueue().GetFamilyIndex()),
      };

      CreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
      CreateInfo.queueFamilyIndexCount = QueueFamilyIndices.size();
      CreateInfo.pQueueFamilyIndices = QueueFamilyIndices.data();
    }

    vk_status Status;
    Status = vkCreateSwapchainKHR(Device_,
                                  &CreateInfo,
                                  VK_NULL_HANDLE,
                                  &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_swap_chain: " << Status);
      return;
    }

    CreateImages();
  }

  vk_swap_chain::vk_swap_chain(vk_swap_chain&& Other)
  {
    std::swap(Device_, Other.Device_);
    std::swap(Handle_, Other.Handle_);
    std::swap(SurfaceFormat_, Other.SurfaceFormat_);
    std::swap(ImageExtent_, Other.ImageExtent_);
    std::swap(CurrentImageIndex_, Other.CurrentImageIndex_);
    std::swap(ImageHandles_, Other.ImageHandles_);
    std::swap(Images_, Other.Images_);
    std::swap(ImageViews_, Other.ImageViews_);
  }

  vk_swap_chain& vk_swap_chain::operator=(vk_swap_chain&& Rhs)
  {
    std::swap(Device_, Rhs.Device_);
    std::swap(Handle_, Rhs.Handle_);
    std::swap(SurfaceFormat_, Rhs.SurfaceFormat_);
    std::swap(ImageExtent_, Rhs.ImageExtent_);
    std::swap(CurrentImageIndex_, Rhs.CurrentImageIndex_);
    std::swap(ImageHandles_, Rhs.ImageHandles_);
    std::swap(Images_, Rhs.Images_);
    std::swap(ImageViews_, Rhs.ImageViews_);
    return *this;
  }

  vk_swap_chain::~vk_swap_chain()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      ImageHandles_.clear();
      ImageViews_.clear();

      LogInternal("Destroying vk_swap_chain...");
      vkDestroySwapchainKHR(Device_,
                            Handle_,
                            VK_NULL_HANDLE);
    }
  }

  vk_swap_chain::image vk_swap_chain::AcquireNextImage(const vk_semaphore& Semaphore)
  {
    image Result;
    Result.Status = vkAcquireNextImageKHR(Device_,
                                          Handle_,
                                          std::numeric_limits<u64>::max(),
                                          Semaphore.GetHandle(),
                                          VK_NULL_HANDLE,
                                          &Result.Index);

    if (Result.Status.IsError())
      LogError("Failed to acquire next image: " << Result.Status);

    CurrentImageIndex_ = Result.Index;
    return Result;
  }

  vk_swap_chain::image vk_swap_chain::AcquireNextImage(const vk_fence& Fence)
  {
    image Result;
    Result.Status = vkAcquireNextImageKHR(Device_,
                                          Handle_,
                                          std::numeric_limits<u64>::max(),
                                          VK_NULL_HANDLE,
                                          Fence.GetHandle(),
                                          &Result.Index);

    if (Result.Status.IsError())
      LogError("Failed to acquire next image: " << Result.Status);

    CurrentImageIndex_ = Result.Index;
    return Result;
  }

  vk_swap_chain::image vk_swap_chain::AcquireNextImage(const vk_semaphore& Semaphore,
                                                       const vk_fence& Fence)
  {
    image Result;
    Result.Status = vkAcquireNextImageKHR(Device_,
                                          Handle_,
                                          std::numeric_limits<u64>::max(),
                                          Semaphore.GetHandle(),
                                          Fence.GetHandle(),
                                          &Result.Index);

    if (Result.Status.IsError())
      LogError("Failed to acquire next image: " << Result.Status);

    CurrentImageIndex_ = Result.Index;
    return Result;
  }

  VkSwapchainKHR vk_swap_chain::GetHandle() const
  {
    return Handle_;
  }

  u32 vk_swap_chain::GetImageCount() const
  {
    return Images_.size();
  }

  u32 vk_swap_chain::GetCurrentImageIndex() const
  {
    return CurrentImageIndex_;
  }

  VkFormat vk_swap_chain::GetImageFormat() const
  {
    return SurfaceFormat_.format;
  }

  VkExtent2D vk_swap_chain::GetImageExtent() const
  {
    return ImageExtent_;
  }

  const vk_image& vk_swap_chain::GetImage(const u32 Index) const
  {
    Assert(Index < GetImageCount());
    return Images_[Index];
  }
  const vk_image_view& vk_swap_chain::GetImageView(const u32 Index) const
  {
    Assert(Index < GetImageCount());
    return ImageViews_[Index];
  }

  const vk_image& vk_swap_chain::GetCurrentImage() const
  {
    return GetImage(GetCurrentImageIndex());
  }

  const vk_image_view& vk_swap_chain::GetCurrentImageView() const
  {
    return GetImageView(GetCurrentImageIndex());
  }

  void vk_swap_chain::CreateImages()
  {
    u32 ImageCount = 0;
    vk_status Status;
    Status = vkGetSwapchainImagesKHR(Device_,
                                     Handle_,
                                     &ImageCount,
                                     VK_NULL_HANDLE);

    if (Status.IsError())
      LogError("Failed to get swapchain images: " << Status);

    if (Status.IsOk() && ImageCount > 0)
    {
      ImageHandles_.resize(ImageCount);
      Status = vkGetSwapchainImagesKHR(Device_,
                                       Handle_,
                                       &ImageCount,
                                       ImageHandles_.data());

      if (Status.IsError())
      {
        LogError("Failed to get swapchain images: " << Status);
        ImageHandles_.clear();
      }
    }

    ImageViews_.reserve(ImageCount);
    Images_.reserve(ImageCount);

    for (const VkImage& ImageHandle : ImageHandles_)
    {
      Images_.emplace_back(vk_image(ImageHandle));
      ImageViews_.emplace_back(vk_image_view(Device_,
                                             ImageHandle,
                                             VK_IMAGE_VIEW_TYPE_2D,
                                             SurfaceFormat_.format,
                                             VK_IMAGE_ASPECT_COLOR_BIT));

    }
  }

}  // myran
