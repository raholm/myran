#pragma once

#include <string>

#include "physical_device.h"

namespace myran {

  struct vk_version
  {
    vk_version() = default;
    vk_version(const u32 _Major,
               const u32 _Minor,
               const u32 _Patch)
      : Major(_Major),
        Minor(_Minor),
        Patch(_Patch)
      {}

    u32 GetPacked() { return VK_MAKE_VERSION(Major, Minor, Patch); }

    u32 Major;
    u32 Minor;
    u32 Patch;
  };

  struct vk_application_info
  {
    vk_version Version;
    vk_version ApiVersion;
    vk_version EngineVersion;
    std::string Name;
    std::string EngineName;
  };

  class vk_instance final
  {
  public:
    vk_instance() = default;
    vk_instance(const vk_application_info& ApplicationInfo,
                const std::vector<std::string>& ExtensionNames,
                const std::vector<std::string>& ValidationLayerNames);

    vk_instance(vk_instance&& Other);
    vk_instance& operator=(vk_instance&& Other);

    ~vk_instance();

    VkInstance GetHandle() const;
    const vk_application_info& GetApplicationInfo() const;
    const std::vector<std::string>& GetExtensionNames() const;
    const std::vector<std::string>& GetValidationLayerNames() const;
    const std::vector<vk_physical_device>& GetPhysicalDevices() const;

  private:
    VkInstance Handle_ { VK_NULL_HANDLE };
    vk_application_info ApplicationInfo_;
    std::vector<std::string> ExtensionNames_;
    std::vector<std::string> ValidationLayerNames_;
    std::vector<vk_physical_device> PhysicalDevices_;

  };

}  // myran
