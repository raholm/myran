#pragma once

#include "vulkan_headers.h"
#include "status.h"

namespace myran {

  class vk_command_buffer;
  class vk_swap_chain;
  class vk_semaphore;
  class vk_fence;

  class vk_queue final
  {
  public:
    vk_queue(const VkQueue& Handle, const s32 FamilyIndex);

    ~vk_queue() = default;

    bool operator==(const vk_queue& Rhs) const;
    bool operator!=(const vk_queue& Rhs) const;

    vk_status Submit(const vk_command_buffer& CommandBuffer);

    vk_status Submit(const vk_command_buffer& CommandBuffer,
                     const VkPipelineStageFlags WaitStageFlags,
                     const vk_semaphore& WaitSemaphore,
                     const vk_semaphore& SignalSemaphore,
                     const vk_fence& Fence);

    vk_status Present(const vk_swap_chain& SwapChain,
                      const vk_semaphore& WaitSemaphore);

    vk_status Wait();

    s32 GetFamilyIndex() const;
    VkQueue GetHandle() const;

  private:
    VkQueue Handle_ { VK_NULL_HANDLE };
    s32 FamilyIndex_ { -1 };

  };

}  // myran
