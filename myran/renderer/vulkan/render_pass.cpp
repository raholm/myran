#include "render_pass.h"
#include "device.h"
#include "swap_chain.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_render_pass vk_render_pass::CreateGlobalObserver(const vk_device& Device,
                                                      const vk_swap_chain& SwapChain,
                                                      const vk_image& DepthBuffer)
  {
    std::vector<VkAttachmentDescription> AttachmentDescriptions;

    // Color Image
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = SwapChain.GetImageFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    // Depth Buffer
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = DepthBuffer.GetFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    VkAttachmentReference ColorAttachmentReference = GetAttachmentReference();
    ColorAttachmentReference.attachment = 0;
    ColorAttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference DepthAttachmentReference = GetAttachmentReference();
    DepthAttachmentReference.attachment = 1;
    DepthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription SubpassDescription = GetSubpassDescription();
    SubpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    SubpassDescription.colorAttachmentCount = 1;
    SubpassDescription.pColorAttachments = &ColorAttachmentReference;
    SubpassDescription.pDepthStencilAttachment = &DepthAttachmentReference;

    VkRenderPassCreateInfo CreateInfo = GetRenderPassCreateInfo();
    CreateInfo.attachmentCount = AttachmentDescriptions.size();
    CreateInfo.pAttachments = AttachmentDescriptions.data();
    CreateInfo.subpassCount = 1;
    CreateInfo.pSubpasses = &SubpassDescription;

    vk_render_pass Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateRenderPass(Result.Device_,
                                                &CreateInfo,
                                                VK_NULL_HANDLE,
                                                &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_render_pass: " << Status);
      return {};
    }

    return Result;
  }

  vk_render_pass vk_render_pass::CreateGameplay(const vk_device& Device,
                                                const vk_swap_chain& SwapChain,
                                                const vk_image& DepthBuffer,
                                                const vk_image& ColorImage,
                                                const vk_image& WorldPositionImage,
                                                const vk_image& NormalImage)
  {
    std::vector<VkAttachmentDescription> AttachmentDescriptions;

    // Ouput Color Image
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = SwapChain.GetImageFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    // Depth Buffer
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = DepthBuffer.GetFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    // Color Image
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = ColorImage.GetFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    // World Position Image
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = WorldPositionImage.GetFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    // Normal Image
    {
      VkAttachmentDescription AttachmentDescription = GetAttachmentDescription();
      AttachmentDescription.format = NormalImage.GetFormat();
      AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
      AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
      AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
      AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
      AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

      AttachmentDescriptions.push_back(AttachmentDescription);
    }

    VkAttachmentReference DepthAttachmentReference = GetAttachmentReference();
    DepthAttachmentReference.attachment = 1;
    DepthAttachmentReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    const std::vector<VkAttachmentReference> OffscreenColorAttachments = {
      // Color
      {
        .attachment = 2,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      },
      // World Position
      {
        .attachment = 3,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      },
      // Normal
      {
        .attachment = 4,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      },
    };

    const std::vector<VkAttachmentReference> OnscreenColorAttachments = {
      // Output Color
      {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      },
    };

    const std::vector<VkAttachmentReference> OnscreenInputAttachments = {
      // Color
      {
        .attachment = 2,
        .layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      },
      // World Position
      {
        .attachment = 3,
        .layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      },
      // Normal
      {
        .attachment = 4,
        .layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
      },
    };

    const std::vector<VkAttachmentReference> MinimapColorAttachments = {
      // Output Color
      {
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      },
    };

    std::vector<VkSubpassDescription> SubpassDescriptions;

    // Offscreen
    {
      VkSubpassDescription Description = GetSubpassDescription();
      Description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
      Description.colorAttachmentCount = OffscreenColorAttachments.size();
      Description.pColorAttachments = OffscreenColorAttachments.data();
      Description.pDepthStencilAttachment = &DepthAttachmentReference;

      SubpassDescriptions.push_back(Description);
    }

    // Onscreen
    {
      VkSubpassDescription Description = GetSubpassDescription();
      Description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
      Description.colorAttachmentCount = OnscreenColorAttachments.size();
      Description.pColorAttachments = OnscreenColorAttachments.data();
      Description.pDepthStencilAttachment = &DepthAttachmentReference;
      Description.inputAttachmentCount = OnscreenInputAttachments.size();
      Description.pInputAttachments = OnscreenInputAttachments.data();

      SubpassDescriptions.push_back(Description);
    }

    // Minimap
    {
      VkSubpassDescription Description = GetSubpassDescription();
      Description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
      Description.colorAttachmentCount = MinimapColorAttachments.size();
      Description.pColorAttachments = MinimapColorAttachments.data();
      Description.pDepthStencilAttachment = &DepthAttachmentReference;

      SubpassDescriptions.push_back(Description);
    }

    std::vector<VkSubpassDependency> SubpassDependencies;

    // External -> Offscreen
    {
      VkSubpassDependency Dependency = GetSubpassDependency();
      Dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
      Dependency.dstSubpass = 0;
      Dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
      Dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
      Dependency.srcAccessMask = 0;
      Dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      Dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

      SubpassDependencies.push_back(Dependency);
    }

    // Offscreen -> Onscreen
    {
      VkSubpassDependency Dependency = GetSubpassDependency();
      Dependency.srcSubpass = 0;
      Dependency.dstSubpass = 1;
      Dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
      Dependency.dstStageMask =
        VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
        VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
      Dependency.srcAccessMask = 0;
      Dependency.dstAccessMask = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
      Dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

      SubpassDependencies.push_back(Dependency);
    }

    // Onscreen -> Minimap
    {
      VkSubpassDependency Dependency = GetSubpassDependency();
      Dependency.srcSubpass = 1;
      Dependency.dstSubpass = 2;
      Dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
      Dependency.dstStageMask =
        VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT |
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT |
        VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
      Dependency.srcAccessMask = 0;
      Dependency.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      Dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

      SubpassDependencies.push_back(Dependency);
    }

    // Minimap -> External
    {
      VkSubpassDependency Dependency = GetSubpassDependency();
      Dependency.srcSubpass = 2;
      Dependency.dstSubpass = VK_SUBPASS_EXTERNAL;
      Dependency.srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
      Dependency.dstStageMask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
      Dependency.srcAccessMask = 0;
      Dependency.dstAccessMask = 0;
      Dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

      SubpassDependencies.push_back(Dependency);
    }

    VkRenderPassCreateInfo CreateInfo = GetRenderPassCreateInfo();
    CreateInfo.attachmentCount = AttachmentDescriptions.size();
    CreateInfo.pAttachments = AttachmentDescriptions.data();
    CreateInfo.subpassCount = SubpassDescriptions.size();
    CreateInfo.pSubpasses = SubpassDescriptions.data();
    CreateInfo.dependencyCount = SubpassDependencies.size();
    CreateInfo.pDependencies = SubpassDependencies.data();

    vk_render_pass Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateRenderPass(Result.Device_,
                                                &CreateInfo,
                                                VK_NULL_HANDLE,
                                                &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_render_pass: " << Status);
      return {};
    }

    return Result;
  }

  vk_render_pass::vk_render_pass(vk_render_pass&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_render_pass& vk_render_pass::operator=(vk_render_pass&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_render_pass::~vk_render_pass()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_render_pass...");
      vkDestroyRenderPass(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkRenderPass vk_render_pass::GetHandle() const
  {
    return Handle_;
  }

}  // myran
