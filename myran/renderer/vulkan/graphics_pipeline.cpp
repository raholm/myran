#include "graphics_pipeline.h"
#include "device.h"
#include "render_pass.h"
#include "swap_chain.h"
#include "shader_module.h"
#include "pipeline_layout.h"
#include "getters.h"

#include <array>

#include "../../debug.h"

namespace myran {

  vk_graphics_pipeline vk_graphics_pipeline::CreateGlobalObserver(const vk_device& Device,
                                                                  const vk_pipeline_layout& Layout,
                                                                  const vk_render_pass& RenderPass,
                                                                  const VkExtent2D ImageSize)
  {
    vk_shader_module VertexShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Vertex, "global_observer");
    vk_shader_module FragmentShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Fragment, "global_observer");

    VkPipelineShaderStageCreateInfo VertexShaderStage = GetPipelineShaderStageCreateInfo();
    VertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
    VertexShaderStage.module = VertexShaderModule.GetHandle();
    VertexShaderStage.pName = "main";

    VkPipelineShaderStageCreateInfo FragmentShaderStage = GetPipelineShaderStageCreateInfo();
    FragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    FragmentShaderStage.module = FragmentShaderModule.GetHandle();
    FragmentShaderStage.pName = "main";

    const std::vector<VkVertexInputBindingDescription> Bindings = {
      {
        .binding = 0,
        .stride = 24,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
      },
      {
        .binding = 1,
        .stride = 68,
        .inputRate = VK_VERTEX_INPUT_RATE_INSTANCE,
      },
    };

    const std::vector<VkVertexInputAttributeDescription> Attributes = {
      // Input Position
      {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0,
      },
      // Input Normal
      {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 12,
      },
      // Input Transform[0]
      {
        .location = 2,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 0,
      },
      // Input Transform[1]
      {
        .location = 3,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 16,
      },
      // Input Transform[2]
      {
        .location = 4,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 32,
      },
      // Input Transform[3]
      {
        .location = 5,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 48,
      },
      // Input Color
      {
        .location = 6,
        .binding = 1,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .offset = 64,
      },
    };

    VkPipelineVertexInputStateCreateInfo VertexInputState = GetPipelineVertexInputStateCreateInfo();
    VertexInputState.vertexBindingDescriptionCount = Bindings.size();
    VertexInputState.pVertexBindingDescriptions = Bindings.data();
    VertexInputState.vertexAttributeDescriptionCount = Attributes.size();
    VertexInputState.pVertexAttributeDescriptions = Attributes.data();

    VkPipelineInputAssemblyStateCreateInfo InputAssemblyState = GetPipelineInputAssemblyStateCreateInfo();
    InputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    InputAssemblyState.primitiveRestartEnable = VK_FALSE;

    // @note: Flipping viewport to be like OpenGL
    VkViewport Viewport = GetViewport();
    Viewport.x = 0.0f;
    Viewport.y = static_cast<s32>(ImageSize.height);
    Viewport.width = static_cast<s32>(ImageSize.width);
    Viewport.height = -static_cast<s32>(ImageSize.height);
    Viewport.minDepth = 0.0f;
    Viewport.maxDepth = 1.0f;

    VkRect2D Scissor = GetRect2D();
    Scissor.offset = { 0, 0 };
    Scissor.extent = ImageSize;

    VkPipelineViewportStateCreateInfo ViewportState = GetPipelineViewportStateCreateInfo();
    ViewportState.viewportCount = 1;
    ViewportState.pViewports = &Viewport;
    ViewportState.scissorCount = 1;
    ViewportState.pScissors = &Scissor;

    VkPipelineRasterizationStateCreateInfo RasterizationState = GetPipelineRasterizationStateCreateInfo();
    RasterizationState.depthClampEnable = VK_FALSE;
    RasterizationState.rasterizerDiscardEnable = VK_FALSE;
    RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    RasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
    RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    RasterizationState.depthBiasEnable = VK_FALSE;
    RasterizationState.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo MultisampleState = GetPipelineMultisampleStateCreateInfo();
    MultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = GetPipelineColorBlendAttachmentState();
    ColorBlendAttachmentState.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT |
      VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT |
      VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo ColorBlendState = GetPipelineColorBlendStateCreateInfo();
    ColorBlendState.attachmentCount = 1;
    ColorBlendState.pAttachments = &ColorBlendAttachmentState;

    const std::array<VkPipelineShaderStageCreateInfo, 2> ShaderStageList = {
      VertexShaderStage,
      FragmentShaderStage,
    };

    VkPipelineDepthStencilStateCreateInfo DepthStencilState = GetPipelineDepthStencilStateCreateInfo();
    DepthStencilState.depthTestEnable = VK_TRUE;
    DepthStencilState.depthWriteEnable = VK_TRUE;
    DepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    DepthStencilState.depthBoundsTestEnable = VK_FALSE;
    DepthStencilState.minDepthBounds = 0.0f;
    DepthStencilState.maxDepthBounds = 1.0f;
    DepthStencilState.stencilTestEnable = VK_FALSE;

    VkGraphicsPipelineCreateInfo CreateInfo = GetGraphicsPipelineCreateInfo();
    CreateInfo.stageCount = ShaderStageList.size();
    CreateInfo.pStages = ShaderStageList.data();
    CreateInfo.pInputAssemblyState = &InputAssemblyState;
    CreateInfo.pVertexInputState = &VertexInputState;
    CreateInfo.pViewportState = &ViewportState;
    CreateInfo.pRasterizationState = &RasterizationState;
    CreateInfo.pMultisampleState = &MultisampleState;
    CreateInfo.pColorBlendState = &ColorBlendState;
    CreateInfo.pDepthStencilState = &DepthStencilState;
    CreateInfo.layout = Layout.GetHandle();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.subpass = 0;

    vk_graphics_pipeline Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateGraphicsPipelines(Result.Device_,
                                       nullptr,
                                       1,
                                       &CreateInfo,
                                       VK_NULL_HANDLE,
                                       &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_graphics_pipeline: " << Status);
      return {};
    }

    return Result;
  }

  vk_graphics_pipeline vk_graphics_pipeline::CreateGameplay_Offscreen(const vk_device& Device,
                                                                      const vk_pipeline_layout& Layout,
                                                                      const vk_render_pass& RenderPass,
                                                                      const VkExtent2D ImageSize)
  {
    vk_shader_module VertexShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Vertex, "gameplay_offscreen");
    vk_shader_module FragmentShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Fragment, "gameplay_offscreen");

    VkPipelineShaderStageCreateInfo VertexShaderStage = GetPipelineShaderStageCreateInfo();
    VertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
    VertexShaderStage.module = VertexShaderModule.GetHandle();
    VertexShaderStage.pName = "main";

    VkPipelineShaderStageCreateInfo FragmentShaderStage = GetPipelineShaderStageCreateInfo();
    FragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    FragmentShaderStage.module = FragmentShaderModule.GetHandle();
    FragmentShaderStage.pName = "main";

    const std::vector<VkVertexInputBindingDescription> Bindings = {
      {
        .binding = 0,
        .stride = 24,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
      },
      {
        .binding = 1,
        .stride = 72,
        .inputRate = VK_VERTEX_INPUT_RATE_INSTANCE,
      },
    };

    const std::vector<VkVertexInputAttributeDescription> Attributes = {
      // Input Position
      {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0,
      },
      // Input Normal
      {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 12,
      },
      // Input Transform[0]
      {
        .location = 2,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 0,
      },
      // Input Transform[1]
      {
        .location = 3,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 16,
      },
      // Input Transform[2]
      {
        .location = 4,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 32,
      },
      // Input Transform[3]
      {
        .location = 5,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 48,
      },
      // Input Color
      {
        .location = 6,
        .binding = 1,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .offset = 64,
      },
      // Input Is Visited (Not Used)
      {
        .location = 7,
        .binding = 1,
        .format = VK_FORMAT_R8G8B8A8_SINT,
        .offset = 68,
      },
    };

    VkPipelineVertexInputStateCreateInfo VertexInputState = GetPipelineVertexInputStateCreateInfo();
    VertexInputState.vertexBindingDescriptionCount = Bindings.size();
    VertexInputState.pVertexBindingDescriptions = Bindings.data();
    VertexInputState.vertexAttributeDescriptionCount = Attributes.size();
    VertexInputState.pVertexAttributeDescriptions = Attributes.data();

    VkPipelineInputAssemblyStateCreateInfo InputAssemblyState = GetPipelineInputAssemblyStateCreateInfo();
    InputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    InputAssemblyState.primitiveRestartEnable = VK_FALSE;

    // @note: Flipping viewport to be like OpenGL
    VkViewport Viewport = GetViewport();
    Viewport.x = 0.0f;
    Viewport.y = static_cast<s32>(ImageSize.height);
    Viewport.width = static_cast<s32>(ImageSize.width);
    Viewport.height = -static_cast<s32>(ImageSize.height);
    Viewport.minDepth = 0.0f;
    Viewport.maxDepth = 1.0f;

    VkRect2D Scissor = GetRect2D();
    Scissor.offset = { 0, 0 };
    Scissor.extent = ImageSize;

    VkPipelineViewportStateCreateInfo ViewportState = GetPipelineViewportStateCreateInfo();
    ViewportState.viewportCount = 1;
    ViewportState.pViewports = &Viewport;
    ViewportState.scissorCount = 1;
    ViewportState.pScissors = &Scissor;

    VkPipelineRasterizationStateCreateInfo RasterizationState = GetPipelineRasterizationStateCreateInfo();
    RasterizationState.depthClampEnable = VK_FALSE;
    RasterizationState.rasterizerDiscardEnable = VK_FALSE;
    RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    RasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
    RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    RasterizationState.depthBiasEnable = VK_FALSE;
    RasterizationState.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo MultisampleState = GetPipelineMultisampleStateCreateInfo();
    MultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    const std::array<VkPipelineColorBlendAttachmentState, 3> ColorBlendAttacthments = {
      VkPipelineColorBlendAttachmentState
      {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT,
      },
      {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT,
      },
      {
        .blendEnable = VK_FALSE,
        .colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT,
      },
    };

    VkPipelineColorBlendStateCreateInfo ColorBlendState = GetPipelineColorBlendStateCreateInfo();
    ColorBlendState.attachmentCount = ColorBlendAttacthments.size();
    ColorBlendState.pAttachments = ColorBlendAttacthments.data();

    const std::array<VkPipelineShaderStageCreateInfo, 2> ShaderStageList = {
      VertexShaderStage,
      FragmentShaderStage,
    };

    VkPipelineDepthStencilStateCreateInfo DepthStencilState = GetPipelineDepthStencilStateCreateInfo();
    DepthStencilState.depthTestEnable = VK_TRUE;
    DepthStencilState.depthWriteEnable = VK_TRUE;
    DepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    DepthStencilState.depthBoundsTestEnable = VK_FALSE;
    DepthStencilState.minDepthBounds = 0.0f;
    DepthStencilState.maxDepthBounds = 1.0f;
    DepthStencilState.stencilTestEnable = VK_FALSE;

    VkGraphicsPipelineCreateInfo CreateInfo = GetGraphicsPipelineCreateInfo();
    CreateInfo.stageCount = ShaderStageList.size();
    CreateInfo.pStages = ShaderStageList.data();
    CreateInfo.pInputAssemblyState = &InputAssemblyState;
    CreateInfo.pVertexInputState = &VertexInputState;
    CreateInfo.pViewportState = &ViewportState;
    CreateInfo.pRasterizationState = &RasterizationState;
    CreateInfo.pMultisampleState = &MultisampleState;
    CreateInfo.pColorBlendState = &ColorBlendState;
    CreateInfo.pDepthStencilState = &DepthStencilState;
    CreateInfo.layout = Layout.GetHandle();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.subpass = 0;

    vk_graphics_pipeline Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateGraphicsPipelines(Result.Device_,
                                       nullptr,
                                       1,
                                       &CreateInfo,
                                       VK_NULL_HANDLE,
                                       &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_graphics_pipeline: " << Status);
      return {};
    }

    return Result;
  }

  vk_graphics_pipeline vk_graphics_pipeline::CreateGameplay_Onscreen(const vk_device& Device,
                                                                     const vk_pipeline_layout& Layout,
                                                                     const vk_render_pass& RenderPass,
                                                                     const VkExtent2D ImageSize)
  {
    vk_shader_module VertexShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Vertex, "gameplay_onscreen");
    vk_shader_module FragmentShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Fragment, "gameplay_onscreen");

    VkPipelineShaderStageCreateInfo VertexShaderStage = GetPipelineShaderStageCreateInfo();
    VertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
    VertexShaderStage.module = VertexShaderModule.GetHandle();
    VertexShaderStage.pName = "main";

    VkPipelineShaderStageCreateInfo FragmentShaderStage = GetPipelineShaderStageCreateInfo();
    FragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    FragmentShaderStage.module = FragmentShaderModule.GetHandle();
    FragmentShaderStage.pName = "main";

    const std::vector<VkVertexInputBindingDescription> Bindings = {
    };

    const std::vector<VkVertexInputAttributeDescription> Attributes = {
    };

    VkPipelineVertexInputStateCreateInfo VertexInputState = GetPipelineVertexInputStateCreateInfo();
    VertexInputState.vertexBindingDescriptionCount = Bindings.size();
    VertexInputState.pVertexBindingDescriptions = Bindings.data();
    VertexInputState.vertexAttributeDescriptionCount = Attributes.size();
    VertexInputState.pVertexAttributeDescriptions = Attributes.data();

    VkPipelineInputAssemblyStateCreateInfo InputAssemblyState = GetPipelineInputAssemblyStateCreateInfo();
    InputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    InputAssemblyState.primitiveRestartEnable = VK_FALSE;

    // @note: Flipping viewport to be like OpenGL
    VkViewport Viewport = GetViewport();
    Viewport.x = 0.0f;
    Viewport.y = static_cast<s32>(ImageSize.height);
    Viewport.width = static_cast<s32>(ImageSize.width);
    Viewport.height = -static_cast<s32>(ImageSize.height);
    Viewport.minDepth = 0.0f;
    Viewport.maxDepth = 1.0f;

    VkRect2D Scissor = GetRect2D();
    Scissor.offset = { 0, 0 };
    Scissor.extent = ImageSize;

    VkPipelineViewportStateCreateInfo ViewportState = GetPipelineViewportStateCreateInfo();
    ViewportState.viewportCount = 1;
    ViewportState.pViewports = &Viewport;
    ViewportState.scissorCount = 1;
    ViewportState.pScissors = &Scissor;

    VkPipelineRasterizationStateCreateInfo RasterizationState = GetPipelineRasterizationStateCreateInfo();
    RasterizationState.depthClampEnable = VK_FALSE;
    RasterizationState.rasterizerDiscardEnable = VK_FALSE;
    RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    RasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
    RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    RasterizationState.depthBiasEnable = VK_FALSE;
    RasterizationState.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo MultisampleState = GetPipelineMultisampleStateCreateInfo();
    MultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = GetPipelineColorBlendAttachmentState();
    ColorBlendAttachmentState.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT |
      VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT |
      VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo ColorBlendState = GetPipelineColorBlendStateCreateInfo();
    ColorBlendState.attachmentCount = 1;
    ColorBlendState.pAttachments = &ColorBlendAttachmentState;

    const std::array<VkPipelineShaderStageCreateInfo, 2> ShaderStageList = {
      VertexShaderStage,
      FragmentShaderStage,
    };

    VkPipelineDepthStencilStateCreateInfo DepthStencilState = GetPipelineDepthStencilStateCreateInfo();
    DepthStencilState.depthTestEnable = VK_TRUE;
    DepthStencilState.depthWriteEnable = VK_FALSE;
    DepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    DepthStencilState.depthBoundsTestEnable = VK_FALSE;
    DepthStencilState.minDepthBounds = 0.0f;
    DepthStencilState.maxDepthBounds = 1.0f;
    DepthStencilState.stencilTestEnable = VK_FALSE;

    VkGraphicsPipelineCreateInfo CreateInfo = GetGraphicsPipelineCreateInfo();
    CreateInfo.stageCount = ShaderStageList.size();
    CreateInfo.pStages = ShaderStageList.data();
    CreateInfo.pInputAssemblyState = &InputAssemblyState;
    CreateInfo.pVertexInputState = &VertexInputState;
    CreateInfo.pViewportState = &ViewportState;
    CreateInfo.pRasterizationState = &RasterizationState;
    CreateInfo.pMultisampleState = &MultisampleState;
    CreateInfo.pColorBlendState = &ColorBlendState;
    CreateInfo.pDepthStencilState = &DepthStencilState;
    CreateInfo.layout = Layout.GetHandle();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.subpass = 1;

    vk_graphics_pipeline Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateGraphicsPipelines(Result.Device_,
                                       nullptr,
                                       1,
                                       &CreateInfo,
                                       VK_NULL_HANDLE,
                                       &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_graphics_pipeline: " << Status);
      return {};
    }

    return Result;
  }

  vk_graphics_pipeline vk_graphics_pipeline::CreateGameplay_Minimap(const vk_device& Device,
                                                                    const vk_pipeline_layout& Layout,
                                                                    const vk_render_pass& RenderPass,
                                                                    const VkExtent2D ImageSize,
                                                                    const VkExtent2D MinimapSize)
  {
    vk_shader_module VertexShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Vertex, "gameplay_minimap");
    vk_shader_module FragmentShaderModule = vk_shader_module::CreateFromName(Device, vk_shader_module::stage::Fragment, "gameplay_minimap");

    VkPipelineShaderStageCreateInfo VertexShaderStage = GetPipelineShaderStageCreateInfo();
    VertexShaderStage.stage = VK_SHADER_STAGE_VERTEX_BIT;
    VertexShaderStage.module = VertexShaderModule.GetHandle();
    VertexShaderStage.pName = "main";

    VkPipelineShaderStageCreateInfo FragmentShaderStage = GetPipelineShaderStageCreateInfo();
    FragmentShaderStage.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    FragmentShaderStage.module = FragmentShaderModule.GetHandle();
    FragmentShaderStage.pName = "main";

    const std::vector<VkVertexInputBindingDescription> Bindings = {
      {
        .binding = 0,
        .stride = 24,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
      },
      {
        .binding = 1,
        .stride = 72,
        .inputRate = VK_VERTEX_INPUT_RATE_INSTANCE,
      },
    };

    const std::vector<VkVertexInputAttributeDescription> Attributes = {
      // Input Position
      {
        .location = 0,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 0,
      },
      // Input Normal
      {
        .location = 1,
        .binding = 0,
        .format = VK_FORMAT_R32G32B32_SFLOAT,
        .offset = 12,
      },
      // Input Transform[0]
      {
        .location = 2,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 0,
      },
      // Input Transform[1]
      {
        .location = 3,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 16,
      },
      // Input Transform[2]
      {
        .location = 4,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 32,
      },
      // Input Transform[3]
      {
        .location = 5,
        .binding = 1,
        .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        .offset = 48,
      },
      // Input Color
      {
        .location = 6,
        .binding = 1,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .offset = 64,
      },
      // Input Is Visited
      {
        .location = 7,
        .binding = 1,
        .format = VK_FORMAT_R8G8B8A8_SINT,
        .offset = 68,
      },
    };

    VkPipelineVertexInputStateCreateInfo VertexInputState = GetPipelineVertexInputStateCreateInfo();
    VertexInputState.vertexBindingDescriptionCount = Bindings.size();
    VertexInputState.pVertexBindingDescriptions = Bindings.data();
    VertexInputState.vertexAttributeDescriptionCount = Attributes.size();
    VertexInputState.pVertexAttributeDescriptions = Attributes.data();

    VkPipelineInputAssemblyStateCreateInfo InputAssemblyState = GetPipelineInputAssemblyStateCreateInfo();
    InputAssemblyState.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    InputAssemblyState.primitiveRestartEnable = VK_FALSE;

    const auto x = static_cast<s32>(ImageSize.width - MinimapSize.width);
    const auto y = 0;

    VkViewport Viewport = GetViewport();
    Viewport.x = x;
    Viewport.y = y;
    Viewport.width = static_cast<s32>(MinimapSize.width);
    Viewport.height = static_cast<s32>(MinimapSize.height);
    Viewport.minDepth = 0.0f;
    Viewport.maxDepth = 1.0f;

    VkRect2D Scissor = GetRect2D();
    Scissor.offset = { x, y };
    Scissor.extent = MinimapSize;

    VkPipelineViewportStateCreateInfo ViewportState = GetPipelineViewportStateCreateInfo();
    ViewportState.viewportCount = 1;
    ViewportState.pViewports = &Viewport;
    ViewportState.scissorCount = 1;
    ViewportState.pScissors = &Scissor;

    VkPipelineRasterizationStateCreateInfo RasterizationState = GetPipelineRasterizationStateCreateInfo();
    RasterizationState.depthClampEnable = VK_FALSE;
    RasterizationState.rasterizerDiscardEnable = VK_FALSE;
    RasterizationState.polygonMode = VK_POLYGON_MODE_FILL;
    RasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
    RasterizationState.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    RasterizationState.depthBiasEnable = VK_FALSE;
    RasterizationState.lineWidth = 1.0f;

    VkPipelineMultisampleStateCreateInfo MultisampleState = GetPipelineMultisampleStateCreateInfo();
    MultisampleState.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineColorBlendAttachmentState ColorBlendAttachmentState = GetPipelineColorBlendAttachmentState();
    ColorBlendAttachmentState.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT |
      VK_COLOR_COMPONENT_G_BIT |
      VK_COLOR_COMPONENT_B_BIT |
      VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo ColorBlendState = GetPipelineColorBlendStateCreateInfo();
    ColorBlendState.attachmentCount = 1;
    ColorBlendState.pAttachments = &ColorBlendAttachmentState;

    const std::array<VkPipelineShaderStageCreateInfo, 2> ShaderStageList = {
      VertexShaderStage,
      FragmentShaderStage,
    };

    VkPipelineDepthStencilStateCreateInfo DepthStencilState = GetPipelineDepthStencilStateCreateInfo();
    DepthStencilState.depthTestEnable = VK_TRUE;
    DepthStencilState.depthWriteEnable = VK_TRUE;
    DepthStencilState.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
    DepthStencilState.depthBoundsTestEnable = VK_FALSE;
    DepthStencilState.minDepthBounds = 0.0f;
    DepthStencilState.maxDepthBounds = 1.0f;
    DepthStencilState.stencilTestEnable = VK_FALSE;

    VkGraphicsPipelineCreateInfo CreateInfo = GetGraphicsPipelineCreateInfo();
    CreateInfo.stageCount = ShaderStageList.size();
    CreateInfo.pStages = ShaderStageList.data();
    CreateInfo.pInputAssemblyState = &InputAssemblyState;
    CreateInfo.pVertexInputState = &VertexInputState;
    CreateInfo.pViewportState = &ViewportState;
    CreateInfo.pRasterizationState = &RasterizationState;
    CreateInfo.pMultisampleState = &MultisampleState;
    CreateInfo.pColorBlendState = &ColorBlendState;
    CreateInfo.pDepthStencilState = &DepthStencilState;
    CreateInfo.layout = Layout.GetHandle();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.subpass = 2;

    vk_graphics_pipeline Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateGraphicsPipelines(Result.Device_,
                                       nullptr,
                                       1,
                                       &CreateInfo,
                                       VK_NULL_HANDLE,
                                       &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_graphics_pipeline: " << Status);
      return {};
    }

    return Result;
  }

  vk_graphics_pipeline::vk_graphics_pipeline(vk_graphics_pipeline&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_graphics_pipeline& vk_graphics_pipeline::operator=(vk_graphics_pipeline&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_graphics_pipeline::~vk_graphics_pipeline()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_graphics_pipeline...");
      vkDestroyPipeline(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkPipeline vk_graphics_pipeline::GetHandle() const
  {
    return Handle_;
  }

}  // myran
