#include "getters.h"

namespace myran {

  VkCommandBufferBeginInfo GetCommandBufferBeginInfo()
  {
    VkCommandBufferBeginInfo CommandBufferBeginInfo = {};
    CommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    return CommandBufferBeginInfo;
  }

  VkRenderPassBeginInfo GetRenderPassBeginInfo()
  {
    VkRenderPassBeginInfo RenderPassBeginInfo = {};
    RenderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    return RenderPassBeginInfo;
  }

  VkClearColorValue GetClearColorValue(const f32 Red, const f32 Green, const f32 Blue, const f32 Alpha)
  {
    VkClearColorValue ClearColorValue = {};
    ClearColorValue.float32[0] = Red;
    ClearColorValue.float32[1] = Green;
    ClearColorValue.float32[2] = Blue;
    ClearColorValue.float32[3] = Alpha;
    return ClearColorValue;
  }

  VkClearValue GetClearValue(const f32 Red, const f32 Green, const f32 Blue, const f32 Alpha)
  {
    VkClearValue ClearColor = {};
    ClearColor.color = GetClearColorValue(Red, Green, Blue, Alpha);
    return ClearColor;
  }

  VkSubmitInfo GetSubmitInfo()
  {
    VkSubmitInfo SubmitInfo = {};
    SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    return SubmitInfo;
  }

  VkPipelineVertexInputStateCreateInfo GetPipelineVertexInputStateCreateInfo()
  {
    VkPipelineVertexInputStateCreateInfo PipelineVertexInputStateCreateInfo = {};
    PipelineVertexInputStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    return PipelineVertexInputStateCreateInfo;
  }

  VkPipelineInputAssemblyStateCreateInfo GetPipelineInputAssemblyStateCreateInfo()
  {
    VkPipelineInputAssemblyStateCreateInfo PipelineInputAssemblyStateCreateInfo = {};
    PipelineInputAssemblyStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    PipelineInputAssemblyStateCreateInfo.primitiveRestartEnable = VK_FALSE;
    return PipelineInputAssemblyStateCreateInfo;
  }

  VkViewport GetViewport()
  {
    VkViewport Viewport = {};
    return Viewport;
  }

  VkRect2D GetRect2D()
  {
    VkRect2D Rect2D = {};
    return Rect2D;
  }

  VkPipelineViewportStateCreateInfo GetPipelineViewportStateCreateInfo()
  {
    VkPipelineViewportStateCreateInfo PipelineViewportStateCreateInfo = {};
    PipelineViewportStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    return PipelineViewportStateCreateInfo;
  }

  VkPipelineRasterizationStateCreateInfo GetPipelineRasterizationStateCreateInfo()
  {
    VkPipelineRasterizationStateCreateInfo PipelineRasterizerStateCreateInfo = {};
    PipelineRasterizerStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    PipelineRasterizerStateCreateInfo.depthClampEnable = VK_FALSE;
    PipelineRasterizerStateCreateInfo.rasterizerDiscardEnable = VK_FALSE;
    PipelineRasterizerStateCreateInfo.polygonMode = VK_POLYGON_MODE_FILL;
    PipelineRasterizerStateCreateInfo.lineWidth = 1.0f;
    PipelineRasterizerStateCreateInfo.cullMode = VK_CULL_MODE_BACK_BIT;
    PipelineRasterizerStateCreateInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    PipelineRasterizerStateCreateInfo.depthBiasEnable = VK_FALSE;
    PipelineRasterizerStateCreateInfo.depthBiasConstantFactor = 0.0f;
    PipelineRasterizerStateCreateInfo.depthBiasClamp = 0.0f;
    PipelineRasterizerStateCreateInfo.depthBiasSlopeFactor = 0.0f;
    return PipelineRasterizerStateCreateInfo;
  }

  VkPipelineMultisampleStateCreateInfo GetPipelineMultisampleStateCreateInfo()
  {
    VkPipelineMultisampleStateCreateInfo PipelineMultisampleStateCreateInfo = {};
    PipelineMultisampleStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    PipelineMultisampleStateCreateInfo.sampleShadingEnable = VK_FALSE;
    PipelineMultisampleStateCreateInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    PipelineMultisampleStateCreateInfo.minSampleShading = 1.0f;
    PipelineMultisampleStateCreateInfo.alphaToCoverageEnable = VK_FALSE;
    PipelineMultisampleStateCreateInfo.alphaToOneEnable = VK_FALSE;
    return PipelineMultisampleStateCreateInfo;
  }

  VkPipelineCacheCreateInfo GetPipelineCacheCreateInfo()
  {
    VkPipelineCacheCreateInfo PipelineCacheCreateInfo = {};
    PipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
    return PipelineCacheCreateInfo;
  }

  VkPipelineLayoutCreateInfo GetPipelineLayoutCreateInfo()
  {
    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo = {};
    PipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    return PipelineLayoutCreateInfo;
  }

  VkPipelineColorBlendAttachmentState GetPipelineColorBlendAttachmentState()
  {
    VkPipelineColorBlendAttachmentState PipelineColorBlendAttachmentState = {};
    PipelineColorBlendAttachmentState.colorWriteMask =
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    PipelineColorBlendAttachmentState.blendEnable = VK_FALSE;
    PipelineColorBlendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    PipelineColorBlendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    PipelineColorBlendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
    PipelineColorBlendAttachmentState.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    PipelineColorBlendAttachmentState.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    PipelineColorBlendAttachmentState.alphaBlendOp = VK_BLEND_OP_ADD;
    return PipelineColorBlendAttachmentState;
  }

  VkPipelineColorBlendStateCreateInfo GetPipelineColorBlendStateCreateInfo()
  {
    VkPipelineColorBlendStateCreateInfo PipelineColorBlendStateCreateInfo = {};
    PipelineColorBlendStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    PipelineColorBlendStateCreateInfo.logicOpEnable = VK_FALSE;
    PipelineColorBlendStateCreateInfo.logicOp = VK_LOGIC_OP_COPY;
    PipelineColorBlendStateCreateInfo.blendConstants[0] = 0.0f;
    PipelineColorBlendStateCreateInfo.blendConstants[1] = 0.0f;
    PipelineColorBlendStateCreateInfo.blendConstants[2] = 0.0f;
    PipelineColorBlendStateCreateInfo.blendConstants[3] = 0.0f;
    return PipelineColorBlendStateCreateInfo;
  }

  VkPipelineLayoutCreateInfo GetVkPipelineLayoutCreateInfo()
  {
    VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo = {};
    PipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    return PipelineLayoutCreateInfo;
  }

  VkPipelineShaderStageCreateInfo GetPipelineShaderStageCreateInfo()
  {
    VkPipelineShaderStageCreateInfo PipelineShaderStageCreateInfo = {};
    PipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    return PipelineShaderStageCreateInfo;
  }

  VkGraphicsPipelineCreateInfo GetGraphicsPipelineCreateInfo()
  {
    VkGraphicsPipelineCreateInfo GraphicsPipelineCreateInfo = {};
    GraphicsPipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    GraphicsPipelineCreateInfo.basePipelineIndex = -1;
    return GraphicsPipelineCreateInfo;
  }

  VkDescriptorSetAllocateInfo GetDescriptorSetAllocateInfo()
  {
    VkDescriptorSetAllocateInfo DescriptorSetAllocateInfo = {};
    DescriptorSetAllocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    return DescriptorSetAllocateInfo;
  }

  VkDescriptorBufferInfo GetDescriptorBufferInfo()
  {
    VkDescriptorBufferInfo DescriptorBufferInfo = {};
    return DescriptorBufferInfo;
  }

  VkDescriptorImageInfo GetDescriptorImageInfo()
  {
    VkDescriptorImageInfo DescriptorImageInfo = {};
    return DescriptorImageInfo;
  }

  VkWriteDescriptorSet GetWriteDescriptorSet()
  {
    VkWriteDescriptorSet WriteDescriptorSet = {};
    WriteDescriptorSet.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    return WriteDescriptorSet;
  }

  VkDescriptorSetLayoutCreateInfo GetDescriptorSetLayoutCreateInfo()
  {
    VkDescriptorSetLayoutCreateInfo DescriptorSetLayoutCreateInfo = {};
    DescriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    return DescriptorSetLayoutCreateInfo;
  }

  VkDescriptorSetLayoutBinding GetDescriptorSetLayoutBinding()
  {
    VkDescriptorSetLayoutBinding DescriptorSetLayoutBinding = {};
    return DescriptorSetLayoutBinding;
  }

  VkDescriptorPoolSize GetDescriptorPoolSize()
  {
    VkDescriptorPoolSize DescriptorPoolSize = {};
    return DescriptorPoolSize;
  }

  VkDescriptorPoolCreateInfo GetDescriptorPoolCreateInfo()
  {
    VkDescriptorPoolCreateInfo DescriptorPoolCreateInfo = {};
    DescriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    return DescriptorPoolCreateInfo;
  }

  VkMappedMemoryRange GetMappedMemoryRange()
  {
    VkMappedMemoryRange MappedMemoryRange = {};
    MappedMemoryRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    return MappedMemoryRange;
  }

  VkMemoryAllocateInfo GetMemoryAllocateInfo()
  {
    VkMemoryAllocateInfo MemoryAllocateInfo = {};
    MemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    return MemoryAllocateInfo;
  }

  VkMemoryAllocateInfo GetMemoryAllocateInfo(const VkDeviceSize Size)
  {
    VkMemoryAllocateInfo MemoryAllocateInfo = GetMemoryAllocateInfo();
    MemoryAllocateInfo.allocationSize = Size;
    return MemoryAllocateInfo;
  }

  VkBufferCreateInfo GetBufferCreateInfo()
  {
    VkBufferCreateInfo BufferCreateInfo = {};
    BufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    return BufferCreateInfo;
  }

  VkCommandBufferAllocateInfo GetCommandBufferAllocateInfo()
  {
    VkCommandBufferAllocateInfo CommandBufferAllocateInfo = {};
    CommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    return CommandBufferAllocateInfo;
  }

  VkBufferCopy GetBufferCopy()
  {
    VkBufferCopy BufferCopy = {};
    return BufferCopy;
  }

  VkBufferImageCopy GetBufferImageCopy()
  {
    VkBufferImageCopy BufferImageCopy = {};
    return BufferImageCopy;
  }

  VkFramebufferCreateInfo GetFramebufferCreateInfo()
  {
    VkFramebufferCreateInfo FramebufferCreateInfo = {};
    FramebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    return FramebufferCreateInfo;
  }

  VkSwapchainCreateInfoKHR GetSwapchainCreateInfoKHR()
  {
    VkSwapchainCreateInfoKHR SwapchainCreateInfo = {};
    SwapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    return SwapchainCreateInfo;
  }

  VkImageCreateInfo GetImageCreateInfo()
  {
    VkImageCreateInfo ImageCreateInfo = {};
    ImageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    return ImageCreateInfo;
  }

  VkImageMemoryBarrier GetImageMemoryBarrier()
  {
    VkImageMemoryBarrier ImageMemoryBarrier = {};
    ImageMemoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    return ImageMemoryBarrier;
  }

  VkImageSubresourceRange GetImageSubresourceRange()
  {
    VkImageSubresourceRange ImageSubresourceRange = {};
    return ImageSubresourceRange;
  }

  VkImageViewCreateInfo GetImageViewCreateInfo()
  {
    VkImageViewCreateInfo ImageViewCreateInfo = {};
    ImageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    ImageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    ImageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    ImageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    ImageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    ImageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    ImageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    ImageViewCreateInfo.subresourceRange.levelCount = 1;
    ImageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    ImageViewCreateInfo.subresourceRange.layerCount = 1;
    return ImageViewCreateInfo;
  }

  VkSamplerCreateInfo GetSamplerCreateInfo()
  {
    VkSamplerCreateInfo SamplerCreateInfo = {};
    SamplerCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    return SamplerCreateInfo;
  }

  VkSemaphoreCreateInfo GetSemaphoreCreateInfo()
  {
    VkSemaphoreCreateInfo SemaphoreCreateInfo = {};
    SemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    return SemaphoreCreateInfo;
  }

  VkFenceCreateInfo GetFenceCreateInfo()
  {
    VkFenceCreateInfo FenceCreateInfo = {};
    FenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    return FenceCreateInfo;
  }

  VkShaderModuleCreateInfo GetShaderModuleCreateInfo()
  {
    VkShaderModuleCreateInfo ShaderModuleCreateInfo = {};
    ShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    return ShaderModuleCreateInfo;
  }

  VkAttachmentDescription GetAttachmentDescription()
  {
    VkAttachmentDescription AttachmentDescription = {};
    AttachmentDescription.samples = VK_SAMPLE_COUNT_1_BIT;
    AttachmentDescription.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    AttachmentDescription.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    AttachmentDescription.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    AttachmentDescription.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    AttachmentDescription.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    AttachmentDescription.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    return AttachmentDescription;
  }

  VkAttachmentReference GetAttachmentReference()
  {
    VkAttachmentReference AttachmentReference = {};
    AttachmentReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    return AttachmentReference;
  }

  VkSubpassDescription GetSubpassDescription()
  {
    VkSubpassDescription SubpassDescription = {};
    return SubpassDescription;
  }

  VkSubpassDependency GetSubpassDependency()
  {
    // NOTE(ra-hol) : We do this because there are two implicit Subpasses, at the beginning and in the end,
    // and we have to make sure that the beginning one is started at the correct time
    VkSubpassDependency SubpassDependency = {};
    SubpassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    SubpassDependency.dstSubpass = 0;
    SubpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    SubpassDependency.srcAccessMask = 0;
    SubpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    SubpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    return SubpassDependency;
  }

  VkRenderPassCreateInfo GetRenderPassCreateInfo()
  {
    VkRenderPassCreateInfo RenderPassCreateInfo = {};
    RenderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    return RenderPassCreateInfo;
  }

  VkApplicationInfo GetApplicationInfo()
  {
    VkApplicationInfo ApplicationInfo = {};
    ApplicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    return ApplicationInfo;
  }
  VkInstanceCreateInfo GetInstanceCreateInfo()
  {
    VkInstanceCreateInfo InstanceCreateInfo = {};
    InstanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    return InstanceCreateInfo;
  }

  VkCommandPoolCreateInfo GetCommandPoolCreateInfo()
  {
    VkCommandPoolCreateInfo CommandPoolCreateInfo = {};
    CommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    return CommandPoolCreateInfo;
  }

  VkDeviceQueueCreateInfo GetDeviceQueueCreateInfo()
  {
    VkDeviceQueueCreateInfo DeviceQueueCreateInfo = {};
    DeviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    return DeviceQueueCreateInfo;
  }

  VkDeviceCreateInfo GetDeviceCreateInfo()
  {
    VkDeviceCreateInfo DeviceCreateInfo = {};
    DeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    return DeviceCreateInfo;
  }

  VkVertexInputBindingDescription GetVertexInputBindingDescription()
  {
    VkVertexInputBindingDescription VertexInputBindingDescription = {};
    return VertexInputBindingDescription;
  }

  VkVertexInputAttributeDescription GetVertexInputAttributeDescription()
  {
    VkVertexInputAttributeDescription VertexInputAttributeDescription = {};
    return VertexInputAttributeDescription;
  }

  VkPresentInfoKHR GetPresentInfoKHR()
  {

    VkPresentInfoKHR PresentationInfoKHR = {};
    PresentationInfoKHR.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    return PresentationInfoKHR;
  }

  VkPipelineDynamicStateCreateInfo GetPipelineDynamicStateCreateInfo()
  {
    VkPipelineDynamicStateCreateInfo PipelineDynamicStateCreateInfo = {};
    PipelineDynamicStateCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    return PipelineDynamicStateCreateInfo;
  }

  VkBufferMemoryBarrier GetBufferMemoryBarrier()
  {
    VkBufferMemoryBarrier Result = {};
    Result.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    Result.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    Result.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    return Result;
  }

  VkPipelineDepthStencilStateCreateInfo GetPipelineDepthStencilStateCreateInfo()
  {
    VkPipelineDepthStencilStateCreateInfo Result = {};
    Result.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    return Result;
  }

}  // myran
