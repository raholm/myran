#include "buffer.h"
#include "getters.h"

#include <myran/debug.h>

namespace myran {

  vk_buffer vk_buffer::CreateVertexBuffer(const vk_device& Device, const VkDeviceSize Size)
  {
    LogInfo("Creating vertex buffer " << Size << "...");

    VkBufferCreateInfo CreateInfo = GetBufferCreateInfo();
    CreateInfo.size = Size;
    CreateInfo.usage =
      VK_BUFFER_USAGE_TRANSFER_DST_BIT |
      VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

    vk_buffer Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateBuffer(Result.Device_,
                                            &CreateInfo,
                                            VK_NULL_HANDLE,
                                            &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk buffer: " << Status);
      return Result;
    }

    vkGetBufferMemoryRequirements(Result.Device_, Result.Handle_, &Result.MemoryRequirements_);
    return Result;
  }

  vk_buffer vk_buffer::CreateUniformBuffer(const vk_device& Device, const VkDeviceSize Size)
  {
    LogInfo("Creating uniform buffer " << Size << "...");

    VkBufferCreateInfo CreateInfo = GetBufferCreateInfo();
    CreateInfo.size = Size;
    CreateInfo.usage =
      VK_BUFFER_USAGE_TRANSFER_DST_BIT |
      VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

    vk_buffer Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateBuffer(Result.Device_,
                                            &CreateInfo,
                                            VK_NULL_HANDLE,
                                            &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk buffer: " << Status);
      return Result;
    }

    vkGetBufferMemoryRequirements(Result.Device_, Result.Handle_, &Result.MemoryRequirements_);
    return Result;
  }

  vk_buffer vk_buffer::CreateStageBuffer(const vk_device& Device, const VkDeviceSize Size)
  {
    LogInfo("Creating stage buffer " << Size << "...");

    VkBufferCreateInfo CreateInfo = GetBufferCreateInfo();
    CreateInfo.size = Size;
    CreateInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    vk_buffer Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateBuffer(Result.Device_,
                                            &CreateInfo,
                                            VK_NULL_HANDLE,
                                            &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk buffer: " << Status);
      return Result;
    }

    vkGetBufferMemoryRequirements(Result.Device_, Result.Handle_, &Result.MemoryRequirements_);
    return Result;
  }

  vk_buffer::vk_buffer(vk_buffer&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
    std::swap(MemoryRequirements_, Other.MemoryRequirements_);
  }

  vk_buffer& vk_buffer::operator=(vk_buffer&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    std::swap(MemoryRequirements_, Rhs.MemoryRequirements_);
    return *this;
  }

  vk_buffer::~vk_buffer()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_buffer...");
      vkDestroyBuffer(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkBuffer vk_buffer::GetHandle() const
  {
    return Handle_;
  }

  VkDeviceSize vk_buffer::GetMemoryOffset() const
  {
    return MemoryOffset_;
  }

  VkDeviceSize vk_buffer::GetRequiredSize() const
  {
    return MemoryRequirements_.size;
  }

  VkDeviceSize vk_buffer::GetNextAlignedOffset(const VkDeviceSize Offset) const
  {
    const auto Alignment = MemoryRequirements_.alignment;
    const auto Missing = Offset % Alignment;

    if (Missing == 0)
      return Offset;

    return Offset + (Alignment - Missing);
  }

  void vk_buffer::Bind(const vk_memory& Memory, const VkDeviceSize Offset)
  {
    MemoryOffset_ = Offset;

    vkBindBufferMemory(Device_, Handle_, Memory.GetHandle(), Offset);
  }

}  // myran
