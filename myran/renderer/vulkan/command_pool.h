#pragma once

#include <vector>

#include "device.h"
#include "command_buffer.h"
#include "status.h"

namespace myran {

  class vk_command_pool
  {
  public:
    vk_command_pool() = default;
    vk_command_pool(const u32 QueueFamilyIndex,
                    const vk_device& Device);

    vk_command_pool(vk_command_pool&& Other);
    vk_command_pool& operator=(vk_command_pool&& Rhs);

    ~vk_command_pool();

    vk_command_buffer AllocateCommandBuffer();
    std::vector<vk_command_buffer> AllocateCommandBuffers(const u32 Count);

    VkCommandPool GetHandle() const;

  private:
    VkCommandPool Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
