#include "semaphore.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_semaphore::vk_semaphore(const vk_device& Device)
    : Device_(Device.GetHandle())
  {
    VkSemaphoreCreateInfo CreateInfo = GetSemaphoreCreateInfo();

    vk_status Status;
    Status = vkCreateSemaphore(Device_,
                               &CreateInfo,
                               VK_NULL_HANDLE,
                               &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_semaphore: " << Status);
      return;
    }
  }

  vk_semaphore::vk_semaphore(vk_semaphore&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_semaphore& vk_semaphore::operator=(vk_semaphore&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_semaphore::~vk_semaphore()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_semaphore...");
      vkDestroySemaphore(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkSemaphore vk_semaphore::GetHandle() const
  {
    return Handle_;
  }

}  // myran
