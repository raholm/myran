#include "descriptor_set.h"
#include "getters.h"

#include <myran/debug.h>

namespace myran {

  vk_descriptor_set vk_descriptor_set::Create(const vk_device& Device,
                                              const vk_descriptor_pool& Pool,
                                              const vk_descriptor_set_layout& Layout)
  {
    LogInfo("Creating global observer descriptor set");

    const auto LayoutHandle = Layout.GetHandle();

    VkDescriptorSetAllocateInfo AllocateInfo = GetDescriptorSetAllocateInfo();
    AllocateInfo.descriptorPool = Pool.GetHandle();
    AllocateInfo.descriptorSetCount = 1;
    AllocateInfo.pSetLayouts = &LayoutHandle;

    vk_descriptor_set Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkAllocateDescriptorSets(Result.Device_,
                                                      &AllocateInfo,
                                                      &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk descriptor set: " << Status);
      return Result;
    }

    Result.Pool_ = Pool.GetHandle();
    return Result;
  }

  vk_descriptor_set::vk_descriptor_set(vk_descriptor_set&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
    std::swap(Pool_, Other.Pool_);
  }

  vk_descriptor_set& vk_descriptor_set::operator=(vk_descriptor_set&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    std::swap(Pool_, Rhs.Pool_);
    return *this;
  }

  vk_descriptor_set::~vk_descriptor_set()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk descriptor set...");
      // vkFreeDescriptorSets(Device_, Pool_, 1, &Handle_);
    }
  }

  VkDescriptorSet vk_descriptor_set::GetHandle() const
  {
    return Handle_;
  }

}  // myran
