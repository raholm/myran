#pragma once

#include "device.h"
#include "status.h"
#include "descriptor_set_layout.h"

namespace myran {

  class vk_pipeline_layout
  {
  public:
    static vk_pipeline_layout CreateGlobalObserver(const vk_device& Device,
                                                   const vk_descriptor_set_layout& DescriptorSetLayout);
    static vk_pipeline_layout CreateGameplay(const vk_device& Device,
                                             const vk_descriptor_set_layout& DescriptorSetLayout);

  public:
    vk_pipeline_layout() = default;

    vk_pipeline_layout(vk_pipeline_layout&& Other);
    vk_pipeline_layout& operator=(vk_pipeline_layout&& Rhs);

    ~vk_pipeline_layout();

    VkPipelineLayout GetHandle() const;

  private:
    VkPipelineLayout Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
