#include "memory.h"
#include "getters.h"

#include <myran/debug.h>

namespace myran {

  vk_memory vk_memory::CreateDeviceMemory(const vk_device& Device,
                                          const vk_physical_device& PhysicalDevice,
                                          const VkDeviceSize Size)
  {
    LogInfo("Creating device memory " << Size << "...");

    VkMemoryAllocateInfo AllocateInfo = GetMemoryAllocateInfo();
    AllocateInfo.allocationSize = Size;
    AllocateInfo.memoryTypeIndex = PhysicalDevice.GetMemoryTypeIndex(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    vk_memory Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkAllocateMemory(Result.Device_,
                                              &AllocateInfo,
                                              VK_NULL_HANDLE,
                                              &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_memory: " << Status);
      return {};
    }

    Result.Size_ = Size;
    return Result;
  }

  vk_memory vk_memory::CreateHostMemory(const vk_device& Device,
                                        const vk_physical_device& PhysicalDevice,
                                        const VkDeviceSize Size)
  {
    LogInfo("Creating host memory " << Size << "...");

    VkMemoryAllocateInfo AllocateInfo = GetMemoryAllocateInfo();
    AllocateInfo.allocationSize = Size;
    AllocateInfo.memoryTypeIndex = PhysicalDevice.GetMemoryTypeIndex(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                                                                     VK_MEMORY_PROPERTY_HOST_COHERENT_BIT |
                                                                     VK_MEMORY_PROPERTY_HOST_CACHED_BIT);

    vk_memory Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkAllocateMemory(Result.Device_,
                                              &AllocateInfo,
                                              VK_NULL_HANDLE,
                                              &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_memory: " << Status);
      return {};
    }

    Result.Size_ = Size;
    return Result;
  }

  vk_memory::vk_memory(vk_memory&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
    std::swap(Size_, Other.Size_);
    std::swap(Data_, Other.Data_);
  }

  vk_memory& vk_memory::operator=(vk_memory&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    std::swap(Size_, Rhs.Size_);
    std::swap(Data_, Rhs.Data_);
    return *this;
  }

  vk_memory::~vk_memory()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_memory...");
      vkFreeMemory(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkDeviceMemory vk_memory::GetHandle() const
  {
    return Handle_;
  }

  vk_memory::stack_view vk_memory::GetStackView(const VkDeviceSize Offset)
  {
    Map();

    Assert(Size_ > Offset);

    stack_view Result;
    Result.TotalSize_ = Size_ - Offset;
    Result.Data_ = Data_ + Offset;
    return Result;
  }

  void vk_memory::Map()
  {
    if (Data_) return;

    vkMapMemory(Device_, Handle_, 0, VK_WHOLE_SIZE, 0, (void**) &Data_);
  }

}  // myran
