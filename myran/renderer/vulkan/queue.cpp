#include "queue.h"
#include "fence.h"
#include "semaphore.h"
#include "command_buffer.h"
#include "swap_chain.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_queue::vk_queue(const VkQueue& Handle, const s32 FamilyIndex)
    : Handle_(Handle),
      FamilyIndex_(FamilyIndex)
  {}

  bool vk_queue::operator==(const vk_queue& Rhs) const
  {
    return Handle_ == Rhs.Handle_ && FamilyIndex_ == Rhs.FamilyIndex_;
  }

  bool vk_queue::operator!=(const vk_queue& Rhs) const
  {
    return !(*this == Rhs);
  }

  vk_status vk_queue::Submit(const vk_command_buffer& CommandBuffer)
  {
    const auto CommandBufferHandle = CommandBuffer.GetHandle();

    VkSubmitInfo SubmitInfo = GetSubmitInfo();
    SubmitInfo.commandBufferCount = 1;
    SubmitInfo.pCommandBuffers = &CommandBufferHandle;

    const vk_status Status = vkQueueSubmit(Handle_,
                                           1,
                                           &SubmitInfo,
                                           VK_NULL_HANDLE);

    if (Status.IsError())
      LogError("Failed to submit to vk_queue: " << Status);

    return Status;
  }

  vk_status vk_queue::Submit(const vk_command_buffer& CommandBuffer,
                             const VkPipelineStageFlags WaitStageFlags,
                             const vk_semaphore& WaitSemaphore,
                             const vk_semaphore& SignalSemaphore,
                             const vk_fence& Fence)
  {
    VkSemaphore WaitSemaphoreHandle = WaitSemaphore.GetHandle();
    VkSemaphore SignalSemaphoreHandle = SignalSemaphore.GetHandle();
    VkCommandBuffer CommandBufferHandle = CommandBuffer.GetHandle();

    VkSubmitInfo SubmitInfo = GetSubmitInfo();
    SubmitInfo.waitSemaphoreCount = 1;
    SubmitInfo.pWaitSemaphores = &WaitSemaphoreHandle;
    SubmitInfo.pWaitDstStageMask = &WaitStageFlags;
    SubmitInfo.commandBufferCount = 1;
    SubmitInfo.pCommandBuffers = &CommandBufferHandle;
    SubmitInfo.signalSemaphoreCount = 1;
    SubmitInfo.pSignalSemaphores = &SignalSemaphoreHandle;

    vk_status Status;
    Status = vkQueueSubmit(Handle_,
                           1,
                           &SubmitInfo,
                           Fence.GetHandle());

    if (Status.IsError())
      LogError("Failed to submit to vk_queue: " << Status);

    return Status;
  }

  vk_status vk_queue::Present(const vk_swap_chain& SwapChain,
                              const vk_semaphore& WaitSemaphore)
  {
    VkSemaphore SemaphoreHandle = WaitSemaphore.GetHandle();
    VkSwapchainKHR SwapChainHandle = SwapChain.GetHandle();
    u32 ImageIndex = SwapChain.GetCurrentImageIndex();

    VkPresentInfoKHR PresentInfo = GetPresentInfoKHR();
    PresentInfo.waitSemaphoreCount = 1;
    PresentInfo.pWaitSemaphores = &SemaphoreHandle;
    PresentInfo.swapchainCount = 1;
    PresentInfo.pSwapchains = &SwapChainHandle;
    PresentInfo.pImageIndices = &ImageIndex;

    vk_status Status;
    Status = vkQueuePresentKHR(Handle_, &PresentInfo);

    if (Status.IsError())
      LogError("Failed to present in vk_queue: " << Status);

    return Status;

  }

  vk_status vk_queue::Wait()
  {
    return vkQueueWaitIdle(Handle_);
  }

  s32 vk_queue::GetFamilyIndex() const
  {
    return FamilyIndex_;
  }

  VkQueue vk_queue::GetHandle() const
  {
    return Handle_;
  }

}  // myran
