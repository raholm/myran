#pragma once

#include "device.h"
#include "image.h"
#include "status.h"

namespace myran {

  class vk_command_buffer
  {
  public:
    vk_command_buffer(const VkCommandBuffer Handle);

    vk_command_buffer(vk_command_buffer&& Other);
    vk_command_buffer& operator=(vk_command_buffer&& Rhs);

    ~vk_command_buffer() = default;

    void PipelineBarrier(const VkPipelineStageFlags SourceStageMask,
                         const VkPipelineStageFlags DestinationStageMask,
                         const VkImageMemoryBarrier& Barrier);

    void ClearColorImage(const vk_image& Image,
                         const VkImageLayout ImageLayout,
                         const VkClearColorValue& Color,
                         const VkImageSubresourceRange&  ImageSubresourceRange);

    vk_status Begin();
    vk_status End();

    VkCommandBuffer GetHandle() const;

  private:
    VkCommandBuffer Handle_ { VK_NULL_HANDLE };

  };

}  // myran
