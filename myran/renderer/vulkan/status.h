#pragma once

#include <iostream>
#include <string>

#include "vulkan_headers.h"

#include "../../types.h"

namespace myran {

  class vk_status final
  {
  public:
    vk_status();
    vk_status(const VkResult StatusCode);

    ~vk_status() = default;

    vk_status& operator=(const VkResult StatusCode);

    bool operator==(const VkResult StatusCode);
    bool operator!=(const VkResult StatusCode);

    bool IsError() const;
    bool IsOk() const;

    std::string ToString() const;

  private:
    VkResult StatusCode_;

  };

  inline std::ostream& operator<<(std::ostream& Out, const vk_status Status)
  {
    Out << Status.ToString();
    return Out;
  }

}  // myran
