#pragma once

#include <string>
#include <vector>

#include "vulkan_headers.h"

#include "../../window.h"

namespace myran {

  class vk_instance;
  class vk_device;
  class vk_physical_device;

  class vk_surface
  {
  public:
    vk_surface() = default;
    vk_surface(vk_instance& Instance,
               const window& Window);

    vk_surface(vk_surface&& Other);
    vk_surface& operator=(vk_surface&& Rhs);

    ~vk_surface();

    VkSurfaceKHR GetHandle() const;
    const std::vector<std::string>& GetExtensionNames() const;

  private:
    VkSurfaceKHR Handle_ { VK_NULL_HANDLE };
    VkInstance Instance_ { VK_NULL_HANDLE };

  };


  class vk_surface_capabilities
  {
  public:
    vk_surface_capabilities(const vk_surface& Surface,
                            const vk_physical_device& PhysicalDevice);

    ~vk_surface_capabilities() = default;

    bool SupportsImageCount(const u32 Count) const;
    bool SupportsImageExtent(const VkExtent2D Extent) const;
    bool SupportsImageUsage(const VkImageUsageFlags Flags) const;
    bool SupportsImageArrayLayers(const u32 ArrayLayers) const;
    bool SupportsTransform(const VkSurfaceTransformFlagsKHR Transform) const;
    bool SupportsCompositeAlpha(const VkCompositeAlphaFlagsKHR CompositeAlpha) const;
    bool SupportsPresentMode(const VkPresentModeKHR PresentMode) const;

    VkSurfaceFormatKHR GetPreferredSurfaceFormat() const;
    VkFormat GetPreferredImageFormat() const;
    VkSurfaceTransformFlagBitsKHR GetPreferredPreTransform() const;
    VkColorSpaceKHR GetPreferredImageColorSpace() const;
    VkPresentModeKHR GetPreferredPresentMode() const;
    VkExtent2D GetPreferredImageExtent(const u32 PreferredWidth,
                                       const u32 PreferredHeight) const;
    VkImageUsageFlags GetPreferredImageUsage() const;

    const VkSurfaceCapabilitiesKHR& GetCapabilities() const;
    const std::vector<VkSurfaceFormatKHR>& GetFormats() const;
    const std::vector<VkPresentModeKHR>& GetPresentModes() const;

  private:
    VkSurfaceKHR Surface_;
    VkPhysicalDevice PhysicalDevice_;

    VkSurfaceCapabilitiesKHR Capabilities_;
    std::vector<VkSurfaceFormatKHR> Formats_;
    std::vector<VkPresentModeKHR> PresentModes_;

    VkSurfaceCapabilitiesKHR Capabilities() const;
    std::vector<VkSurfaceFormatKHR> Formats() const;
    std::vector<VkPresentModeKHR> PresentModes() const;

  };


}  // myran
