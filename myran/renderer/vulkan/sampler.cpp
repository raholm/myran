#include "sampler.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_sampler vk_sampler::CreateGameplay(const vk_device& Device)
  {
    VkSamplerCreateInfo CreateInfo = GetSamplerCreateInfo();
    CreateInfo.magFilter = VK_FILTER_NEAREST;
    CreateInfo.minFilter = VK_FILTER_NEAREST;
    CreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    CreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    CreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    CreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
    CreateInfo.mipLodBias = 0.0f;
    CreateInfo.maxAnisotropy = 1.0f;
    CreateInfo.minLod = 0.0f;
    CreateInfo.maxLod = 1.0f;
    CreateInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;

    vk_sampler Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateSampler(Result.Device_,
                             &CreateInfo,
                             VK_NULL_HANDLE,
                             &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_sampler: " << Status);
      return {};
    }

    return Result;
  }

  vk_sampler::vk_sampler(vk_sampler&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_sampler& vk_sampler::operator=(vk_sampler&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_sampler::~vk_sampler()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_sampler...");
      vkDestroySampler(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkSampler vk_sampler::GetHandle() const
  {
    return Handle_;
  }

}  // myran
