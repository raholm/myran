#pragma once

#include "device.h"
#include "status.h"
#include "memory.h"

namespace myran {

  class vk_descriptor_pool final
  {
  public:
    static vk_descriptor_pool Create(const vk_device& Device);

  public:
    vk_descriptor_pool() = default;

    vk_descriptor_pool(vk_descriptor_pool&& Other);
    vk_descriptor_pool& operator=(vk_descriptor_pool&& Rhs);

    ~vk_descriptor_pool();

    VkDescriptorPool GetHandle() const;

  private:
    VkDescriptorPool Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
