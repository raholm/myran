#pragma once

#include "vulkan_headers.h"
#include "status.h"

namespace myran {

  class vk_render_pass;
  class vk_image_view;
  class vk_device;

  class vk_framebuffer
  {
  public:
    static vk_framebuffer CreateGlobalObserver(const vk_device& Device,
                                               const vk_render_pass& RenderPass,
                                               const vk_image_view& ColorAttachment,
                                               const vk_image_view& DepthAttachment,
                                               const VkExtent2D Extent);
    static vk_framebuffer CreateGameplay(const vk_device& Device,
                                         const vk_render_pass& RenderPass,
                                         const vk_image_view& OutputColorAttachment,
                                         const vk_image_view& DepthAttachment,
                                         const vk_image_view& ColorAttachment,
                                         const vk_image_view& WorldPositionAttachment,
                                         const vk_image_view& NormalAttachment,
                                         const VkExtent2D Extent);

  public:
    vk_framebuffer() = default;
    vk_framebuffer(const vk_render_pass& RenderPass,
                   const vk_image_view& Attachment,
                   const VkExtent2D Extent,
                   const vk_device& Device);

    vk_framebuffer(vk_framebuffer&& Other);
    vk_framebuffer& operator=(vk_framebuffer&& Rhs);

    ~vk_framebuffer();

    VkFramebuffer GetHandle() const;

  private:
    VkFramebuffer Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
