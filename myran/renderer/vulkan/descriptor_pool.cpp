#include "descriptor_pool.h"
#include "getters.h"

#include <myran/debug.h>

namespace myran {

  vk_descriptor_pool vk_descriptor_pool::Create(const vk_device& Device)
  {
    LogInfo("Creating descriptor pool...");

    const u32 count = 100;

    const std::vector<VkDescriptorPoolSize> PoolSizes = {
      {
        VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        count,
      },
      {
        VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        count,
      },
      {
        VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
        count,
      },
      {
        VK_DESCRIPTOR_TYPE_SAMPLER,
        count,
      },
      {
        VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
        count,
      },
    };

    VkDescriptorPoolCreateInfo CreateInfo = GetDescriptorPoolCreateInfo();
    CreateInfo.maxSets = count;
    CreateInfo.poolSizeCount = PoolSizes.size();
    CreateInfo.pPoolSizes = PoolSizes.data();

    vk_descriptor_pool Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateDescriptorPool(Result.Device_,
                                                    &CreateInfo,
                                                    VK_NULL_HANDLE,
                                                    &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk descriptor pool: " << Status);
      return Result;
    }

    return Result;
  }

  vk_descriptor_pool::vk_descriptor_pool(vk_descriptor_pool&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_descriptor_pool& vk_descriptor_pool::operator=(vk_descriptor_pool&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_descriptor_pool::~vk_descriptor_pool()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk descriptor pool...");
      vkDestroyDescriptorPool(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkDescriptorPool vk_descriptor_pool::GetHandle() const
  {
    return Handle_;
  }

}  // myran
