#include "instance.h"
#include "getters.h"
#include "status.h"

#include <utility>

#include "../../debug.h"

namespace myran {

  vk_instance::vk_instance(const vk_application_info& ApplicationInfo,
                           const std::vector<std::string>& ExtensionNames,
                           const std::vector<std::string>& ValidationLayerNames)
    : ApplicationInfo_(ApplicationInfo),
      ExtensionNames_(ExtensionNames),
      ValidationLayerNames_(ValidationLayerNames)
  {
    LogInfo("Creating vk_instance...");

    VkApplicationInfo AppInfo = myran::GetApplicationInfo();
    AppInfo.pApplicationName = ApplicationInfo_.Name.c_str();
    AppInfo.applicationVersion = ApplicationInfo_.Version.GetPacked();
    AppInfo.pEngineName = ApplicationInfo_.EngineName.c_str();
    AppInfo.engineVersion = ApplicationInfo_.EngineVersion.GetPacked();
    AppInfo.apiVersion = ApplicationInfo_.ApiVersion.GetPacked();

    VkInstanceCreateInfo CreateInfo = GetInstanceCreateInfo();
    CreateInfo.pApplicationInfo = &AppInfo;

    std::vector<const char*> ExtensionNamesCStr(ExtensionNames_.size());
    for (u32 Index = 0; Index < ExtensionNames_.size(); ++Index)
      ExtensionNamesCStr[Index] = ExtensionNames_[Index].c_str();

    std::vector<const char*> ValidationLayerNamesCStr(ValidationLayerNames_.size());
    for (u32 Index = 0; Index < ValidationLayerNames_.size(); ++Index)
      ValidationLayerNamesCStr[Index] = ValidationLayerNames_[Index].c_str();

    if (ExtensionNamesCStr.size() > 0)
    {
      CreateInfo.enabledExtensionCount = ExtensionNamesCStr.size();
      CreateInfo.ppEnabledExtensionNames = ExtensionNamesCStr.data();
    }

    if (ValidationLayerNamesCStr.size() > 0)
    {
      CreateInfo.enabledLayerCount = ValidationLayerNamesCStr.size();
      CreateInfo.ppEnabledLayerNames = ValidationLayerNamesCStr.data();
    }

    vk_status Status;
    Status = vkCreateInstance(&CreateInfo, VK_NULL_HANDLE, &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to create vk_instance: " << Status);
      return;
    }

    u32 PhysicalDeviceCount = 0;
    Status = vkEnumeratePhysicalDevices(Handle_, &PhysicalDeviceCount, VK_NULL_HANDLE);

    if (Status.IsError())
    {
      LogError("Failed to enumerate physical devices: " << Status);
      return;
    }

    std::vector<VkPhysicalDevice> PhysicalDevices(PhysicalDeviceCount);
    Status = vkEnumeratePhysicalDevices(Handle_, &PhysicalDeviceCount, PhysicalDevices.data());

    if (Status.IsError())
    {
      LogError("Failed to enumerate physical devices: " << Status);
      return;
    }

    PhysicalDevices_.reserve(PhysicalDeviceCount);

    for (const VkPhysicalDevice& PhysicalDevice : PhysicalDevices)
      PhysicalDevices_.push_back(vk_physical_device(PhysicalDevice));
  }

  vk_instance::vk_instance(vk_instance&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(ApplicationInfo_, Other.ApplicationInfo_);
    std::swap(ExtensionNames_, Other.ExtensionNames_);
    std::swap(ValidationLayerNames_, Other.ValidationLayerNames_);
    std::swap(PhysicalDevices_, Other.PhysicalDevices_);
  }

  vk_instance& vk_instance::operator=(vk_instance&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(ApplicationInfo_, Rhs.ApplicationInfo_);
    std::swap(ExtensionNames_, Rhs.ExtensionNames_);
    std::swap(ValidationLayerNames_, Rhs.ValidationLayerNames_);
    std::swap(PhysicalDevices_, Rhs.PhysicalDevices_);
    return *this;
  }

  vk_instance::~vk_instance()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_instance...");
      vkDestroyInstance(Handle_, VK_NULL_HANDLE);
    }
  }

  VkInstance vk_instance::GetHandle() const
  {
    return Handle_;
  }

  const vk_application_info& vk_instance::GetApplicationInfo() const
  {
    return ApplicationInfo_;
  }

  const std::vector<std::string>& vk_instance::GetExtensionNames() const
  {
    return ExtensionNames_;
  }

  const std::vector<std::string>& vk_instance::GetValidationLayerNames() const
  {
    return ValidationLayerNames_;
  }

  const std::vector<vk_physical_device>& vk_instance::GetPhysicalDevices() const
  {
    return PhysicalDevices_;
  }

}  // myran
