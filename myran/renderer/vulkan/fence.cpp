#include "fence.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_fence::vk_fence(vk_device& Device)
    : Device_(Device.GetHandle())
  {
    VkFenceCreateInfo CreateInfo = GetFenceCreateInfo();
    CreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    vk_status Status;
    Status = vkCreateFence(Device_,
                           &CreateInfo,
                           VK_NULL_HANDLE,
                           &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_fence: " << Status);
      return;
    }
  }

  vk_fence::vk_fence(vk_fence&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_fence& vk_fence::operator=(vk_fence&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_fence::~vk_fence()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_fence...");
      vkDestroyFence(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  vk_status vk_fence::Wait(u64 Timeout)
  {
    vk_status Status;
    Status = vkWaitForFences(Device_,
                             1,
                             &Handle_,
                             VK_TRUE,
                             Timeout);

    if (Status.IsError())
      LogError("Failed to wait for vk_fence: " << Status);

    return Status;
  }

  vk_status vk_fence::Reset()
  {
    vk_status Status;
    Status = vkResetFences(Device_,
                           1,
                           &Handle_);

    if (Status.IsError())
      LogError("Failed to reset vk_fence: " << Status);

    return Status;
  }

  vk_status vk_fence::WaitAndReset(u64 Timeout)
  {
    vk_status Status;
    Status = Wait(Timeout);

    if (Status.IsError())
      return Status;

    return Reset();
  }

  bool vk_fence::IsSignlad() const
  {
    vk_status Status;
    Status = vkGetFenceStatus(Device_, Handle_);
    return Status.IsOk();
  }

  VkFence vk_fence::GetHandle() const
  {
    return Handle_;
  }

}  // myran
