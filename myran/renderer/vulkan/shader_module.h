#pragma once

#include <string>
#include <filesystem>

#include "device.h"
#include "status.h"

namespace myran {

  class vk_shader_module
  {
  public:
    enum class stage
    {
      Vertex,
      Fragment,
    };

  public:
    static vk_shader_module CreateFromName(const vk_device& Device,
                                           const stage Stage,
                                           const std::string& Name);

  private:
    static std::vector<byte> GetSpirv(const std::filesystem::path& FilePath);

  public:
    vk_shader_module() = default;

    vk_shader_module(vk_shader_module&& Other);
    vk_shader_module& operator=(vk_shader_module&& Rhs);

    ~vk_shader_module();

    VkShaderModule GetHandle() const;

  private:
    VkShaderModule Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

    stage Stage_;
    std::string FilePath_;
    std::vector<byte> Spirv_;

  };

}  // myran
