#pragma once

#include <vector>
#include <string>

#include "vulkan_headers.h"

#include "../../types.h"

namespace myran {

  // VkApplicationInfo
  VkApplicationInfo GetApplicationInfo();

  // VkAttachmentDescription
  VkAttachmentDescription GetAttachmentDescription();

  // VkAttachmentReference
  VkAttachmentReference GetAttachmentReference();

  // VkBufferCopy
  VkBufferCopy GetBufferCopy();

  // VkBufferImageCopy
  VkBufferImageCopy GetBufferImageCopy();

  // VkBufferCreateInfo
  VkBufferCreateInfo GetBufferCreateInfo();

  // VkClearColorValue
  VkClearColorValue GetClearColorValue(const f32 Red = 0.0f,
                                       const f32 Green = 0.0f,
                                       const f32 Blue = 0.0f,
                                       const f32 Alpha = 1.0f);

  // VkClearValue
  VkClearValue GetClearValue(const f32 Red = 0.0f,
                             const f32 Green = 0.0f,
                             const f32 Blue = 0.0f,
                             const f32 Alpha = 1.0f);

  // VkCommandBufferAllocateInfo
  VkCommandBufferAllocateInfo GetCommandBufferAllocateInfo();

  // VkCommandBufferBeginInfo
  VkCommandBufferBeginInfo GetCommandBufferBeginInfo();

  // VkCommandPoolCreateInfo
  VkCommandPoolCreateInfo GetCommandPoolCreateInfo();

  // VkDescriptorBufferInfo
  VkDescriptorBufferInfo GetDescriptorBufferInfo();

  // GetDescriptorImageInfo
  VkDescriptorImageInfo GetDescriptorImageInfo();

  // VkDescriptorPoolSize
  VkDescriptorPoolSize GetDescriptorPoolSize();

  // VkDescriptorPoolCreateInfo
  VkDescriptorPoolCreateInfo GetDescriptorPoolCreateInfo();

  // VkDescriptorSetAllocateInfo
  VkDescriptorSetAllocateInfo GetDescriptorSetAllocateInfo();

  // VkDescriptorSetLayoutBinding
  VkDescriptorSetLayoutBinding GetDescriptorSetLayoutBinding();

  // VkDescriptorSetLayoutCreateInfo
  VkDescriptorSetLayoutCreateInfo GetDescriptorSetLayoutCreateInfo();

  // VkDeviceQueueCreateInfo
  VkDeviceQueueCreateInfo GetDeviceQueueCreateInfo();

  // VkDeviceCreateInfo
  VkDeviceCreateInfo  GetDeviceCreateInfo();

  // VkFenceCreateInfo
  VkFenceCreateInfo GetFenceCreateInfo();

  // VkFramebufferCreateInfo
  VkFramebufferCreateInfo GetFramebufferCreateInfo();

  // VkGraphicsPipelineCreateInfo
  VkGraphicsPipelineCreateInfo GetGraphicsPipelineCreateInfo();

  // VkImageCreateInfo
  VkImageCreateInfo GetImageCreateInfo();

  // VkImageMemoryBarrier
  VkImageMemoryBarrier GetImageMemoryBarrier();

  // VkImageSubresourceRange
  VkImageSubresourceRange GetImageSubresourceRange();

  // VkImageViewCreateInfo
  VkImageViewCreateInfo GetImageViewCreateInfo();

  // VkInstanceCreateInfo
  VkInstanceCreateInfo GetInstanceCreateInfo();

  // VkMappedMemoryRange
  VkMappedMemoryRange GetMappedMemoryRange();

  // VkMemoryAllocateInfo
  VkMemoryAllocateInfo GetMemoryAllocateInfo();

  // VkPipelineCacheCreateInfo
  VkPipelineCacheCreateInfo GetPipelineCacheCreateInfo();

  // VkPipelineLayoutCreateInfo
  VkPipelineLayoutCreateInfo GetPipelineLayoutCreateInfo();

  // VkPipelineColorBlendAttachmentState
  VkPipelineColorBlendAttachmentState GetPipelineColorBlendAttachmentState();

  // VkPipelineColorBlendStateCreateInfo
  VkPipelineColorBlendStateCreateInfo GetPipelineColorBlendStateCreateInfo();

  // VkPipelineDynamicStateCreateInfo
  VkPipelineDynamicStateCreateInfo GetPipelineDynamicStateCreateInfo();

  // VkPipelineInputAssemblyStateCreateInfo
  VkPipelineInputAssemblyStateCreateInfo GetPipelineInputAssemblyStateCreateInfo();

  // VkPipelineLayoutCreateInfo
  VkPipelineLayoutCreateInfo GetVkPipelineLayoutCreateInfo();

  // VkPipelineMultisampleStateCreateInfo
  VkPipelineMultisampleStateCreateInfo GetPipelineMultisampleStateCreateInfo();

  // VkPipelineRasterizationStateCreateInfo
  VkPipelineRasterizationStateCreateInfo GetPipelineRasterizationStateCreateInfo();

  // VkPipelineShaderStageCreateInfo
  VkPipelineShaderStageCreateInfo GetPipelineShaderStageCreateInfo();

  // VkPipelineVertexInputStateCreateInfo
  VkPipelineVertexInputStateCreateInfo GetPipelineVertexInputStateCreateInfo();

  // VkPipelineViewportStateCreateInfo
  VkPipelineViewportStateCreateInfo GetPipelineViewportStateCreateInfo();

  // VkPresentInfoKHR
  VkPresentInfoKHR GetPresentInfoKHR();

  // VkRect2D
  VkRect2D GetRect2D();

  // VkRenderPassBeginInfo
  VkRenderPassBeginInfo GetRenderPassBeginInfo();

  // VkRenderPassCreateInfo
  VkRenderPassCreateInfo GetRenderPassCreateInfo();

  // VkSamplerCreateInfo
  VkSamplerCreateInfo GetSamplerCreateInfo();

  // VkSemaphoreCreateInfo
  VkSemaphoreCreateInfo GetSemaphoreCreateInfo();

  // VkShaderModuleCreateInfo
  VkShaderModuleCreateInfo GetShaderModuleCreateInfo();

  // VkSubmitInfo
  VkSubmitInfo GetSubmitInfo();

  // VkSubpassDescription
  VkSubpassDescription GetSubpassDescription();

  // VkSubpassDependency
  VkSubpassDependency GetSubpassDependency();

  // VkSwapchainCreateInfoKHR
  VkSwapchainCreateInfoKHR GetSwapchainCreateInfoKHR();

  // VkVertexInputAttributeDescription
  VkVertexInputAttributeDescription GetVertexInputAttributeDescription();

  // VkVertexInputBindingDescription
  VkVertexInputBindingDescription GetVertexInputBindingDescription();

  // VkViewport
  VkViewport GetViewport();

  // VkWriteDescriptorSet
  VkWriteDescriptorSet GetWriteDescriptorSet();

  VkBufferMemoryBarrier GetBufferMemoryBarrier();

  VkPipelineDepthStencilStateCreateInfo GetPipelineDepthStencilStateCreateInfo();

}  // myran
