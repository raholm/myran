#include "physical_device.h"
#include "status.h"

#include "../../debug.h"

namespace myran {

  vk_physical_device::vk_physical_device(const VkPhysicalDevice& Handle)
    : Handle_(Handle)
  {
    vkGetPhysicalDeviceProperties(Handle_, &Properties_);
    vkGetPhysicalDeviceFeatures(Handle_, &Features_);
    vkGetPhysicalDeviceMemoryProperties(Handle_, &MemoryProperties_);

    Name_ = Properties_.deviceName;

    u32 QueueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(Handle_,
                                             &QueueFamilyCount,
                                             VK_NULL_HANDLE);

    if (QueueFamilyCount > 0)
    {
      QueueFamilyProperties_.resize(QueueFamilyCount);
      vkGetPhysicalDeviceQueueFamilyProperties(Handle_,
                                               &QueueFamilyCount,
                                               QueueFamilyProperties_.data());
    }

    u32 ExtensionCount;
    vk_status Status;
    Status = vkEnumerateDeviceExtensionProperties(Handle_,
                                                  VK_NULL_HANDLE,
                                                  &ExtensionCount,
                                                  VK_NULL_HANDLE);

    if (Status.IsError())
      LogError("Failed to enumerate device extension properties: " << Status);

    if (Status.IsOk() && ExtensionCount > 0) {
      ExtensionProperties_.resize(ExtensionCount);
      Status = vkEnumerateDeviceExtensionProperties(Handle_,
                                                    VK_NULL_HANDLE,
                                                    &ExtensionCount,
                                                    ExtensionProperties_.data());

      if (Status.IsError())
        LogError("Failed to enumerate device extension properties: " << Status);
    }

    FindAndAssignQueueFamilyIndex();
  }

  VkPhysicalDevice vk_physical_device::GetHandle() const
  {
    return Handle_;
  }

  const std::string& vk_physical_device::GetName() const
  {
    return Name_;
  }

  const VkPhysicalDeviceFeatures& vk_physical_device::GetFeatures() const
  {
    return Features_;
  }

  const VkPhysicalDeviceProperties& vk_physical_device::GetProperties() const
  {
    return Properties_;
  }

  const VkPhysicalDeviceMemoryProperties& vk_physical_device::GetMemoryProperties() const
  {
    return MemoryProperties_;
  }

  const std::vector<VkExtensionProperties>& vk_physical_device::GetExtensionProperties() const
  {
    return ExtensionProperties_;
  }

  bool vk_physical_device::HasComputeQueue() const
  {
    return ComputeQueueFamilyIndex_ != -1;
  }

  bool vk_physical_device::HasGraphicsQueue() const
  {
    return GraphicsQueueFamilyIndex_ != -1;
  }

  bool vk_physical_device::HasPresentQueue() const
  {
    return PresentQueueFamilyIndex_ != -1;
  }

  bool vk_physical_device::HasTransferQueue() const
  {
    return TransferQueueFamilyIndex_ != -1;
  }

  s32 vk_physical_device::GetComputeQueueFamilyIndex() const
  {
    return ComputeQueueFamilyIndex_;
  }

  s32 vk_physical_device::GetGraphicsQueueFamilyIndex() const
  {
    return GraphicsQueueFamilyIndex_;
  }

  s32 vk_physical_device::GetPresentQueueFamilyIndex() const
  {
    return PresentQueueFamilyIndex_;
  }

  s32 vk_physical_device::GetTransferQueueFamilyIndex() const
  {
    return TransferQueueFamilyIndex_;
  }

  const std::unordered_set<s32>& vk_physical_device::GetQueueFamilyIndices() const
  {

    return QueueFamilyIndices_;
  }

  u32 vk_physical_device::GetMemoryTypeIndex(const VkMemoryPropertyFlags properties) const
  {
    for (u32 index = 0; index < MemoryProperties_.memoryTypeCount; ++index)
    {
      const auto& memory_type = MemoryProperties_.memoryTypes [index];

      if ((memory_type.propertyFlags & properties) == properties)
        return index;
    }

    LogError("Failed to find memory type index!");
    return 0;
  }

  bool vk_physical_device::SupportsSurface(vk_surface& Surface) const
  {
    for (u32 Index = 0; Index < QueueFamilyProperties_.size(); ++Index)
    {
      if (QueueFamilyProperties_[Index].queueCount == 0)
        continue;

      VkBool32 PresentSupport = VK_FALSE;
      vk_status Status;
      Status = vkGetPhysicalDeviceSurfaceSupportKHR(Handle_,
                                                    Index,
                                                    Surface.GetHandle(),
                                                    &PresentSupport);

      if (Status.IsError())
      {
        LogError("Failed to get physical device surface support: " << Status);
        continue;
      }

      if (PresentSupport)
        return true;
    }

    return false;
  }

  void vk_physical_device::UpdatePresentQueue(vk_surface& Surface)
  {
    for (u32 Index = 0; Index < QueueFamilyProperties_.size(); ++Index)
    {
      if (QueueFamilyProperties_[Index].queueCount == 0)
        continue;

      VkBool32 PresentSupport = VK_FALSE;
      vk_status Status;
      Status = vkGetPhysicalDeviceSurfaceSupportKHR(Handle_,
                                                    Index,
                                                    Surface.GetHandle(),
                                                    &PresentSupport);

      if (Status.IsError())
      {
        LogError("Failed to get physical device surface support: " << Status);
        continue;
      }

      if (PresentSupport)
      {
        PresentQueueFamilyIndex_ = Index;
        break;
      }
    }

    UpdateQueueFamilyIndices();
  }

  void vk_physical_device::FindAndAssignQueueFamilyIndex()
  {
    for (u32 Index = 0; Index < QueueFamilyProperties_.size(); ++Index)
    {
      if (QueueFamilyProperties_[Index].queueCount == 0)
        continue;

      auto QueueFlags = QueueFamilyProperties_[Index].queueFlags;

      if (!HasComputeQueue() && QueueFlags & VK_QUEUE_COMPUTE_BIT)
        ComputeQueueFamilyIndex_ = Index;

      if (!HasGraphicsQueue() && QueueFlags & VK_QUEUE_GRAPHICS_BIT)
        GraphicsQueueFamilyIndex_ = Index;

      if (!HasTransferQueue() && QueueFlags & VK_QUEUE_TRANSFER_BIT)
        TransferQueueFamilyIndex_ = Index;
    }

    UpdateQueueFamilyIndices();
  }

  void vk_physical_device::UpdateQueueFamilyIndices()
  {
    if (HasComputeQueue())
      QueueFamilyIndices_.insert(ComputeQueueFamilyIndex_);

    if (HasGraphicsQueue())
      QueueFamilyIndices_.insert(GraphicsQueueFamilyIndex_);

    if (HasPresentQueue())
      QueueFamilyIndices_.insert(PresentQueueFamilyIndex_);

    if (HasTransferQueue())
      QueueFamilyIndices_.insert(TransferQueueFamilyIndex_);
  }

}  // myran
