#pragma once

#include "device.h"
#include "status.h"
#include "memory.h"

namespace myran {

  class vk_descriptor_set_layout final
  {
  public:
    static vk_descriptor_set_layout CreateGlobalObserver(const vk_device& Device);
    static vk_descriptor_set_layout CreateGameplay(const vk_device& Device);

  public:
    vk_descriptor_set_layout() = default;

    vk_descriptor_set_layout(vk_descriptor_set_layout&& Other);
    vk_descriptor_set_layout& operator=(vk_descriptor_set_layout&& Rhs);

    ~vk_descriptor_set_layout();

    VkDescriptorSetLayout GetHandle() const;

  private:
    VkDescriptorSetLayout Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
