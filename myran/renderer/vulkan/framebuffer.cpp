#include "framebuffer.h"
#include "render_pass.h"
#include "image_view.h"
#include "device.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_framebuffer vk_framebuffer::CreateGlobalObserver(const vk_device& Device,
                                                      const vk_render_pass& RenderPass,
                                                      const vk_image_view& ColorAttachment,
                                                      const vk_image_view& DepthAttachment,
                                                      const VkExtent2D Extent)
  {
    const std::array<VkImageView, 2> Attachments = {
      ColorAttachment.GetHandle(),
      DepthAttachment.GetHandle(),
    };

    VkFramebufferCreateInfo CreateInfo = GetFramebufferCreateInfo();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.attachmentCount = Attachments.size();
    CreateInfo.pAttachments = Attachments.data();
    CreateInfo.width = Extent.width;
    CreateInfo.height = Extent.height;
    CreateInfo.layers = 1;

    vk_framebuffer Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateFramebuffer(Result.Device_,
                                                 &CreateInfo,
                                                 VK_NULL_HANDLE,
                                                 &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_framebuffer: " << Status);
      return {};
    }

    return Result;
  }

  vk_framebuffer vk_framebuffer::CreateGameplay(const vk_device& Device,
                                                const vk_render_pass& RenderPass,
                                                const vk_image_view& OutputColorAttachment,
                                                const vk_image_view& DepthAttachment,
                                                const vk_image_view& ColorAttachment,
                                                const vk_image_view& WorldPositionAttachment,
                                                const vk_image_view& NormalAttachment,
                                                const VkExtent2D Extent)
  {
    const std::array<VkImageView, 5> Attachments = {
      OutputColorAttachment.GetHandle(),
      DepthAttachment.GetHandle(),
      ColorAttachment.GetHandle(),
      WorldPositionAttachment.GetHandle(),
      NormalAttachment.GetHandle(),
    };

    VkFramebufferCreateInfo CreateInfo = GetFramebufferCreateInfo();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.attachmentCount = Attachments.size();
    CreateInfo.pAttachments = Attachments.data();
    CreateInfo.width = Extent.width;
    CreateInfo.height = Extent.height;
    CreateInfo.layers = 1;

    vk_framebuffer Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateFramebuffer(Result.Device_,
                                                 &CreateInfo,
                                                 VK_NULL_HANDLE,
                                                 &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_framebuffer: " << Status);
      return {};
    }

    return Result;
  }

  vk_framebuffer::vk_framebuffer(const vk_render_pass& RenderPass,
                                 const vk_image_view& Attachment,
                                 const VkExtent2D Extent,
                                 const vk_device& Device)
    : Device_(Device.GetHandle())
  {
    VkImageView AttachmentHandle = Attachment.GetHandle();

    VkFramebufferCreateInfo CreateInfo = GetFramebufferCreateInfo();
    CreateInfo.renderPass = RenderPass.GetHandle();
    CreateInfo.attachmentCount = 1;
    CreateInfo.pAttachments = &AttachmentHandle;
    CreateInfo.width = Extent.width;
    CreateInfo.height = Extent.height;
    CreateInfo.layers = 1;

    vk_status Status;
    Status = vkCreateFramebuffer(Device_,
                                 &CreateInfo,
                                 VK_NULL_HANDLE,
                                 &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_framebuffer: " << Status);
      return;
    }
  }

  vk_framebuffer::vk_framebuffer(vk_framebuffer&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_framebuffer& vk_framebuffer::operator=(vk_framebuffer&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_framebuffer::~vk_framebuffer()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      // LogInternal("Destroying vk_framebuffer...");
      vkDestroyFramebuffer(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkFramebuffer vk_framebuffer::GetHandle() const
  {
    return Handle_;
  }

}  // myran
