#pragma once

#include <cstring>

#include <myran/debug.h>

#include "device.h"
#include "physical_device.h"
#include "status.h"

namespace myran {

  class vk_memory final
  {
  public:
    static vk_memory CreateDeviceMemory(const vk_device& Device,
                                        const vk_physical_device& PhysicalDevice,
                                        const VkDeviceSize Size);
    static vk_memory CreateHostMemory(const vk_device& Device,
                                      const vk_physical_device& PhysicalDevice,
                                      const VkDeviceSize Size);

  public:
    class stack_view
    {
    public:
      template<typename T>
      void Push(const T& Object) {
        const auto ObjectSize = sizeof(Object);

        const auto AvailableSize = TotalSize_ - PushedSize_;

        if (ObjectSize > AvailableSize)
        {
          Assert(false);
          return;
        }

        std::memcpy(Data_ + PushedSize_, &Object, ObjectSize);
        PushedSize_ += ObjectSize;
      }

      inline VkDeviceSize GetPushedSize() const {
        return PushedSize_;
      }

    private:
      VkDeviceSize PushedSize_ { 0 };
      VkDeviceSize TotalSize_ { 0 };
      byte* Data_ { nullptr };

      friend class vk_memory;

    };

  public:
    vk_memory() = default;

    vk_memory(vk_memory&& Other);
    vk_memory& operator=(vk_memory&& Rhs);

    ~vk_memory();

    VkDeviceMemory GetHandle() const;

    stack_view GetStackView(const VkDeviceSize Offset);

  private:
    void Map();

  private:
    VkDeviceMemory Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };
    VkDeviceSize Size_ { 0 };
    byte* Data_ { nullptr };

  };

}  // myran
