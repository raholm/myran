#pragma once

#include "device.h"
#include "status.h"
#include "memory.h"

namespace myran {

  class vk_buffer final
  {
  public:
    static vk_buffer CreateVertexBuffer(const vk_device& Device, const VkDeviceSize Size);
    static vk_buffer CreateUniformBuffer(const vk_device& Device, const VkDeviceSize Size);
    static vk_buffer CreateStageBuffer(const vk_device& Device, const VkDeviceSize Size);

  public:
    vk_buffer() = default;

    vk_buffer(vk_buffer&& Other);
    vk_buffer& operator=(vk_buffer&& Rhs);

    ~vk_buffer();

    VkBuffer GetHandle() const;
    VkDeviceSize GetMemoryOffset() const;
    VkDeviceSize GetRequiredSize() const;
    VkDeviceSize GetNextAlignedOffset(const VkDeviceSize Offset) const;

    void Bind(const vk_memory& Memory, const VkDeviceSize Offset);

  private:
    VkBuffer Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };
    VkDeviceSize MemoryOffset_ { 0 };
    VkMemoryRequirements MemoryRequirements_;

  };

}  // myran
