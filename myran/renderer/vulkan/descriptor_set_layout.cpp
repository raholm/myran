#include "descriptor_set_layout.h"
#include "getters.h"

#include <myran/debug.h>

namespace myran {

  vk_descriptor_set_layout vk_descriptor_set_layout::CreateGlobalObserver(const vk_device& Device)
  {
    LogInfo("Creating global observer descriptor set layout");

    const std::vector<VkDescriptorSetLayoutBinding> Bindings = {
      {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr,
      },
      {
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = nullptr,
      }
    };

    VkDescriptorSetLayoutCreateInfo CreateInfo = GetDescriptorSetLayoutCreateInfo();
    CreateInfo.bindingCount = Bindings.size();
    CreateInfo.pBindings = Bindings.data();

    vk_descriptor_set_layout Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateDescriptorSetLayout(Result.Device_,
                                                         &CreateInfo,
                                                         VK_NULL_HANDLE,
                                                         &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk descriptor set layout: " << Status);
      return Result;
    }

    return Result;
  }

  vk_descriptor_set_layout vk_descriptor_set_layout::CreateGameplay(const vk_device& Device)
  {
    LogInfo("Creating gameplay descriptor set layout");

    const std::vector<VkDescriptorSetLayoutBinding> Bindings = {
      {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        .pImmutableSamplers = nullptr,
      },
      {
        .binding = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = nullptr,
      },
      {
        .binding = 2,
        .descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = nullptr,
      },
      {
        .binding = 3,
        .descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = nullptr,
      },
      {
        .binding = 4,
        .descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
        .pImmutableSamplers = nullptr,
      },
    };

    VkDescriptorSetLayoutCreateInfo CreateInfo = GetDescriptorSetLayoutCreateInfo();
    CreateInfo.bindingCount = Bindings.size();
    CreateInfo.pBindings = Bindings.data();

    vk_descriptor_set_layout Result;
    Result.Device_ = Device.GetHandle();

    const vk_status Status = vkCreateDescriptorSetLayout(Result.Device_,
                                                         &CreateInfo,
                                                         VK_NULL_HANDLE,
                                                         &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk descriptor set layout: " << Status);
      return Result;
    }

    return Result;
  }

  vk_descriptor_set_layout::vk_descriptor_set_layout(vk_descriptor_set_layout&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_descriptor_set_layout& vk_descriptor_set_layout::operator=(vk_descriptor_set_layout&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_descriptor_set_layout::~vk_descriptor_set_layout()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk descriptor set layout...");
      vkDestroyDescriptorSetLayout(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkDescriptorSetLayout vk_descriptor_set_layout::GetHandle() const
  {
    return Handle_;
  }

}  // myran
