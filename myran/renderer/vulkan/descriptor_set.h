#pragma once

#include "device.h"
#include "status.h"
#include "memory.h"
#include "descriptor_pool.h"
#include "descriptor_set_layout.h"

namespace myran {

  class vk_descriptor_set final
  {
  public:
    static vk_descriptor_set Create(const vk_device& Device,
                                    const vk_descriptor_pool& Pool,
                                    const vk_descriptor_set_layout& Layout);

  public:
    vk_descriptor_set() = default;

    vk_descriptor_set(vk_descriptor_set&& Other);
    vk_descriptor_set& operator=(vk_descriptor_set&& Rhs);

    ~vk_descriptor_set();

    VkDescriptorSet GetHandle() const;

  private:
    VkDescriptorSet Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };
    VkDescriptorPool Pool_ { VK_NULL_HANDLE };

  };

}  // myran
