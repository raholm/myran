#include "command_buffer.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_command_buffer::vk_command_buffer(const VkCommandBuffer Handle)
    : Handle_(Handle)
  {}

  vk_command_buffer::vk_command_buffer(vk_command_buffer&& Other)
  {
    std::swap(Handle_, Other.Handle_);
  }

  vk_command_buffer& vk_command_buffer::operator=(vk_command_buffer&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    return *this;
  }

  void vk_command_buffer::PipelineBarrier(const VkPipelineStageFlags SourceStageMask,
                                          const VkPipelineStageFlags DestinationStageMask,
                                          const VkImageMemoryBarrier& Barrier)
  {
    vkCmdPipelineBarrier(Handle_,
                         SourceStageMask,
                         DestinationStageMask,
                         0,
                         0,
                         nullptr,
                         0,
                         nullptr,
                         1,
                         &Barrier);
  }

  void vk_command_buffer::ClearColorImage(const vk_image& Image,
                                          const VkImageLayout ImageLayout,
                                          const VkClearColorValue& Color,
                                          const VkImageSubresourceRange&  ImageSubresourceRange)
  {
    vkCmdClearColorImage(Handle_,
                         Image.GetHandle(),
                         ImageLayout,
                         &Color,
                         1,
                         &ImageSubresourceRange);
  }

  vk_status vk_command_buffer::Begin()
  {
    VkCommandBufferBeginInfo BeginInfo = GetCommandBufferBeginInfo();
    BeginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;

    vk_status Status;
    Status = vkBeginCommandBuffer(Handle_, &BeginInfo);

    if (Status.IsError())
      LogError("Failed to begin vk_command_buffer: " << Status);

    return Status;
  }

  vk_status vk_command_buffer::End()
  {
    vk_status Status;
    Status = vkEndCommandBuffer(Handle_);

    if (Status.IsError())
      LogError("Failed to end vk_command_buffer: " << Status);

    return Status;
  }

  VkCommandBuffer vk_command_buffer::GetHandle() const
  {
    return Handle_;
  }

}  // myran
