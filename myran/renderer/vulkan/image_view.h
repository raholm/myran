#pragma once

#include "device.h"
#include "image.h"

namespace myran {

  class vk_image_view final
  {
  public:
    static vk_image_view CreateDepthBuffer(const vk_device& Device,
                                           const vk_image& Image);
    static vk_image_view CreateColor(const vk_device& Device,
                                     const vk_image& Image);
    static vk_image_view CreateWorldPosition(const vk_device& Device,
                                             const vk_image& Image);
    static vk_image_view CreateNormal(const vk_device& Device,
                                      const vk_image& Image);
    static vk_image_view Create(const vk_device& Device,
                                const vk_image& Image,
                                const VkImageAspectFlags Aspect);

  public:
    vk_image_view() = default;
    vk_image_view(const VkDevice Device,
                  const VkImage Image,
                  const VkImageViewType Type,
                  const VkFormat Format,
                  const VkImageAspectFlags AspectMask);

    vk_image_view(vk_image_view&& Other);
    vk_image_view& operator=(vk_image_view&& Rhs);

    ~vk_image_view();

    VkImageView GetHandle() const;

  private:
    VkImageView Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
