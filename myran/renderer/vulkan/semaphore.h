#pragma once

#include "device.h"
#include "status.h"

namespace myran {

  class vk_semaphore final
  {
  public:
    vk_semaphore(const vk_device& Device);

    vk_semaphore(vk_semaphore&& Other);
    vk_semaphore& operator=(vk_semaphore&& Rhs);

    ~vk_semaphore();

    VkSemaphore GetHandle() const;

  private:
    VkSemaphore Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
