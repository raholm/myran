#pragma once

#include "vulkan_headers.h"
#include "status.h"

namespace myran {

  class vk_device;
  class vk_render_pass;
  class vk_swap_chain;
  class vk_pipeline_layout;

  class vk_graphics_pipeline
  {
  public:
    static vk_graphics_pipeline CreateGlobalObserver(const vk_device& Device,
                                                     const vk_pipeline_layout& Layout,
                                                     const vk_render_pass& RenderPass,
                                                     const VkExtent2D ImageSize);
    static vk_graphics_pipeline CreateGameplay_Offscreen(const vk_device& Device,
                                                         const vk_pipeline_layout& Layout,
                                                         const vk_render_pass& RenderPass,
                                                         const VkExtent2D ImageSize);
    static vk_graphics_pipeline CreateGameplay_Onscreen(const vk_device& Device,
                                                        const vk_pipeline_layout& Layout,
                                                        const vk_render_pass& RenderPass,
                                                        const VkExtent2D ImageSize);
    static vk_graphics_pipeline CreateGameplay_Minimap(const vk_device& Device,
                                                       const vk_pipeline_layout& Layout,
                                                       const vk_render_pass& RenderPass,
                                                       const VkExtent2D ImageSize,
                                                       const VkExtent2D MinimapSize);

  public:
    vk_graphics_pipeline() = default;

    vk_graphics_pipeline(vk_graphics_pipeline&& Other);
    vk_graphics_pipeline& operator=(vk_graphics_pipeline&& Rhs);

    ~vk_graphics_pipeline();

    VkPipeline GetHandle() const;

  private:
    VkPipeline Handle_ { VK_NULL_HANDLE };
    VkDevice Device_ { VK_NULL_HANDLE };

  };

}  // myran
