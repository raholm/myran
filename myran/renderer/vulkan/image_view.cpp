#include "image_view.h"
#include "getters.h"

#include "../../debug.h"

namespace myran {

  vk_image_view vk_image_view::CreateDepthBuffer(const vk_device& Device,
                                                 const vk_image& Image)
  {
    return Create(Device, Image, VK_IMAGE_ASPECT_DEPTH_BIT);
  }

  vk_image_view vk_image_view::CreateColor(const vk_device& Device,
                                           const vk_image& Image)
  {
    return Create(Device, Image, VK_IMAGE_ASPECT_COLOR_BIT);
  }

  vk_image_view vk_image_view::CreateWorldPosition(const vk_device& Device,
                                                   const vk_image& Image)
  {
    return Create(Device, Image, VK_IMAGE_ASPECT_COLOR_BIT);
  }

  vk_image_view vk_image_view::CreateNormal(const vk_device& Device,
                                            const vk_image& Image)
  {
    return Create(Device, Image, VK_IMAGE_ASPECT_COLOR_BIT);
  }

  vk_image_view vk_image_view::Create(const vk_device& Device,
                                      const vk_image& Image,
                                      const VkImageAspectFlags Aspect)
  {
    VkImageViewCreateInfo CreateInfo = GetImageViewCreateInfo();
    CreateInfo.image = Image.GetHandle();
    CreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

    CreateInfo.format = Image.GetFormat();

    CreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    CreateInfo.subresourceRange.aspectMask = Aspect;
    CreateInfo.subresourceRange.baseMipLevel = 0;
    CreateInfo.subresourceRange.levelCount = 1;
    CreateInfo.subresourceRange.baseArrayLayer = 0;
    CreateInfo.subresourceRange.layerCount = 1;

    vk_image_view Result;
    Result.Device_ = Device.GetHandle();

    vk_status Status;
    Status = vkCreateImageView(Result.Device_,
                               &CreateInfo,
                               VK_NULL_HANDLE,
                               &Result.Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_image_view: " << Status);
      return {};
    }

    return Result;
  }

  vk_image_view::vk_image_view(const VkDevice Device,
                               const VkImage Image,
                               const VkImageViewType Type,
                               const VkFormat Format,
                               const VkImageAspectFlags AspectMask)
    : Device_(Device)
  {
    VkImageViewCreateInfo CreateInfo = GetImageViewCreateInfo();
    CreateInfo.image = Image;
    CreateInfo.viewType = Type;

    CreateInfo.format = Format;

    CreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    CreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

    CreateInfo.subresourceRange.aspectMask = AspectMask;
    CreateInfo.subresourceRange.baseMipLevel = 0;
    CreateInfo.subresourceRange.levelCount = 1;
    CreateInfo.subresourceRange.baseArrayLayer = 0;
    CreateInfo.subresourceRange.layerCount = 1;

    vk_status Status;
    Status = vkCreateImageView(Device_,
                               &CreateInfo,
                               VK_NULL_HANDLE,
                               &Handle_);

    if (Status.IsError())
    {
      LogError("Failed to build vk_image_view: " << Status);
      return;
    }
  }

  vk_image_view::vk_image_view(vk_image_view&& Other)
  {
    std::swap(Handle_, Other.Handle_);
    std::swap(Device_, Other.Device_);
  }

  vk_image_view& vk_image_view::operator=(vk_image_view&& Rhs)
  {
    std::swap(Handle_, Rhs.Handle_);
    std::swap(Device_, Rhs.Device_);
    return *this;
  }

  vk_image_view::~vk_image_view()
  {
    if (Handle_ != VK_NULL_HANDLE)
    {
      LogInternal("Destroying vk_image_view...");
      vkDestroyImageView(Device_, Handle_, VK_NULL_HANDLE);
    }
  }

  VkImageView vk_image_view::GetHandle() const
  {
    return Handle_;
  }

}  // myran
