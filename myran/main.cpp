#include "debug.h"
#include "window.h"

#include "game/maze/grid.h"
#include "game/maze/distance_map.h"
#include "game/maze/samplers/uniform.h"
#include "game/maze/generators/binary_tree.h"
#include "game/maze/generators/sidewinder.h"
#include "game/maze/generators/hunt_and_kill.h"
#include "game/maze/generators/recursive_backtracer.h"
#include "game/maze/solvers/djikstra.h"

#include "renderer/vulkan/vulkan.h"

#include "application.h"

using namespace myran;

void GridTest()
{
  u32 Rows = 16;
  u32 Cols = 24;
  distance_grid Grid(Rows, Cols);

  uniform_sampler Sampler;
  djikstra_solver Solver;
  path ShortestPath;

  binary_tree_generator BTGenerator(Sampler);
  Grid = distance_grid(Rows, Cols);
  BTGenerator.Generate(Grid);
  ShortestPath = Solver.FindShortestPath(Grid, 0, 0, 0, 9);
  Grid.SetDistanceMap(ConvertPathToDistanceMap(ShortestPath));
  LogInfo("Binary Tree Maze:\n" << Grid);

  sidewinder_generator SWGenerator(Sampler);
  Grid = distance_grid(Rows, Cols);
  SWGenerator.Generate(Grid);
  distance_map Distances = Solver.FindAllDistances(Grid, Rows / 2, Cols / 2);
  Grid.SetDistanceMap(Distances);
  LogInfo("Sidewinder Maze:\n" << Grid);

  hunt_and_kill_generator HuntAndKillGenerator(Sampler);
  Grid = distance_grid(Rows, Cols);
  HuntAndKillGenerator.Generate(Grid);
  ShortestPath = Solver.FindShortestPath(Grid, 0, 0, 11, 14);
  Grid.SetDistanceMap(ConvertPathToDistanceMap(ShortestPath));
  LogInfo("Hunt-and-Kill Maze:\n" << Grid);

  recursive_backtracer_generator RecursiveBacktracerGenerator(Sampler);
  Grid = distance_grid(Rows, Cols);
  RecursiveBacktracerGenerator.Generate(Grid);
  ShortestPath = Solver.FindShortestPath(Grid, 0, 0, 11, 14);
  Grid.SetDistanceMap(ConvertPathToDistanceMap(ShortestPath));
  LogInfo("Recursive Backtracer Maze:\n" << Grid);
}

int main(int Argc, char** Argv)
{
  LogInfo("Welcome to myran");

  GridTest();

  auto Application = myran::application::Create();
  if (!Application) return 1;
  return Application->Run();
}
