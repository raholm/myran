#pragma once

#include <utility>
#include <unordered_map>

#include <myran/types.h>

namespace myran {

  struct pair_hash
  {
    template <class T1, class T2>
    std::size_t operator () (std::pair<T1, T2> const &Pair) const
	    {
        std::size_t Hash1 = std::hash<T1>()(Pair.first);
        std::size_t Hash2 = std::hash<T2>()(Pair.second);
        return Hash1 ^ (Hash2 << 1);
	    }
  };

  // @todo we might want to call this a sparse_distance-map
  using distance_map = std::unordered_map<std::pair<u32, u32>, s32, pair_hash>;

  // @todo implement dense distance map
  using dense_distance_Map = std::vector<s32>;

}  // myran
