#include "uniform.h"

namespace myran {

  uniform_sampler::uniform_sampler()
    : RandomGenerator_(std::random_device{}())
  {}

  uniform_sampler::uniform_sampler(const seed_type Seed)
    : RandomGenerator_(Seed)
  {}

  u32 uniform_sampler::NextU32(const u32 Min, const u32 Max)
  {
    std::uniform_int_distribution<u32> Distribution(Min, Max);
    return Distribution(RandomGenerator_);
  }

  u32 uniform_sampler::NextU32(const u32 Max)
  {
    return NextU32(0, Max);
  }

}  // myran
