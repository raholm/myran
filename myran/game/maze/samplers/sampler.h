#pragma once

#include <myran/types.h>

namespace myran {

  class sampler
  {
  public:
    virtual ~sampler() = default;

    virtual u32 NextU32(const u32 Min, const u32 Max) = 0;
    virtual u32 NextU32(const u32 Max) = 0;

  };

}  // myran
