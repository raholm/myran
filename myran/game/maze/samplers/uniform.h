#pragma once

#include <random>

#include <myran/types.h>

#include "sampler.h"

namespace myran {

  class uniform_sampler final : public sampler
  {
  public:
    using seed_type = std::mt19937::result_type;

  public:
    uniform_sampler();
    uniform_sampler(const seed_type Seed);

    ~uniform_sampler() = default;

    u32 NextU32(const u32 Min, const u32 Max) override;
    u32 NextU32(const u32 Max) override;

  private:
    std::mt19937 RandomGenerator_;

  };

}  // myran
