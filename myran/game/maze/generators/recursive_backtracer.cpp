#include "recursive_backtracer.h"

#include <stack>

#include <myran/debug.h>

namespace myran {

  recursive_backtracer_generator::recursive_backtracer_generator(sampler& Sampler)
    : Sampler_(Sampler)
  {}

  void recursive_backtracer_generator::Generate(grid& Grid)
  {
    cell::coord CurrentCell = cell::coord(Sampler_.NextU32(Grid.GetRows() - 1),
                                          Sampler_.NextU32(Grid.GetCols() - 1));

    std::stack<cell::coord> Stack;
    Stack.push(CurrentCell);

    while (!Stack.empty())
    {
      CurrentCell = Stack.top();
      Grid.SetVisited(CurrentCell.Row, CurrentCell.Col);

      std::vector<cell::neighbor_data> UnvisitedNeighbors = Grid.GetUnvisitedNeighbors(CurrentCell.Row,
                                                                                       CurrentCell.Col);

      if (UnvisitedNeighbors.size() == 0)
      {
        Stack.pop();
        continue;
      }

      const u32 Index = Sampler_.NextU32(static_cast<u32>(UnvisitedNeighbors.size()) - 1);
      const cell::neighbor_data Neighbor = UnvisitedNeighbors[Index];

      Grid.AddLink(CurrentCell.Row, CurrentCell.Col, Neighbor.Direction);

      cell::coord NeighborCoord(Neighbor.Row, Neighbor.Col);
      Stack.push(NeighborCoord);
    }
  }

}  // myran
