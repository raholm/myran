#include "hunt_and_kill.h"

#include <myran/debug.h>

namespace myran {

  hunt_and_kill_generator::hunt_and_kill_generator(sampler& Sampler)
    : Sampler_(Sampler)
  {}

  void hunt_and_kill_generator::Generate(grid& Grid)
  {
    cell::coord CurrentCell = cell::coord(Sampler_.NextU32(Grid.GetRows() - 1),
                                          Sampler_.NextU32(Grid.GetCols() - 1));

    bool Done = false;

    while (!Done)
    {
      std::vector<cell::neighbor_data> UnvisitedNeighbors = Grid.GetUnvisitedNeighbors(CurrentCell.Row,
                                                                                       CurrentCell.Col);

      if (UnvisitedNeighbors.size() > 0)
      {
        const u32 Index = Sampler_.NextU32(static_cast<u32>(UnvisitedNeighbors.size()) - 1);
        const cell::neighbor_data Neighbor = UnvisitedNeighbors[Index];

        Grid.AddLink(CurrentCell.Row, CurrentCell.Col, Neighbor.Direction);
        Grid.SetVisited(CurrentCell.Row, CurrentCell.Col);

        CurrentCell.Row = Neighbor.Row;
        CurrentCell.Col = Neighbor.Col;
      }
      else
      {
        Done = true;
        bool FoundNewCell = false;

        for (u32 Row = 0; Row < Grid.GetRows(); ++Row)
        {
          for (u32 Col = 0; Col < Grid.GetCols(); ++Col)
          {
            CurrentCell.Row = Row;
            CurrentCell.Col = Col;

            const cell& Cell = Grid(CurrentCell.Row, CurrentCell.Col);

            std::vector<cell::neighbor_data> VisitedNeighbors = Grid.GetVisitedNeighbors(CurrentCell.Row,
                                                                                         CurrentCell.Col);

            if (!Cell.IsLinked() && VisitedNeighbors.size() > 0)
            {

              const u32 Index = Sampler_.NextU32(static_cast<u32>(VisitedNeighbors.size()) - 1);
              const cell::neighbor_data Neighbor = VisitedNeighbors[Index];

              Grid.AddLink(CurrentCell.Row, CurrentCell.Col, Neighbor.Direction);
              Grid.SetVisited(CurrentCell.Row, CurrentCell.Col);

              Done = false;
              FoundNewCell = true;
              break;
            }
          }

          if (FoundNewCell)
            break;
        }
      }
    }
  }

}  // myran
