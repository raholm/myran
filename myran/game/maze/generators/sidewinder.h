#pragma once

#include "../samplers/sampler.h"

#include "generator.h"

namespace myran {

  class sidewinder_generator final : public generator
  {
  public:
    sidewinder_generator(sampler& Sampler);

    ~sidewinder_generator() = default;

    void Generate(grid& Grid) override;

  private:
    sampler& Sampler_;

  };

}  // myran
