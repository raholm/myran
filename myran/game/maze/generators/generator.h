#pragma once

#include "../grid.h"

namespace myran {

  class generator
  {
  public:
    virtual ~generator() = default;

    virtual void Generate(grid& Grid) = 0;

  };

}  // myran
