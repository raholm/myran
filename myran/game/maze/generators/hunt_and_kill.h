#pragma once

#include "../samplers/sampler.h"

#include "generator.h"

namespace myran {

  class hunt_and_kill_generator final : public generator
  {
  public:
    hunt_and_kill_generator(sampler& Sampler);

    ~hunt_and_kill_generator() = default;

    void Generate(grid& Grid) override;

  private:
    sampler& Sampler_;

  };

}  // myran
