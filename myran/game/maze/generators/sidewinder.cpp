#include "sidewinder.h"

#include <myran/debug.h>

namespace myran {

  sidewinder_generator::sidewinder_generator(sampler& Sampler)
    : Sampler_(Sampler)
  {}

  void sidewinder_generator::Generate(grid& Grid)
  {
    for (u32 Row = 0; Row < Grid.GetRows(); ++Row)
    {
      std::vector<cell::coord> Run;

      for (u32 Col = 0; Col < Grid.GetCols(); ++Col)
      {
        const cell& Cell = Grid(Row, Col);

        if (Cell.IsVisited())
          continue;

        Run.push_back(cell::coord(Row, Col));

        const bool AtEasternBoundary = !Cell.HasNeighbor(cell::neighbor::East);
        const bool AtNorthernBoundary = !Cell.HasNeighbor(cell::neighbor::North);

        if (AtEasternBoundary ||
            (!AtNorthernBoundary && Sampler_.NextU32(1) == 0))
        {
          const u32 Index = Sampler_.NextU32(static_cast<u32>(Run.size()) - 1);
          const cell::coord CellCoordinate = Run[Index];
          const cell& CellToLink = Grid(CellCoordinate.Row, CellCoordinate.Col);

          if (CellToLink.HasNeighbor(cell::neighbor::North))
            Grid.AddLink(CellCoordinate.Row,
                         CellCoordinate.Col,
                         cell::neighbor::North);

          Run.clear();
        }
        else
        {
          Grid.AddLink(Row, Col, cell::neighbor::East);
        }
      }
    }
  }

}  // myran
