#pragma once

#include "../samplers/sampler.h"

#include "generator.h"

namespace myran {

  class recursive_backtracer_generator final : public generator
  {
  public:
    recursive_backtracer_generator(sampler& Sampler);

    ~recursive_backtracer_generator() = default;

    void Generate(grid& Grid) override;

  private:
    sampler& Sampler_;

  };

}  // myran
