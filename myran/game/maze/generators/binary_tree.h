#pragma once

#include "../samplers/sampler.h"

#include "generator.h"

namespace myran {

  class binary_tree_generator final : public generator
  {
  public:
    binary_tree_generator(sampler& Sampler);

    ~binary_tree_generator() = default;

    void Generate(grid& Grid) override;

  private:
    sampler& Sampler_;

  };

}  // myran
