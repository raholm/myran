#include "binary_tree.h"

#include <myran/debug.h>

namespace myran {

  binary_tree_generator::binary_tree_generator(sampler& Sampler)
    : Sampler_(Sampler)
  {}

  void binary_tree_generator::Generate(grid& Grid)
  {
    for (u32 Row = 0; Row < Grid.GetRows(); ++Row)
    {
      for (u32 Col = 0; Col < Grid.GetCols(); ++Col)
      {
        cell& Cell = Grid(Row, Col);

        if (Cell.IsVisited())
          continue;

        std::vector<cell::neighbor> Neighbors;

        if (Cell.HasNeighbor(cell::neighbor::North))
          Neighbors.push_back(cell::neighbor::North);

        if (Cell.HasNeighbor(cell::neighbor::East))
          Neighbors.push_back(cell::neighbor::East);

        if (Neighbors.size() > 0)
        {
          u32 Index = Sampler_.NextU32(static_cast<u32>(Neighbors.size()) - 1);
          Grid.AddLink(Row, Col, Neighbors[Index]);
        }

        Grid.SetVisited(Row, Col);
      }
    }
  }

}  // myran
