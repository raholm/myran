#include "grid.h"

#include <myran/debug.h>

namespace myran {

  bool cell::HasNeighbor(const neighbor Neighbor) const
  {
    switch(Neighbor)
    {
    case neighbor::North: return Mask_ & CELL_NEIGHBOR_NORTH;
    case neighbor::South: return Mask_ & CELL_NEIGHBOR_SOUTH;
    case neighbor::West: return Mask_ & CELL_NEIGHBOR_WEST;
    case neighbor::East: return Mask_ & CELL_NEIGHBOR_EAST;
    };

    return false;
  }

  bool cell::IsLinked(const neighbor Neighbor) const
  {
    switch(Neighbor)
    {
    case neighbor::North: return Mask_ & CELL_LINK_NORTH;
    case neighbor::South: return Mask_ & CELL_LINK_SOUTH;
    case neighbor::West: return Mask_ & CELL_LINK_WEST;
    case neighbor::East: return Mask_ & CELL_LINK_EAST;
    };

    return false;
  }

  bool cell::IsLinked() const
  {
    return (Mask_ & CELL_LINK_NORTH) |
      (Mask_ & CELL_LINK_SOUTH) |
      (Mask_ & CELL_LINK_WEST) |
      (Mask_ & CELL_LINK_EAST);
  }

  bool cell::IsVisited() const
  {
    return Mask_ & CELL_VISITED;
  }

  u32 cell::GetLinkedCount() const
  {
    return
      IsLinked(neighbor::North) +
      IsLinked(neighbor::South) +
      IsLinked(neighbor::East) +
      IsLinked(neighbor::West);
  }

  grid::grid(const u32 Rows,
             const u32 Cols)
    : Cells_(Rows * Cols),
      Rows_(Rows),
      Cols_(Cols)
  {
    Assert(Rows > 0);
    Assert(Cols > 0);

    Reset();
  }

  cell& grid::operator()(const u32 Row, const u32 Col)
  {
    Assert(Row < GetRows());
    Assert(Col < GetCols());

    return Cells_[Row * GetCols() + Col];
  }

  const cell& grid::operator()(const u32 Row, const u32 Col) const
  {
    Assert(Row < GetRows());
    Assert(Col < GetCols());

    return Cells_[Row * GetCols() + Col];
  }

  void grid::Reset()
  {
    for (u32 Row = 0; Row < GetRows(); ++Row)
    {
      for (u32 Col = 0; Col < GetCols(); ++Col)
      {
        cell& Cell = operator()(Row, Col);
        Cell.Mask_ = 0;

        if (Row > 0)
          Cell.Mask_ |= cell::CELL_NEIGHBOR_SOUTH;

        if (Row < GetRows() - 1)
          Cell.Mask_ |= cell::CELL_NEIGHBOR_NORTH;

        if (Col > 0)
          Cell.Mask_ |= cell::CELL_NEIGHBOR_WEST;

        if (Col < GetCols() - 1)
          Cell.Mask_ |= cell::CELL_NEIGHBOR_EAST;
      }
    }
  }

  void grid::SetVisited(const u32 Row, const u32 Col)
  {
    cell& Cell = operator()(Row, Col);
    Cell.Mask_ |= cell::CELL_VISITED;
  }

  void grid::AddLink(const u32 Row, const u32 Col,
                     const cell::neighbor Neighbor,
                     const bool Bidirectional)
  {
    cell& Cell = operator()(Row, Col);
    Assert(Cell.HasNeighbor(Neighbor));

    switch(Neighbor)
    {
    case cell::neighbor::North: Cell.Mask_ |= cell::CELL_LINK_NORTH; break;
    case cell::neighbor::South: Cell.Mask_ |= cell::CELL_LINK_SOUTH; break;
    case cell::neighbor::West:  Cell.Mask_ |= cell::CELL_LINK_WEST;  break;
    case cell::neighbor::East:  Cell.Mask_ |= cell::CELL_LINK_EAST;  break;
    };

    if (Bidirectional)
    {
      cell::neighbor_data NeighborData = GetNeighbor(Row, Col, Neighbor);
      cell::neighbor OppositeNeighbor = GetOppositeNeighbor(Neighbor);
      AddLink(NeighborData.Row, NeighborData.Col, OppositeNeighbor, false);
    }
  }

  u32 grid::GetRows() const
  {
    return Rows_;
  }

  u32 grid::GetCols() const
  {
    return Cols_;
  }

  std::string grid::GetCellContent(const u32 Row, const u32 Col) const
  {
    return "   ";
  }

  std::vector<cell::neighbor_data> grid::GetLinkedNeighbors(const u32 Row, const u32 Col) const
  {
    const cell& Cell = operator()(Row, Col);

    std::vector<cell::neighbor_data> LinkedNeighbors;

    auto AddLinkedNeighbor = [&](const cell::neighbor Direction)
                               {
                                 if (Cell.IsLinked(Direction))
                                   LinkedNeighbors.push_back(GetNeighbor(Row, Col, Direction));
                               };

    AddLinkedNeighbor(cell::neighbor::North);
    AddLinkedNeighbor(cell::neighbor::South);
    AddLinkedNeighbor(cell::neighbor::West);
    AddLinkedNeighbor(cell::neighbor::East);

    return LinkedNeighbors;
  }

  std::vector<cell::neighbor_data> grid::GetUnvisitedNeighbors(const u32 Row, const u32 Col) const
  {
    const cell& Cell = operator()(Row, Col);

    std::vector<cell::neighbor_data> UnvisitedNeighbors;

    auto AddUnvisitedNeighbor = [&](const cell::neighbor Direction)
                                  {
                                    if (Cell.HasNeighbor(Direction))
                                    {
                                      cell::neighbor_data Neighbor = GetNeighbor(Row, Col, Direction);
                                      const cell& NeighborCell = operator()(Neighbor.Row, Neighbor.Col);

                                      if (!NeighborCell.IsVisited())
                                        UnvisitedNeighbors.push_back(Neighbor);
                                    }
                                  };

    AddUnvisitedNeighbor(cell::neighbor::North);
    AddUnvisitedNeighbor(cell::neighbor::South);
    AddUnvisitedNeighbor(cell::neighbor::West);
    AddUnvisitedNeighbor(cell::neighbor::East);

    return UnvisitedNeighbors;
  }

  std::vector<cell::neighbor_data> grid::GetVisitedNeighbors(const u32 Row, const u32 Col) const
  {
    const cell& Cell = operator()(Row, Col);

    std::vector<cell::neighbor_data> VisitedNeighbors;

    auto AddVisitedNeighbor = [&](const cell::neighbor Direction)
                                {
                                  if (Cell.HasNeighbor(Direction))
                                  {
                                    cell::neighbor_data Neighbor = GetNeighbor(Row, Col, Direction);
                                    const cell& NeighborCell = operator()(Neighbor.Row, Neighbor.Col);

                                    if (NeighborCell.IsVisited())
                                      VisitedNeighbors.push_back(Neighbor);
                                  }
                                };

    AddVisitedNeighbor(cell::neighbor::North);
    AddVisitedNeighbor(cell::neighbor::South);
    AddVisitedNeighbor(cell::neighbor::West);
    AddVisitedNeighbor(cell::neighbor::East);

    return VisitedNeighbors;
  }

  cell::neighbor grid::GetOppositeNeighbor(const cell::neighbor Neighbor) const
  {
    switch(Neighbor)
    {
    case cell::neighbor::North: return cell::neighbor::South;
    case cell::neighbor::South: return cell::neighbor::North;
    case cell::neighbor::West:  return cell::neighbor::East;
    case cell::neighbor::East:  return cell::neighbor::West;
    };

    LogError("This should never happen");
    return cell::neighbor::North;
  }

  cell::neighbor_data grid::GetNeighbor(const u32 Row,
                                        const u32 Col,
                                        const cell::neighbor Neighbor) const

  {
    switch(Neighbor)
    {
    case cell::neighbor::North: return cell::neighbor_data(Row + 1, Col, Neighbor);
    case cell::neighbor::South: return cell::neighbor_data(Row - 1, Col, Neighbor);
    case cell::neighbor::West:  return cell::neighbor_data(Row, Col - 1, Neighbor);
    case cell::neighbor::East:  return cell::neighbor_data(Row, Col + 1, Neighbor);
    };

    LogError("This should never happen");
    return cell::neighbor_data(Row, Col, cell::neighbor::North);
  }

  distance_grid::distance_grid(const u32 Rows, const u32 Cols)
    : base_class(Rows, Cols)
  {}

  void distance_grid::SetDistanceMap(const distance_map& DistanceMap)
  {
    DistanceMap_ = DistanceMap;
  }

  std::string distance_grid::GetCellContent(const u32 Row, const u32 Col) const
  {
    std::pair<u32, u32> Coordinate = std::make_pair(Row, Col);

    auto It = DistanceMap_.find(Coordinate);

    if (It == DistanceMap_.end())
      return "   ";

    s32 Distance = It->second;

    if (Distance < 10)
      return " " + std::to_string(Distance) + " ";
    else if (Distance < 100)
      return std::to_string(Distance) + " ";
    else
      return std::to_string(Distance);
  }


}  // myran
