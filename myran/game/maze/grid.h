#pragma once

#include <vector>
#include <iostream>
#include <sstream>
#include <string>

#include <myran/types.h>

#include "distance_map.h"

namespace myran {

  class grid;

  class cell
  {
  public:
    enum class neighbor
    {
	    North,
	    South,
	    East,
	    West,
    };

    struct coord
    {
	    coord() = default;
	    coord(const u32 _Row,
            const u32 _Col)
        : Row(_Row),
          Col(_Col)
        {}

	    u32 Row;
	    u32 Col;
    };

    struct neighbor_data
    {
	    neighbor_data() = default;
	    neighbor_data(const u32 _Row, const u32 _Col, const neighbor _Direction)
        : Row(_Row),
          Col(_Col),
          Direction(_Direction)
        {}

	    u32 Row;
	    u32 Col;
	    neighbor Direction;
    };

  public:
    cell() = default;

    ~cell() = default;

    bool HasNeighbor(const neighbor Neighbor) const;
    bool IsLinked(const neighbor Neighbor) const;
    bool IsLinked() const;
    bool IsVisited() const;
    u32 GetLinkedCount() const;

  private:
    enum flag
    {
	    CELL_LINK_NORTH = 0x1,
	    CELL_LINK_SOUTH = 0x2,
	    CELL_LINK_WEST = 0x4,
	    CELL_LINK_EAST = 0x8,
	    CELL_VISITED = 0x10,
	    CELL_NEIGHBOR_NORTH = 0x20,
	    CELL_NEIGHBOR_SOUTH = 0x40,
	    CELL_NEIGHBOR_WEST = 0x80,
	    CELL_NEIGHBOR_EAST = 0x100,
    };

    s32 Mask_ { 0 };

    friend class grid;

  };

  inline std::string ToString(const cell::neighbor Neighbor)
  {
    switch(Neighbor)
    {
    case cell::neighbor::North: return "North";
    case cell::neighbor::South: return "South";
    case cell::neighbor::West:  return "West";
    case cell::neighbor::East:  return "East";
    };

    return "Unknown";
  }

  class grid
  {
  public:
    grid(const u32 Rows,
         const u32 Cols);

    ~grid() = default;

    cell& operator()(const u32 Row, const u32 Col);
    const cell& operator()(const u32 Row, const u32 Col) const;

    void Reset();

    void SetVisited(const u32 Row, const u32 Col);

    void AddLink(const u32 Row, const u32 Col,
                 const cell::neighbor Neighbor,
                 const bool Bidirectional = true);

    u32 GetRows() const;
    u32 GetCols() const;
    std::vector<cell::neighbor_data> GetLinkedNeighbors(const u32 Row, const u32 Col) const;
    std::vector<cell::neighbor_data> GetUnvisitedNeighbors(const u32 Row, const u32 Col) const;
    std::vector<cell::neighbor_data> GetVisitedNeighbors(const u32 Row, const u32 Col) const;
    virtual std::string GetCellContent(const u32 Row, const u32 Col) const;

  private:
    std::vector<cell> Cells_;
    u32 Rows_;
    u32 Cols_;

    cell::neighbor GetOppositeNeighbor(const cell::neighbor Neighbor) const;
    cell::neighbor_data GetNeighbor(const u32 Row,
                                    const u32 Col,
                                    const cell::neighbor Neighbor) const;

  };

  class distance_grid : public grid
  {
  public:
    using base_class = grid;

  public:
    distance_grid(const u32 Rows, const u32 Cols);

    ~distance_grid() = default;

    void SetDistanceMap(const distance_map& DistanceMap);

    std::string GetCellContent(const u32 Row, const u32 Col) const override;

  private:
    distance_map DistanceMap_;

  };

  inline std::ostream& operator<<(std::ostream& Out, const grid& Grid)
  {
    const u32 Rows = Grid.GetRows();
    const u32 Cols = Grid.GetCols();

    Out << "+";

    for (u32 Col = 0; Col < Cols; ++Col)
	    Out << "---+";

    Out << "\n";

    for (u32 Row = Rows; Row; --Row)
    {
	    std::ostringstream Top;
	    std::ostringstream Bottom;

	    Top << "|";
	    Bottom << "+";

	    for (u32 Col = 0; Col < Cols; ++Col)
	    {
        const cell& Cell = Grid(Row - 1, Col);

        Top << Grid.GetCellContent(Row - 1, Col)
            << (Cell.IsLinked(cell::neighbor::East) ? " " : "|");
        Bottom << (Cell.IsLinked(cell::neighbor::South) ? "   " : "---")
               << "+";
	    }

	    Out << Top.str() << "\n";
	    Out << Bottom.str() << "\n";
    }

    return Out;
  }

}  // myran
