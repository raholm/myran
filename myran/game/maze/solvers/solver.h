#pragma once

#include <myran/types.h>

#include "../grid.h"

#include "path.h"

namespace myran {

  class solver
  {
  public:
    virtual ~solver() = default;

    virtual path FindShortestPath(const grid& Grid,
                                  const u32 StartRow,
                                  const u32 StartCol,
                                  const u32 EndRow,
                                  const u32 EndCol) = 0;
    virtual path FindLongestPath(const grid& Grid) = 0;
    virtual distance_map FindAllDistances(const grid& Grid,
                                          const u32 StartRow,
                                          const u32 StartCol) = 0;

  };


}  // myran
