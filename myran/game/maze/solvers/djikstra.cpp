#include "djikstra.h"

#include <queue>
#include <algorithm>

#include <myran/debug.h>

namespace myran {

  path djikstra_solver::FindShortestPath(const grid& Grid,
                                         const u32 StartRow,
                                         const u32 StartCol,
                                         const u32 EndRow,
                                         const u32 EndCol)
  {
    Assert(StartRow < Grid.GetRows());
    Assert(StartCol < Grid.GetCols());
    Assert(EndRow < Grid.GetRows());
    Assert(EndCol < Grid.GetCols());

    distance_map Distances = ConstructDistanceMap(Grid, StartRow, StartCol, EndRow, EndCol);
    return ConstructShortestPath(Grid, Distances, StartRow, StartCol, EndRow, EndCol);
  }

  path djikstra_solver::FindLongestPath(const grid& Grid)
  {
    // @todo : Implement this
    LogError("NOT IMPLEMENTED YET");
    return path();
  }

  distance_map djikstra_solver::FindAllDistances(const grid& Grid,
                                                 const u32 StartRow,
                                                 const u32 StartCol)
  {
    distance_map Distances;
    Distances[std::make_pair(StartRow, StartCol)] = 0;

    std::queue<std::pair<u32, u32>> Frontier;
    Frontier.push(std::make_pair(StartRow, StartCol));

    while (!Frontier.empty())
    {
      std::pair<u32, u32> CurrentCell = Frontier.front();
      Frontier.pop();
      std::vector<cell::neighbor_data> LinkedNeighbors = Grid.GetLinkedNeighbors(CurrentCell.first,
                                                                                 CurrentCell.second);

      for (const cell::neighbor_data& Neighbor : LinkedNeighbors)
      {
        std::pair<u32, u32> NeighborCoord = std::make_pair(Neighbor.Row, Neighbor.Col);
        if (Distances.find(NeighborCoord) != Distances.end())
          continue;

        Distances[NeighborCoord] = Distances[CurrentCell] + 1;
        Frontier.push(NeighborCoord);
      }
    }

    return Distances;
  }

  distance_map djikstra_solver::ConstructDistanceMap(const grid& Grid,
                                                     const u32 StartRow,
                                                     const u32 StartCol,
                                                     const u32 EndRow,
                                                     const u32 EndCol) const
  {
    distance_map Distances;
    Distances[std::make_pair(StartRow, StartCol)] = 0;

    std::queue<std::pair<u32, u32>> Frontier;
    Frontier.push(std::make_pair(StartRow, StartCol));
    bool FoundDestination = false;

    while (!FoundDestination && !Frontier.empty())
    {
      std::pair<u32, u32> CurrentCell = Frontier.front();
      Frontier.pop();

      std::vector<cell::neighbor_data> LinkedNeighbors = Grid.GetLinkedNeighbors(CurrentCell.first,
                                                                                 CurrentCell.second);

      for (const cell::neighbor_data& Neighbor : LinkedNeighbors)
      {
        std::pair<u32, u32> NeighborCoord = std::make_pair(Neighbor.Row, Neighbor.Col);
        if (Distances.find(NeighborCoord) != Distances.end())
          continue;

        Distances[NeighborCoord] = Distances[CurrentCell] + 1;

        if (NeighborCoord.first == EndRow &&
            NeighborCoord.second == EndCol)
        {
          FoundDestination = true;
          break;
        }

        Frontier.push(NeighborCoord);
      }
    }

    return Distances;
  }

  path djikstra_solver::ConstructShortestPath(const grid& Grid,
                                              const distance_map& Distances,
                                              const u32 StartRow,
                                              const u32 StartCol,
                                              const u32 EndRow,
                                              const u32 EndCol) const
  {
    path ShortestPath;
    bool HasConstructedPath = false;
    std::pair<u32, u32> CurrentCell = std::make_pair(EndRow, EndCol);

    while (!HasConstructedPath)
    {
      path_element PathElement;
      PathElement.Row = CurrentCell.first;
      PathElement.Col = CurrentCell.second;
      PathElement.Distance = Distances.find(CurrentCell)->second;
      ShortestPath.push_back(PathElement);

      std::vector<cell::neighbor_data> LinkedNeighbors = Grid.GetLinkedNeighbors(CurrentCell.first,
                                                                                 CurrentCell.second);

      s32 MinDistance = std::numeric_limits<s32>::max();
      std::pair<u32, u32> MinCell;

      for (const cell::neighbor_data& Neighbor : LinkedNeighbors)
      {
        std::pair<u32, u32> NeighborCoord = std::make_pair(Neighbor.Row, Neighbor.Col);
        auto It = Distances.find(NeighborCoord);

        if (It == Distances.end())
          continue;

        const s32 NeighborDistance = It->second;

        if (NeighborDistance < MinDistance)
        {
          MinDistance = NeighborDistance;
          MinCell = NeighborCoord;
        }
      }

      if (MinCell.first == StartRow &&
          MinCell.second == StartCol)
        HasConstructedPath = true;

      CurrentCell = MinCell;
    }

    path_element PathElement;
    PathElement.Row = StartRow;
    PathElement.Col = StartCol;
    PathElement.Distance = 0;
    ShortestPath.push_back(PathElement);

    std::reverse(ShortestPath.begin(), ShortestPath.end());
    return ShortestPath;
  }

}  // myran
