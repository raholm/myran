#pragma once

#include <vector>

#include <myran/types.h>

#include "../distance_map.h"

namespace myran {

  struct path_element
  {
    u32 Row;
    u32 Col;
    s32 Distance;
  };

  using path = std::vector<path_element>;

  distance_map ConvertPathToDistanceMap(const path& Path);

}  // myran
