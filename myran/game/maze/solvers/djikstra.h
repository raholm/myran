#pragma once

#include "solver.h"

namespace myran {

  class djikstra_solver : public solver
  {
  public:
    djikstra_solver() = default;

    ~djikstra_solver() = default;

    path FindShortestPath(const grid& Grid,
                          const u32 StartRow,
                          const u32 StartCol,
                          const u32 EndRow,
                          const u32 EndCol) override;
    path FindLongestPath(const grid& Grid) override;
    distance_map FindAllDistances(const grid& Grid,
                                  const u32 StartRow,
                                  const u32 StartCol) override;

  private:
    distance_map ConstructDistanceMap(const grid& Grid,
                                      const u32 StartRow,
                                      const u32 StartCol,
                                      const u32 EndRow,
                                      const u32 EndCol) const;
    path ConstructShortestPath(const grid& Grid,
                               const distance_map& Distances,
                               const u32 StartRow,
                               const u32 StartCol,
                               const u32 EndRow,
                               const u32 EndCol) const;

  };


}  // myran
