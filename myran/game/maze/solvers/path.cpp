#include "path.h"

namespace myran {

  distance_map ConvertPathToDistanceMap(const path& Path)
  {
    distance_map DistanceMap;

    for (const path_element& PathElement : Path)
      DistanceMap[std::make_pair(PathElement.Row, PathElement.Col)] = PathElement.Distance;

    return DistanceMap;
  }

}  // myran
