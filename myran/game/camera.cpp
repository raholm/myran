#include <myran/debug.h>

#include "camera.h"

namespace myran {

  glm::mat4 first_person_camera::GetView() const
  {
    if (Changed)
      Update();

    return View;
  }

  glm::mat4 first_person_camera::GetProjection() const
  {
    return Projection;
  }

  glm::mat4 first_person_camera::GetProjectionView() const
  {
    if (Changed)
      Update();

    return ProjectionView;
  }

  void first_person_camera::Update() const
  {
    View = glm::lookAt(Position, Position + Forward, Up);
    ProjectionView = Projection * View;
    Changed = false;
  }

  glm::mat4 minimap_camera::GetView() const
  {
    if (Changed)
      Update();

    return View;
  }

  glm::mat4 minimap_camera::GetProjection() const
  {
    return Projection;
  }

  glm::mat4 minimap_camera::GetProjectionView() const
  {
    if (Changed)
      Update();

    return ProjectionView;
  }

  void minimap_camera::Update() const
  {
    View = glm::lookAt(Position, Position + Forward, Up);
    ProjectionView = Projection * View;
    Changed = false;
  }

}  // myran
