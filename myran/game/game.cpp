#include <myran/debug.h>

#include "../window.h"

#include "maze/generators/binary_tree.h"
#include "maze/generators/sidewinder.h"
#include "maze/generators/hunt_and_kill.h"
#include "maze/generators/recursive_backtracer.h"

#include "game.h"

namespace myran {

  std::unique_ptr<game> game::Create(const window& Window,
                                     const u32 Rows,
                                     const u32 Cols,
                                     const generator_type Generator)
  {
    LogInfo("Creating game...");

    auto result = std::unique_ptr<game>(new game(Window, Rows, Cols, Generator));
    if (!result) return nullptr;

    return result;
  }

  game::game(const window& Window,
             const u32 Rows,
             const u32 Cols,
             const generator_type Generator)
    : Grid_(Rows, Cols),
      Mode_(mode::Gameplay),
      MoveForward_(false),
      MoveBackward_(false),
      MoveRight_(false),
      MoveLeft_(false)
  {
    GenerateGrid(Generator);

    MazeGeometry_.FloorBlocks.reserve(4 * Rows * Cols);
    MazeGeometry_.WallBlocks.reserve(7 * Rows * Cols);

    const auto AspectRatio = static_cast<f32>(Window.GetScreenWidth()) / static_cast<f32>(Window.GetScreenHeight());
    const auto FieldOfView = 70.0f;
    const auto Near = 0.01f;
    const auto Far = 1000.0f;

    FirstMousePosition_ = true;

    GlobalObserver_.Position = { 0.0f, 15.0f, 40.0f };
    GlobalObserver_.Forward = { 0.0f, 0.0f, -1.0f };
    GlobalObserver_.Up = { 0.0f, 1.0f, 0.0f };
    GlobalObserver_.Yaw = -90.0f;
    GlobalObserver_.Pitch = 0.0f;
    GlobalObserver_.Projection = glm::perspective(glm::radians(FieldOfView), AspectRatio, Near, Far);
    GlobalObserver_.Changed = true;

    Player_.Position = { 1.0f, 0.5f, 0.0f };
    Player_.Forward = { 0.0f, 0.0f, -1.0f };
    Player_.Up = { 0.0f, 1.0f, 0.0f };
    Player_.Yaw = -90.0f;
    Player_.Pitch = 0.0f;
    Player_.Projection = glm::perspective(glm::radians(FieldOfView), AspectRatio, Near, Far);
    Player_.Changed = true;

    Minimap_.Position = { 1.0f, 4.0f, 0.0f };
    Minimap_.Forward= { 0.0f, -1.0f, 0.0f };
    Minimap_.Up = { 1.0f, 0.0f, 0.0f };
    Minimap_.Projection = glm::perspective(glm::radians(FieldOfView), AspectRatio, Near, Far);
    Minimap_.Changed = true;
  }

  void game::GenerateGrid(const generator_type Generator)
  {
    Grid_.Reset();

    if (Generator == generator_type::BinaryTree)
    {
      binary_tree_generator BTGenerator(Sampler_);
      BTGenerator.Generate(Grid_);
    }
    else if (Generator == generator_type::Sidewinder)
    {
      sidewinder_generator SWGenerator(Sampler_);
      SWGenerator.Generate(Grid_);
    }
    else if (Generator == generator_type::HuntAndKill)
    {
      hunt_and_kill_generator HuntAndKillGenerator(Sampler_);
      HuntAndKillGenerator.Generate(Grid_);
    }
    else if(Generator == generator_type::RecursiveBacktracer)
    {
      recursive_backtracer_generator RecursiveBacktracerGenerator(Sampler_);
      RecursiveBacktracerGenerator.Generate(Grid_);
    }

    GenerateMazeGeometry_();
  }

  void game::GenerateMazeGeometry_()
  {
    const auto Rows = Grid_.GetRows();
    const auto Cols = Grid_.GetCols();

    const auto FloorCenterOffset = kFloorSize + 2.0f * kWallWidth;
    const auto FloorCenterToWallCenterOffset = kFloorSize / 2.0f + kWallWidth;
    const auto WallCenterToGapCenteroffset = kFloorSize / 2.0f + kWallWidth;

    MazeGeometry_.FloorBlocks.clear();
    MazeGeometry_.WallBlocks.clear();

    ItemCount_ = 0;
    ItemAcquiredCount_ = 0;

    for (u32 Row = 0; Row < Rows; ++Row)
    {
      for (u32 Col = 0; Col < Cols; ++Col)
      {
        const auto FloorCenterX = Col * FloorCenterOffset;
        const auto FloorCenterZ = Row * FloorCenterOffset;

        const auto& Cell = Grid_(Row, Col);

        // Middle floor block
        {
          maze_floor_block FloorBlock;
          FloorBlock.Center = { FloorCenterX, 0.0f, FloorCenterZ };
          FloorBlock.Size = { kFloorSize, kFloorHeight, kFloorSize };
          FloorBlock.IsVisited = false;
          FloorBlock.HasItem = Cell.GetLinkedCount() == 1;

          ItemCount_ += FloorBlock.HasItem;

          MazeGeometry_.FloorBlocks.emplace_back(FloorBlock);
        }

        if (Cell.IsLinked(cell::neighbor::North))
        {
          maze_floor_block FloorBlock;
          FloorBlock.Center = { FloorCenterX, 0.0f, FloorCenterZ + FloorCenterToWallCenterOffset };
          FloorBlock.Size = { kFloorSize, kFloorHeight, 2.0f * kWallWidth };
          FloorBlock.IsVisited = false;

          MazeGeometry_.FloorBlocks.emplace_back(FloorBlock);
        }
        else
        {
          maze_wall_block WallBlock;
          WallBlock.Center = { FloorCenterX + kWallWidth, kWallHeight / 2.0f, FloorCenterZ + FloorCenterToWallCenterOffset };
          WallBlock.Size = { kFloorSize + 2.0f * kWallWidth, kWallHeight, 2.0f * kWallWidth };

          MazeGeometry_.WallBlocks.emplace_back(WallBlock);
        }

        if (Cell.IsLinked(cell::neighbor::East))
        {
          maze_floor_block FloorBlock;
          FloorBlock.Center = { FloorCenterX + FloorCenterToWallCenterOffset, 0.0f, FloorCenterZ };
          FloorBlock.Size = { 2.0f * kWallWidth, kFloorHeight, kFloorSize };
          FloorBlock.IsVisited = false;

          MazeGeometry_.FloorBlocks.emplace_back(FloorBlock);
        }
        else
        {
          maze_wall_block WallBlock;
          WallBlock.Center = { FloorCenterX + FloorCenterToWallCenterOffset, kWallHeight / 2.0f, FloorCenterZ  + kWallWidth };
          WallBlock.Size = { 2.0f * kWallWidth, kWallHeight, kFloorSize + 2.0f * kWallWidth };

          MazeGeometry_.WallBlocks.emplace_back(WallBlock);
        }

        // @note: This is a special case when there are small gaps left around corners to the west side of the maze
        if (Cell.IsLinked(cell::neighbor::West))
        {
          const auto& WestNeighbor = Grid_(Row, Col - 1);

          if (WestNeighbor.IsLinked(cell::neighbor::North))
          {
            maze_wall_block GapBlock;
            GapBlock.Center = { FloorCenterX - WallCenterToGapCenteroffset, kWallHeight / 2.0f, FloorCenterZ + FloorCenterToWallCenterOffset };
            GapBlock.Size = { kGapSize, kWallHeight, kGapSize };

            MazeGeometry_.WallBlocks.emplace_back(GapBlock);
          }
        }

        if (Row == 0)
        {
          maze_wall_block WallBlock;
          WallBlock.Center = { FloorCenterX - kWallWidth, kWallHeight / 2.0f, FloorCenterZ - FloorCenterToWallCenterOffset };
          WallBlock.Size = { kFloorSize + 2.0f * kWallWidth, kWallHeight, 2.0f * kWallWidth };

          MazeGeometry_.WallBlocks.emplace_back(WallBlock);
        }

        if (Col == 0)
        {
          maze_wall_block WallBlock;
          WallBlock.Center = { FloorCenterX - FloorCenterToWallCenterOffset, kWallHeight / 2.0f, FloorCenterZ + kWallWidth };
          WallBlock.Size = { 2.0f * kWallWidth, kWallHeight, kFloorSize + 2.0f * kWallWidth };

          MazeGeometry_.WallBlocks.emplace_back(WallBlock);
        }
      }
    }
  }

  void game::Update(const f32 Timestep)
  {
    if (Mode_ == mode::GlobalObserver)
      UpdateGlobalObserver_(Timestep);
    else if (Mode_ == mode::Gameplay)
      UpdateGameplay_(Timestep);
  }

  void game::OnKeyPressed(const int Key, const bool Repeat)
  {
    if (Key == GLFW_KEY_W)
    {
      MoveForward_ = true;
    }
    else if (Key == GLFW_KEY_S)
    {
      MoveBackward_ = true;
    }
    else if (Key == GLFW_KEY_A)
    {
      MoveLeft_ = true;
    }
    else if (Key == GLFW_KEY_D)
    {
      MoveRight_ = true;
    }
  }

  void game::OnKeyReleased(const int Key)
  {
    if (Key == GLFW_KEY_W)
    {
      MoveForward_ = false;
    }
    else if (Key == GLFW_KEY_S)
    {
      MoveBackward_ = false;
    }
    else if (Key == GLFW_KEY_A)
    {
      MoveLeft_ = false;
    }
    else if (Key == GLFW_KEY_D)
    {
      MoveRight_ = false;
    }
    else if (Key == GLFW_KEY_1)
    {
      GenerateGrid(generator_type::BinaryTree);
    }
    else if (Key == GLFW_KEY_2)
    {
      GenerateGrid(generator_type::Sidewinder);
    }
    else if (Key == GLFW_KEY_3)
    {
      GenerateGrid(generator_type::HuntAndKill);
    }
    else if (Key == GLFW_KEY_4)
    {
      GenerateGrid(generator_type::RecursiveBacktracer);
    }
    else if (Key == GLFW_KEY_O)
    {
      Mode_ = mode::GlobalObserver;
    }
    else if (Key == GLFW_KEY_P)
    {
      Mode_ = mode::Gameplay;
    }
  }

  void game::OnMouseMotion(const double PositionX, const double PositionY)
  {
    CurrentMousePosition_ = { PositionX, PositionY };

    if (FirstMousePosition_)
    {
      LastMousePosition_ = CurrentMousePosition_;
      FirstMousePosition_ = false;
    }
  }

  game::mode game::GetMode() const
  {
    return Mode_;
  }

  const first_person_camera& game::GetGlobalObserver() const
  {
    return GlobalObserver_;
  }

  const first_person_camera& game::GetPlayer() const
  {
    return Player_;
  }

  const minimap_camera& game::GetMinimap() const
  {
    return Minimap_;
  }

  const maze_geometry& game::GetMazeGeometry() const
  {
    return MazeGeometry_;
  }

  void game::UpdateGlobalObserver_(const f32 Timestep)
  {
    const auto MouseMovement = CurrentMousePosition_ - LastMousePosition_;
    LastMousePosition_ = CurrentMousePosition_;

    if (glm::length(MouseMovement) > 0.0f)
    {
      GlobalObserver_.Yaw += Timestep * kCameraSensitivity * MouseMovement.x;
      GlobalObserver_.Yaw = std::fmod(GlobalObserver_.Yaw, 360.0f);
      GlobalObserver_.Pitch -= Timestep * kCameraSensitivity * MouseMovement.y;
      GlobalObserver_.Pitch = std::clamp(GlobalObserver_.Pitch, -89.0f, 89.0f);

      glm::vec3 Direction;
      Direction.x = std::cos(glm::radians(GlobalObserver_.Yaw)) * std::cos(glm::radians(GlobalObserver_.Pitch));
      Direction.y = std::sin(glm::radians(GlobalObserver_.Pitch));
      Direction.z = std::sin(glm::radians(GlobalObserver_.Yaw)) * std::cos(glm::radians(GlobalObserver_.Pitch));

      GlobalObserver_.Forward = glm::normalize(Direction);
      GlobalObserver_.Changed = true;
    }

    if (MoveForward_)
    {
      GlobalObserver_.Position += Timestep * kCameraSpeed * GlobalObserver_.Forward;
      GlobalObserver_.Changed = true;
    }

    if (MoveBackward_)
    {
      GlobalObserver_.Position -= Timestep * kCameraSpeed * GlobalObserver_.Forward;
      GlobalObserver_.Changed = true;
    }

    if (MoveRight_)
    {
      GlobalObserver_.Position += Timestep * kCameraSpeed * glm::normalize(glm::cross(GlobalObserver_.Forward, GlobalObserver_.Up));
      GlobalObserver_.Changed = true;
    }

    if (MoveLeft_)
    {
      GlobalObserver_.Position -= Timestep * kCameraSpeed * glm::normalize(glm::cross(GlobalObserver_.Forward, GlobalObserver_.Up));
      GlobalObserver_.Changed = true;
    }
  }

  void game::UpdateGameplay_(const f32 Timestep)
  {
    const auto MouseMovement = CurrentMousePosition_ - LastMousePosition_;
    LastMousePosition_ = CurrentMousePosition_;

    if (glm::length(MouseMovement) > 0.0f)
    {
      Player_.Yaw += Timestep * kPlayerCameraSensitivity * MouseMovement.x;
      Player_.Yaw = std::fmod(Player_.Yaw, 360.0f);
      Player_.Pitch -= Timestep * kPlayerCameraSensitivity * MouseMovement.y;
      Player_.Pitch = std::clamp(Player_.Pitch, -89.0f, 89.0f);

      glm::vec3 Direction;
      Direction.x = std::cos(glm::radians(Player_.Yaw)) * std::cos(glm::radians(Player_.Pitch));
      Direction.y = std::sin(glm::radians(Player_.Pitch));
      Direction.z = std::sin(glm::radians(Player_.Yaw)) * std::cos(glm::radians(Player_.Pitch));

      Player_.Forward = glm::normalize(Direction);
      Player_.Changed = true;
    }

    auto NewPosition = Player_.Position;
    bool ChangedNewPosition = false;

    if (MoveForward_)
    {
      NewPosition.x += Timestep * kPlayerCameraSpeed * Player_.Forward.x;
      NewPosition.z += Timestep * kPlayerCameraSpeed * Player_.Forward.z;
      ChangedNewPosition = true;
    }

    if (MoveBackward_)
    {
      NewPosition.x -= Timestep * kPlayerCameraSpeed * Player_.Forward.x;
      NewPosition.z -= Timestep * kPlayerCameraSpeed * Player_.Forward.z;
      ChangedNewPosition = true;
    }

    if (MoveRight_)
    {
      const auto Direction = glm::normalize(glm::cross(Player_.Forward, Player_.Up));

      NewPosition.x += Timestep * kPlayerCameraSpeed * Direction.x;
      NewPosition.z += Timestep * kPlayerCameraSpeed * Direction.z;
      ChangedNewPosition = true;
    }

    if (MoveLeft_)
    {
      const auto Direction = glm::normalize(glm::cross(Player_.Forward, Player_.Up));

      NewPosition.x -= Timestep * kPlayerCameraSpeed * Direction.x;
      NewPosition.z -= Timestep * kPlayerCameraSpeed * Direction.z;
      ChangedNewPosition = true;
    }

    if (ChangedNewPosition)
    {
      const auto IsValidPosition = IsValidPlayerPosition_(NewPosition);
      bool ChangedPosition = false;

      if (IsValidPosition)
      {
        Player_.Position = NewPosition;
        Player_.Changed = true;
        ChangedPosition = true;
      }
      else
      {
        const glm::vec3 NewXPosition = { NewPosition.x, Player_.Position.y, Player_.Position.z };

        const auto IsValidPosition = IsValidPlayerPosition_(NewXPosition);

        if (IsValidPosition)
        {
          Player_.Position.x = NewPosition.x;
          Player_.Changed = true;
          ChangedPosition = true;
        }
        else
        {
          const glm::vec3 NewZPosition = { Player_.Position.x, Player_.Position.y, NewPosition.z };

          const auto IsValidPosition = IsValidPlayerPosition_(NewZPosition);

          if (IsValidPosition)
          {
            Player_.Position.z = NewPosition.z;
            Player_.Changed = true;
            ChangedPosition = true;
          }
        }
      }

      if (ChangedPosition)
      {
        UpdateIsVisited_();
        UpdateMinimapCamera_();
      }
    }
  }

  bool game::IsValidPlayerPosition_(const glm::vec3 Position) const
  {
    for (const auto& Wall : MazeGeometry_.WallBlocks)
    {
      const auto MaximumX = Wall.Center.x + Wall.Size.x / 2.0f;
      const auto MinimumX = Wall.Center.x - Wall.Size.x / 2.0f;

      const auto MaximumZ = Wall.Center.z + Wall.Size.z / 2.0f;
      const auto MinimumZ = Wall.Center.z - Wall.Size.z / 2.0f;

      const auto IsInsideX = (Position.x - kPlayerCollisionDistance) <= MaximumX && (Position.x + kPlayerCollisionDistance) >= MinimumX;
      const auto IsInsideZ = (Position.z - kPlayerCollisionDistance) <= MaximumZ && (Position.z + kPlayerCollisionDistance) >= MinimumZ;

      if (IsInsideX && IsInsideZ)
        return false;
    }

    return true;
  }

  void game::UpdateIsVisited_()
  {
    const auto Position = Player_.Position;

    for (auto& Floor : MazeGeometry_.FloorBlocks)
    {
      const auto MaximumX = Floor.Center.x + Floor.Size.x / 2.0f;
      const auto MinimumX = Floor.Center.x - Floor.Size.x / 2.0f;

      const auto MaximumZ = Floor.Center.z + Floor.Size.z / 2.0f;
      const auto MinimumZ = Floor.Center.z - Floor.Size.z / 2.0f;

      const auto IsInsideX = Position.x <= MaximumX && Position.x >= MinimumX;
      const auto IsInsideZ = Position.z <= MaximumZ && Position.z >= MinimumZ;

      Floor.IsVisited |= (IsInsideX && IsInsideZ);

      if (Floor.IsVisited && Floor.HasItem)
      {
        Floor.HasItem = false;
        ItemAcquiredCount_++;

        if (ItemAcquiredCount_ == ItemCount_)
        {
          LogInfo("You won!");
          Restart_();
        }
      }
    }
  }

  void game::UpdateMinimapCamera_()
  {
    Minimap_.Position.x = Player_.Position.x;
    Minimap_.Position.z = Player_.Position.z;
    Minimap_.Changed = true;
  }

  void game::Restart_()
  {
    Player_.Position = { 1.0f, 0.5f, 0.0f };
    Player_.Changed = true;

    Minimap_.Position = { 1.0f, 4.0f, 0.0f };
    Minimap_.Changed = true;

    GenerateGrid(generator_type::BinaryTree);
  }

}  // myran
