#pragma once

#include <memory>

#include "../window.h"

#include "maze/grid.h"
#include "maze/samplers/uniform.h"
#include "maze/solvers/djikstra.h"

#include "camera.h"

namespace myran {

  constexpr f32 kWallWidth = 0.1f;
  constexpr f32 kWallHeight = 1.0f;
  constexpr f32 kFloorSize = 1.0f;
  constexpr f32 kFloorHeight = 0.1f;
  constexpr f32 kGapSize = 2.0f * kWallWidth;

  constexpr f32 kCameraSpeed = 2.0f;
  constexpr f32 kCameraSensitivity = 0.2f;

  constexpr f32 kPlayerCameraSpeed = 0.25f;
  constexpr f32 kPlayerCameraSensitivity = 0.2f;
  constexpr f32 kPlayerCollisionDistance = 0.05f;

  enum class generator_type
  {
    BinaryTree,
    Sidewinder,
    HuntAndKill,
    RecursiveBacktracer,
  };

  struct maze_floor_block
  {
    glm::vec3 Center;
    glm::vec3 Size;

    bool IsVisited { false };
    bool IsGoal { false };
    bool HasItem { false };
  };

  struct maze_wall_block
  {
    glm::vec3 Center;
    glm::vec3 Size;
  };

  struct maze_geometry
  {
    std::vector<maze_floor_block> FloorBlocks;
    std::vector<maze_wall_block> WallBlocks;
  };

  class game
  {
  public:
    static std::unique_ptr<game> Create(const window& Window,
                                        const u32 Rows,
                                        const u32 Cols,
                                        const generator_type Generator);

  public:
    enum class mode
    {
      GlobalObserver,
      Gameplay,
    };

  public:
    void GenerateGrid(const generator_type Generator);

    void Update(const f32 timestep);

    void OnKeyPressed(const int Key, const bool Repeat);
    void OnKeyReleased(const int Key);
    void OnMouseMotion(const double PositionX, const double PositionY);

    mode GetMode() const;

    const first_person_camera& GetGlobalObserver() const;
    const first_person_camera& GetPlayer() const;
    const minimap_camera& GetMinimap() const;

    const maze_geometry& GetMazeGeometry() const;


  private:
    game(const window& Window,
         const u32 Rows,
         const u32 Cols,
         const generator_type Generator);

    void GenerateMazeGeometry_();
    void UpdateGlobalObserver_(const f32 Timestep);
    void UpdateGameplay_(const f32 Timestep);

    void UpdateIsVisited_();
    void UpdateMinimapCamera_();

    void Restart_();

    bool IsValidPlayerPosition_(const glm::vec3 Position) const;

  private:
    uniform_sampler Sampler_;
    distance_grid Grid_;
    djikstra_solver Solver_;

    maze_geometry MazeGeometry_;

    first_person_camera Player_;
    first_person_camera GlobalObserver_;
    minimap_camera Minimap_;

    bool FirstMousePosition_;
    glm::vec2 LastMousePosition_;
    glm::vec2 CurrentMousePosition_;

    mode Mode_;

    u32 ItemCount_;
    u32 ItemAcquiredCount_;

    bool MoveForward_;
    bool MoveBackward_;
    bool MoveRight_;
    bool MoveLeft_;

  };

}  // myran
