#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>

namespace myran {

  struct first_person_camera
  {
    glm::mat4 GetView() const;
    glm::mat4 GetProjection() const;
    glm::mat4 GetProjectionView() const;
    void Update() const;

    glm::vec3 Position;
    mutable glm::vec3 Forward;
    mutable glm::vec3 Up;

    f32 Yaw;
    f32 Pitch;

    mutable glm::mat4 View;
    glm::mat4 Projection;
    mutable glm::mat4 ProjectionView;

    mutable bool Changed { false };
  };

  struct minimap_camera
  {
    glm::mat4 GetView() const;
    glm::mat4 GetProjection() const;
    glm::mat4 GetProjectionView() const;
    void Update() const;

    glm::vec3 Position;
    mutable glm::vec3 Forward;
    mutable glm::vec3 Up;

    mutable glm::mat4 View;
    glm::mat4 Projection;
    mutable glm::mat4 ProjectionView;

    mutable bool Changed { false };
  };

}  // myran
