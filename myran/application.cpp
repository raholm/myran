#include "debug.h"
#include "application.h"

namespace myran {

  std::optional<application> application::Create()
  {
    LogInfo("Creating application...");

    auto Window = std::make_unique<window>(1920, 1080, "myran");
    if (!Window) return {};

    auto Renderer = renderer::Create(*Window);
    if (!Renderer) return {};

    auto Game = game::Create(*Window, kRows, kCols, kGeneratorType);
    if (!Game) return {};

    application Result;
    Result.Window_ = std::move(Window);
    Result.Renderer_ = std::move(Renderer);
    Result.Game_ = std::move(Game);

    return Result;
  }

  s32 application::Run()
  {
    Window_->SetOnKeyPressedCallback(std::bind(&game::OnKeyPressed, Game_.get(), std::placeholders::_1, std::placeholders::_2));
    Window_->SetOnKeyReleasedCallback(std::bind(&game::OnKeyReleased, Game_.get(), std::placeholders::_1));
    Window_->SetOnMouseMotionCallback(std::bind(&game::OnMouseMotion, Game_.get(), std::placeholders::_1, std::placeholders::_2));

    while (Window_->IsOpen())
    {
      Window_->PollEvents();

      Game_->Update(0.166667f);

      Renderer_->StartFrame();

      const auto GameMode = Game_->GetMode();

      if (GameMode == game::mode::GlobalObserver)
        Renderer_->RenderGlobalObserver(Game_->GetGlobalObserver(), Game_->GetPlayer().Position, Game_->GetMazeGeometry());
      else if (GameMode == game::mode::Gameplay)
        Renderer_->RenderGameplay(Game_->GetPlayer(), Game_->GetMinimap(), Game_->GetMazeGeometry());

      Renderer_->RenderFrame();
    }

    Renderer_->WaitToFinish();

    return 0;
  }

}  // myran
