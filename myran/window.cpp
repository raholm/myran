#include "window.h"

#include "debug.h"

namespace myran {

  namespace details {

    void glfwSetWindowCenter(GLFWwindow* window) {
      GLFWmonitor* Primary = glfwGetPrimaryMonitor();
      if (!Primary) return;

      const GLFWvidmode* Desktop = glfwGetVideoMode(Primary);
      if (!Desktop) return;

      s32 sx = 0;
      s32 sy = 0;
      glfwGetWindowSize(window, &sx, &sy);
      glfwSetWindowPos(window, (Desktop->width - sx) / 2, (Desktop->height - sy) / 2);
    }

  }  // details

  void window::KeyCallback(GLFWwindow* Window, int Key, int Scancode, int Action, int Mods)
  {
    window* Self = (window*) glfwGetWindowUserPointer(Window);

    switch(Action)
    {
    case GLFW_PRESS:
    {
      switch(Key)
      {
      case GLFW_KEY_ESCAPE:
      {
        glfwSetWindowShouldClose(Window, GLFW_TRUE);
        break;
      }
      default:
      {
        if (Self->OnKeyPressedCallback_)
          Self->OnKeyPressedCallback_(Key, false);

        break;
      }
      }

      break;
    }
    case GLFW_RELEASE:
    {
      if (Self->OnKeyReleasedCallback_)
        Self->OnKeyReleasedCallback_(Key);

      break;
    }
    case GLFW_REPEAT:
      switch(Key)
      {
      case GLFW_KEY_ESCAPE:
      {
        glfwSetWindowShouldClose(Window, GLFW_TRUE);
        break;
      }
      default:
      {
        if (Self->OnKeyPressedCallback_)
          Self->OnKeyPressedCallback_(Key, true);

        break;
      }
      }

      break;
    }
  }

  void window::MouseButtonCallback(GLFWwindow* Window, int Button, int Action, int Mods)
  {
    switch(Action)
    {
    case GLFW_PRESS:
      switch(Button)
      {
      case GLFW_MOUSE_BUTTON_LEFT:
      {
        double XPosition, YPosition;
        glfwGetCursorPos(Window, &XPosition, &YPosition);
        LogDebug("Mouse Position: (" << XPosition << ", " << YPosition << ")");
        break;
      }
      }
      break;
    }
  }

  void window::CursorMovementCallback(GLFWwindow* Window, double PositionX, double PositionY)
  {
    window* Self = (window*) glfwGetWindowUserPointer(Window);

    if (Self->OnMouseMotionCallback_)
      Self->OnMouseMotionCallback_(PositionX, PositionY);
  }

  window::window(const u32 Width,
                 const u32 Height,
                 const std::string& Title)
    : Width_(Width), Height_(Height), Title_(Title), Handle_(nullptr)
  {
    LogInternal("Creating window...");

    if (!glfwInit())
    {
      LogError("Failed to initialize glfw");
      return;
    }

    if (!glfwVulkanSupported())
    {
      LogError("Glfw failed to find vulkan loader");
      return;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    Handle_ = glfwCreateWindow(Width_,
                               Height_,
                               Title_.c_str(),
                               nullptr,
                               nullptr);

    if (!Handle_)
    {
      LogError("Failed to create glfw window");
      return;
    }

    glfwGetFramebufferSize(Handle_, &ScreenWidth_, &ScreenHeight_);

    u32 ExtensionCount = 0;
    const char** ExtensionNames = glfwGetRequiredInstanceExtensions(&ExtensionCount);

    if (ExtensionNames && ExtensionCount > 0)
    {
      SurfaceExtensions_.resize(ExtensionCount);
      for (u32 Index = 0; Index < ExtensionCount; ++Index)
        SurfaceExtensions_[Index] = ExtensionNames[Index];
    }

    glfwSetKeyCallback(Handle_, window::KeyCallback);
    glfwSetCursorPosCallback(Handle_, window::CursorMovementCallback);
    glfwSetMouseButtonCallback(Handle_, window::MouseButtonCallback);
    glfwSetWindowUserPointer(Handle_, this);
    glfwSetInputMode(Handle_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwMakeContextCurrent(Handle_);

    details::glfwSetWindowCenter(Handle_);
  }

  window::~window()
  {
    LogInternal("Destroying window...");
    glfwDestroyWindow(Handle_);
    glfwTerminate();
  }

  void window::PollEvents() const
  {
    glfwPollEvents();
  }

  void window::SetTitle(const std::string& String)
  {
    glfwSetWindowTitle(Handle_, String.c_str());
  }

  u32 window::GetWidth() const
  {
    return Width_;
  }

  u32 window::GetHeight() const
  {
    return Height_;
  }

  u32 window::GetScreenWidth() const
  {
    return static_cast<u32>(ScreenWidth_);
  }

  u32 window::GetScreenHeight() const
  {
    return static_cast<u32>(ScreenHeight_);
  }

  std::string window::GetTitle() const
  {
    return Title_;
  }

  GLFWwindow* window::GetHandle() const
  {
    return Handle_;
  }

  const std::vector<std::string>& window::GetSurfaceExtensions() const
  {
    return SurfaceExtensions_;
  }

  bool window::IsOpen() const
  {
    return !glfwWindowShouldClose(Handle_);
  }

  void window::Show() const
  {
    return glfwShowWindow(Handle_);
  }

  void window::Hide() const
  {
    return glfwHideWindow(Handle_);
  }

  void window::SetOnKeyPressedCallback(on_key_pressed_callback Callback)
  {
    OnKeyPressedCallback_ = Callback;
  }

  void window::SetOnKeyReleasedCallback(on_key_released_callback Callback)
  {
    OnKeyReleasedCallback_ = Callback;
  }

  void window::SetOnMouseMotionCallback(on_mouse_motion_callback Callback)
  {
    OnMouseMotionCallback_ = Callback;
  }

}  // myran
