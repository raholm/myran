#pragma once

#include <string>
#include <vector>
#include <functional>

#include <GLFW/glfw3.h>

#include "types.h"

namespace myran {

  class window
  {
  private:
    static void KeyCallback(GLFWwindow* Window, int Key, int Scancode, int Action, int Mods);
    static void MouseButtonCallback(GLFWwindow* Window, int Button, int Action, int Mods);
    static void CursorMovementCallback(GLFWwindow* Window, double PositionX, double PositionY);

  public:
    using on_key_pressed_callback = std::function<void(int, bool)>;
    using on_key_released_callback = std::function<void(int)>;
    using on_mouse_motion_callback = std::function<void(double, double)>;

  public:
    window(const u32 Width,
           const u32 Height,
           const std::string& Title);

    ~window();

    void PollEvents() const;

    void SetTitle(const std::string& String);

    u32 GetWidth() const;
    u32 GetHeight() const;
    u32 GetScreenWidth() const;
    u32 GetScreenHeight() const;
    std::string GetTitle() const;
    GLFWwindow* GetHandle() const;
    const std::vector<std::string>& GetSurfaceExtensions() const;

    bool IsOpen() const;

    void Show() const;
    void Hide() const;

    void SetOnKeyPressedCallback(on_key_pressed_callback Callback);
    void SetOnKeyReleasedCallback(on_key_released_callback Callback);
    void SetOnMouseMotionCallback(on_mouse_motion_callback Callback);

  private:
    u32 Width_;
    u32 Height_;
    s32 ScreenWidth_;
    s32 ScreenHeight_;
    std::string Title_;
    GLFWwindow* Handle_ { nullptr };
    std::vector<std::string> SurfaceExtensions_;

    on_key_pressed_callback OnKeyPressedCallback_;
    on_key_released_callback OnKeyReleasedCallback_;
    on_mouse_motion_callback OnMouseMotionCallback_;

  };

}  // myran
