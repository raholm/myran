#pragma once

#include <memory>
#include <optional>

#include "renderer/renderer.h"
#include "game/game.h"

#include "types.h"
#include "window.h"

namespace myran {

  constexpr u32 kRows = 32;
  constexpr u32 kCols = 32;
  constexpr generator_type kGeneratorType = generator_type::BinaryTree;

  class application final
  {
  public:
    static std::optional<application> Create();

  public:
    s32 Run();

  private:
    application() = default;

  private:
    std::unique_ptr<window> Window_;
    std::unique_ptr<renderer> Renderer_;
    std::unique_ptr<game> Game_;

  };

}  // myran
