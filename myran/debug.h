#pragma once

#include <iostream>
#include <sstream>
#include <chrono>
#include <iomanip>
#include <thread>
#include <mutex>
#include <stdio.h>
#include <cassert>

#include "types.h"

namespace myran {

  enum class log_level
  {
    Internal,
    Debug,
    Info,
    Warning,
    Error,
    Critical
  };

  inline std::string CurrentDateTimeString()
  {
    auto Now = std::chrono::system_clock::now();
    auto Milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(Now.time_since_epoch()) % 1000;
    auto Timer = std::chrono::system_clock::to_time_t(Now);

    std::ostringstream Output;
    Output << std::put_time(std::localtime(&Timer), "%Y-%m-%d %H:%M:%S") << "."
           << std::setfill('0') << std::setw(3) << Milliseconds.count();
    return Output.str();
  }

  inline std::string ToString(const log_level LogLevel)
  {
    switch(LogLevel)
    {
    case log_level::Internal:
      return "INTERNAL";
    case log_level::Debug:
      return "DEBUG";
    case log_level::Info:
      return "INFO";
    case log_level::Warning:
      return "WARNING";
    case log_level::Error:
      return "ERROR";
    case log_level::Critical:
      return "CRITICAL";
    default:
      return "UNKOWN";
    }
  }

  inline void Log(const log_level LogLevel, const std::string& Message, const std::string& FilePath, const u32 LineNumber)
  {
    static std::mutex Mutex;
    std::lock_guard<std::mutex> LockGuard(Mutex);
    auto FileNameStartIndex = FilePath.find_last_of("/\\") + 1;
    auto FileName = FilePath.substr(FileNameStartIndex);

    std::cerr << "[" << std::left << std::setw(22) << CurrentDateTimeString() << "|"
              << std::right << std::setw(8) << ToString(LogLevel) << "|"
              << "T" << std::this_thread::get_id() << "|"
              << FileName << ":" << LineNumber << "]$ "
              << Message << std::endl;
  }

} // myran

#define FormatMessage(Items) ((dynamic_cast<std::ostringstream&>(std::ostringstream().seekp(0, std::ios_base::cur) << Items)).str())
#define LogInternal(Message) myran::Log(myran::log_level::Internal, FormatMessage(Message), __FILE__, __LINE__)
#define LogDebug(Message) myran::Log(myran::log_level::Debug, FormatMessage(Message), __FILE__, __LINE__)
#define LogInfo(Message) myran::Log(myran::log_level::Info, FormatMessage(Message), __FILE__, __LINE__)
#define LogWarning(Message) myran::Log(myran::log_level::Warning, FormatMessage(Message), __FILE__, __LINE__)
#define LogError(Message) myran::Log(myran::log_level::Error, FormatMessage(Message), __FILE__, __LINE__)
#define LogCritical(Message) myran::Log(myran::log_level::Critical, FormatMessage(Message), __FILE__, __LINE__)
#define Assert(Expression) assert((Expression))
