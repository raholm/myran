#!/bin/sh

ROOT_PATH=`dirname $0 | xargs readlink -f`
BUILD_PATH=${ROOT_PATH}/build

mkdir -p ${BUILD_PATH}

cd ${BUILD_PATH}

python3 ../build_shaders.py --search-directory ${ROOT_PATH}/resources/shaders

cmake ..
cmake --build . --parallel 12
