# myran

A maze game where you are supposed to pick up all the items (purple boxes). Use the minimap to see where you have been.

You are able to generate the maze with four different algorithms by pressing:
1 - Binary Tree
2 - Sidewinder
3 - Hunt And Kill
4 - Recursive Backtracer

You can go between being a observer (O) and gameplay (P).

Here is some gameplay footage:

![Gameplay](resources/gameplay.gif)
