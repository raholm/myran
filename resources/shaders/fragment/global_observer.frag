#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 1) uniform Camera {
  vec3 Position;
} InputCamera;

layout(location = 0) in vec4 InputColor;
layout(location = 1) in vec3 InputNormal;
layout(location = 2) in vec3 InputWorldPosition;

layout(location = 0) out vec4 OutputColor;

void main()
{
  vec3 LightColor = vec3(1.0, 1.0, 1.0);

  float AmbientStrength = 0.1;
  vec3 AmbientColor = AmbientStrength * LightColor;

  vec3 Normal = normalize(InputNormal);
  vec3 LightDirection = normalize(InputCamera.Position - InputWorldPosition);

  float DiffuseStrength = max(dot(Normal, LightDirection), 0.0);
  vec3 DiffuseColor = DiffuseStrength * LightColor;

  OutputColor.rgb = (AmbientColor + DiffuseColor) * InputColor.rgb;
  OutputColor.a = 1.0;
}
