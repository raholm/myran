#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 InputColor;
layout(location = 1) in vec3 InputNormal;
layout(location = 2) in vec3 InputWorldPosition;

layout(location = 0) out vec4 OutputColor;
layout(location = 1) out vec3 OutputWorldPosition;
layout(location = 2) out vec3 OutputNormal;

void main()
{
  OutputColor = InputColor;
  OutputWorldPosition = InputWorldPosition;
  OutputNormal = InputNormal;
}
