#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 1) uniform Camera {
  vec3 Position;
  vec3 Forward;
  vec2 Resolution;

  vec3 MinimapPosition;
} InputCamera;

layout(location = 0) in vec4 InputColor;
layout(location = 1) in vec3 InputNormal;
layout(location = 2) in vec3 InputWorldPosition;

layout(location = 0) out vec4 OutputColor;

void main()
{
  float FieldOfView = radians(70.0);

  vec3 LightColor = vec3(1.0, 1.0, 1.0);

  float AmbientStrength = 0.1;
  vec3 AmbientColor = AmbientStrength * LightColor;

  vec3 Normal = normalize(InputNormal);
  vec3 LightDirection = normalize(InputCamera.MinimapPosition - InputWorldPosition);

  float DiffuseStrength = max(dot(Normal, LightDirection), 0.0);
  vec3 DiffuseColor = DiffuseStrength * LightColor;

  vec2 PlayerViewDirection = normalize(InputCamera.Forward.xz);
  vec2 DirectionToPlayer = normalize(InputWorldPosition.xz - InputCamera.Position.xz);

  if (dot(PlayerViewDirection, DirectionToPlayer) >= 0.7) {
    OutputColor.rgb = (AmbientColor + DiffuseColor) * InputColor.rgb;
  }
  else {
    OutputColor.rgb = 0.05 * (AmbientColor + DiffuseColor) * InputColor.rgb;
  }

  OutputColor.a = 1.0;
}
