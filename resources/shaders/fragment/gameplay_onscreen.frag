#version 460
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 1) uniform Camera {
  vec3 Position;
  vec3 Forward;
  ivec2 Resolution;

} InputCamera;

layout(input_attachment_index = 0, binding = 2) uniform subpassInput InputAttachmentColor;
layout(input_attachment_index = 1, binding = 3) uniform subpassInput InputAttachmentWorldPosition;
layout(input_attachment_index = 2, binding = 4) uniform subpassInput InputAttachmentNormal;

layout(location = 0) in vec2 InputTextureCoordinate;

layout(location = 0) out vec4 OutputColor;

void main()
{
  vec3 InputWorldPosition = subpassLoad(InputAttachmentWorldPosition).xyz;
  vec3 InputNormal = subpassLoad(InputAttachmentNormal).xyz;
  vec3 InputColor = subpassLoad(InputAttachmentColor).rgb;

  vec3 LightColor = vec3(1.0, 1.0, 1.0);

  float AmbientStrength = 0.2;
  vec3 AmbientColor = AmbientStrength * LightColor;

  vec3 Normal = normalize(InputNormal);
  vec3 LightDirection = normalize(InputCamera.Forward);
  vec3 DirectionToLightSource = normalize(InputCamera.Position - InputWorldPosition);

  float DistanceFromCenter = distance(vec2(gl_FragCoord.xy) / vec2(InputCamera.Resolution), vec2(0.5, 0.5));
  float InverseDistanceFromCenter = 1.0 / (100.0 * max(DistanceFromCenter, 0.05));

  float Angle = abs(dot(LightDirection, DirectionToLightSource));

  float DiffuseStrength = max(dot(Normal, DirectionToLightSource), 0.0);
  vec3 DiffuseColor = DiffuseStrength * LightColor;

  OutputColor.rgb = Angle * InverseDistanceFromCenter * (AmbientColor + DiffuseColor) * InputColor.rgb;
  OutputColor.a = 1.0;
}
