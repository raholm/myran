#version 460 core
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec2 OutputTextureCoordinate;

out gl_PerVertex
{
  vec4 gl_Position;
};

void main()
{
  OutputTextureCoordinate = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
  gl_Position = vec4(OutputTextureCoordinate * 2.0f - 1.0f, 0.0f, 1.0f);
}
