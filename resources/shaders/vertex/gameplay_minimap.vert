#version 460 core
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform Camera {
  mat4 View;
  mat4 Projection;
  mat4 ProjectionView;

  mat4 MinimapView;
  mat4 MinimapProjection;
  mat4 MinimapProjectionView;
} InputCamera;

// Vertex
layout(location = 0) in vec3 InputPosition;
layout(location = 1) in vec3 InputNormal;

// Instance
layout(location = 2) in mat4 InputTransform;
layout(location = 6) in vec4 InputColor;
layout(location = 7) in ivec4 InputIsVisited;

layout(location = 0) out vec4 OutputColor;
layout(location = 1) out vec3 OutputNormal;
layout(location = 2) out vec3 OutputWorldPosition;

out gl_PerVertex
{
  vec4 gl_Position;
};

void main()
{
  vec4 WorldPosition = InputTransform * vec4(InputPosition, 1.0);
  gl_Position = InputCamera.MinimapProjectionView * WorldPosition;
  gl_Position.y = -gl_Position.y;

  if (InputIsVisited.x == 1) {
    OutputColor = vec4(1.0, 1.0, 0, 1.0);
  }
  else {
    OutputColor = InputColor;
  }

  OutputNormal = mat3(transpose(inverse(InputTransform))) * InputNormal;
  OutputWorldPosition = WorldPosition.xyz;
}
