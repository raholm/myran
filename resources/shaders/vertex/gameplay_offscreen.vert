#version 460 core
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform Camera {
  mat4 View;
  mat4 Projection;
  mat4 ProjectionView;
} InputCamera;

// Vertex
layout(location = 0) in vec3 InputPosition;
layout(location = 1) in vec3 InputNormal;

// Instance
layout(location = 2) in mat4 InputTransform;
layout(location = 6) in vec4 InputColor;

layout(location = 0) out vec4 OutputColor;
layout(location = 1) out vec3 OutputNormal;
layout(location = 2) out vec3 OutputWorldPosition;

out gl_PerVertex
{
  vec4 gl_Position;
};

void main()
{
  vec4 WorldPosition = InputTransform * vec4(InputPosition, 1.0);
  gl_Position = InputCamera.ProjectionView * WorldPosition;
  OutputColor = InputColor;
  OutputNormal = mat3(transpose(inverse(InputTransform))) * InputNormal;
  OutputWorldPosition = WorldPosition.xyz;
}
