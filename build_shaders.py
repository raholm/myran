import sys
import os
import subprocess
import pathlib
import argparse

def create_directory_if_missing(directory):
    pathlib.Path(directory).mkdir(parents=True, exist_ok=True)

def get_files_recursively(directory):
    for subdir, dirs, files in os.walk(directory):
        for f in files:
            yield subdir, f, os.path.join(subdir, f)

def get_compiler():
    if "VULKAN_SDK" not in os.environ:
        return "/bin/glslangValidator"

    return os.path.join(os.environ["VULKAN_SDK"], "bin", "glslangValidator")

def is_shader(filename):
    for extension in (".frag", ".vert"):
        if filename.endswith(extension):
            return True

    return False

def get_destination_file(destination_path, filename):
    filename, extension = os.path.splitext(filename)
    filename_postfix = extension[1:] # We remove the '.'
    return os.path.join(destination_path, filename + ".spv")

def get_destination_directory(directory):
    return directory.replace("resources", os.path.join("build", "bin"))

def main():
    parser = argparse.ArgumentParser(description = "Compile GLSL shaders to SPIR-V")
    parser.add_argument("--search-directory",
                        dest="search_directory",
                        type=str,
                        required=True,
                        help="Directory to search for shaders to compile")

    args = parser.parse_args()

    print("Compiling shaders to byte code...")

    compiler = get_compiler()

    for directory, filename, source_file in get_files_recursively(args.search_directory):
        if not is_shader(filename):
            continue

        destination_directory = get_destination_directory(directory)
        create_directory_if_missing(destination_directory)

        destination_file = get_destination_file(destination_directory, filename)

        # Only compile changed files
        if os.path.isfile(destination_file):
            source_modify_time = os.path.getmtime(source_file)
            destination_modify_time = os.path.getmtime(destination_file)

            if source_modify_time <= destination_modify_time:
                continue

        print("Compiling {0} into {1}...".format(source_file, destination_file))

        result = subprocess.run([compiler,
                                 "--target-env", "vulkan1.2",
                                 "-V", source_file,
                                 "-o", destination_file],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                text=True)

        if result.returncode != 0:
            print("Standard output:")
            print(result.stdout)
            print("Standard error:")
            print(result.stderr)
            sys.exit(1)

if __name__ == "__main__":
    main()
